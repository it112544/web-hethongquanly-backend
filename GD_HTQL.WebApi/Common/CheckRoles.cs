﻿using System.Security.Claims;

namespace GD_HTQL.WebApi.Common
{
    public class CheckRoles
    {
        public static bool CheckRoleGroupKhachHang(ClaimsPrincipal currentUser)
        {
            if (currentUser.IsInRole("Admin") || currentUser.IsInRole("GD") || currentUser.IsInRole("PTGD") || currentUser.IsInRole("PGD") || currentUser.IsInRole("Quản trị kinh doanh"))
            {             
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool CheckRoleGroupDoiTac(ClaimsPrincipal currentUser)
        {
            if (currentUser.IsInRole("Admin") || currentUser.IsInRole("GD") || currentUser.IsInRole("PTGD") || currentUser.IsInRole("PGD") || currentUser.IsInRole("Quản trị đối tác"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool CheckRoleGroupCongViec(ClaimsPrincipal currentUser)
        {
            if (currentUser.IsInRole("Admin") || currentUser.IsInRole("GD") || currentUser.IsInRole("PTGD") || currentUser.IsInRole("PGD"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

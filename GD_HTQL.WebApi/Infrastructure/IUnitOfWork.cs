﻿using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        // Nhóm danh mục địa phương
        ITinhRepository DMTinhRepos { get; }
        IHuyenRepository DMHuyenRepos { get; }
        IXaRepository DMXaRepos { get; }

        // Nhóm khách hàng
        ITacGiaRepository TacGiaRepos { get; }
        INSCoQuanRepository NSCoQuanRepos { get; }
        INSCanBoRepository NSCanBoRepos { get; }
        INhaCungCapRepository NhaCungCapRepos { get; }
        INhaThauRepository NhaThauRepos { get; }
        IQuanLyTuongTacRepository QuanLyTuongTacRepos { get; }

        // Nhóm quản lý nhân viên

        ICongTyRepository CongTyRepos { get; }
        IPhongBanRepository PhongBanRepos { get; }
        IChucVuRepository ChucVuRepos { get; }
        INhanVienRepository NhanVienRepos { get; }

        // Nhóm sản phẩm
        ILoaiSanPhamRepository LoaiSanPhamRepos { get; }
        IKhoiLopRepository KhoiLopRepos { get; }
        IMonHocRepository MonHocRepos { get; }
        IFileRepository FileRepos { get; }

        // Văn bản 
        ILoaiVanBanRepository LoaiVanBanRepos { get; }
        IVanBanRepository VanBanRepos { get; }


        ISanPhamRepository SanPhamRepos { get; }
        IDuToanRepository DuToanRepos { get; }
        IKhuVucRepository KhuVucRepos { get; }
        ILoaiNhaCungCapRepository LoaiNhaCungCapRepos { get; }
        ILoaiNhaThauRepository LoaiNhaThauRepos { get; }
        IViTriRepository ViTriRepos { get; }
        IDaiLyRepository DaiLyRepos { get; }
        ILoaiDaiLyRepository LoaiDaiLyRepos { get; }
        IQuanLyNhanVienRepository QuanLyNhanVienRepos { get; }
        IThongTuRepository ThongTuRepos { get; }
        ITinhNhanVienRepository TinhNhanVienRepos { get; }
        IDonViTinhRepository DonViTinhRepos { get; }
        INT_GiaTriCoHoiRepository NT_GiaTriCoHoiRepos { get; }
        INguonVonRepository NguonVonRepos { get; }

        // Công việc 
        INhomCongViecRepository NhomCongViecRepos { get; }
        ICongViecRepository CongViecRepos { get; }
        ITrangThaiCongViecRepository TrangThaiCongViecRepos { get; }
        IKHCT_NgayRepository KHCT_NgayRepos { get; }
        IKHCT_TuanRepository KHCT_TuanRepos { get; }
        IKHCT_ThangRepository KHCT_ThangRepos { get; }
        ICtyDuToanRepository CtyDuToanRepos { get; }
        IKHCT_NoiDungRepository KHCT_NoiDungRepos { get; }
        IKHCT_NguoiDiCungRepository KHCT_NguoiDiCungRepos { get; }
        IKHCT_ChiPhiRepository KHCT_ChiPhiRepos { get; }
        IKHCT_XeRepository KHCT_XeRepos { get; }
        IKHCT_Ngay_LichSuRepository KHCT_Ngay_LichSuRepos { get; }
        ILichSuDuToanRepository LichSuDuToanRepos { get; }
        IKHCT_ChiPhiKhacRepository KHCT_ChiPhiKhacRepos { get; }
        INT_DuToanRepository NT_DuToanRepos { get; }

        INghiPhepRepository NghiPhepRepos { get; }
        INghiPhep_LichSuRepository NghiPhep_LichSuRepos { get; }

        INSDuToanRepository NSDuToanRepos { get; }
        ILichSuCongViecRepository LichSuCongViecRepos { get; }

        IThongBaoCauHinhRepository ThongBaoCauHinhRepos { get; }
        IThongBaoNhanVienRepository ThongBaoNhanVienRepos { get; }
        int Save();    
    }
}

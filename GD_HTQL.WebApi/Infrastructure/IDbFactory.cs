﻿using GD_HTQL.WebApi.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        ApplicationDbContext Init();
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class Up_KHCT_cty : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "KHCTCongTyID",
                table: "KHCT_Ngay",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "KHCTTenCongTy",
                table: "KHCT_Ngay",
                type: "nvarchar(225)",
                maxLength: 225,
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "KHCTCongTyID",
                table: "KHCT_Ngay");

            migrationBuilder.DropColumn(
                name: "KHCTTenCongTy",
                table: "KHCT_Ngay");
        }
    }
}

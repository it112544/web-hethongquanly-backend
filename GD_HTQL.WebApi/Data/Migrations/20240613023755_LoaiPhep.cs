﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class LoaiPhep : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LoaiID",
                table: "NghiPhep",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "NghiPhepLoai",
                columns: table => new
                {
                    LoaiID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenLoaiNghiPhep = table.Column<string>(type: "nvarchar(225)", maxLength: 225, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NghiPhepLoai", x => x.LoaiID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NghiPhep_LoaiID",
                table: "NghiPhep",
                column: "LoaiID");

            migrationBuilder.AddForeignKey(
                name: "FK_NghiPhep_NghiPhepLoai_LoaiID",
                table: "NghiPhep",
                column: "LoaiID",
                principalTable: "NghiPhepLoai",
                principalColumn: "LoaiID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NghiPhep_NghiPhepLoai_LoaiID",
                table: "NghiPhep");

            migrationBuilder.DropTable(
                name: "NghiPhepLoai");

            migrationBuilder.DropIndex(
                name: "IX_NghiPhep_LoaiID",
                table: "NghiPhep");

            migrationBuilder.DropColumn(
                name: "LoaiID",
                table: "NghiPhep");
        }
    }
}

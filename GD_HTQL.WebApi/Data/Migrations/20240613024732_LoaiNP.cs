﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace GD_HTQL.WebApi.Data.Migrations
{
    /// <inheritdoc />
    public partial class LoaiNP : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NghiPhep_NghiPhepLoai_LoaiID",
                table: "NghiPhep");

            migrationBuilder.AlterColumn<int>(
                name: "LoaiID",
                table: "NghiPhep",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_NghiPhep_NghiPhepLoai_LoaiID",
                table: "NghiPhep",
                column: "LoaiID",
                principalTable: "NghiPhepLoai",
                principalColumn: "LoaiID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NghiPhep_NghiPhepLoai_LoaiID",
                table: "NghiPhep");

            migrationBuilder.AlterColumn<int>(
                name: "LoaiID",
                table: "NghiPhep",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_NghiPhep_NghiPhepLoai_LoaiID",
                table: "NghiPhep",
                column: "LoaiID",
                principalTable: "NghiPhepLoai",
                principalColumn: "LoaiID");
        }
    }
}

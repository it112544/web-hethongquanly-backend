﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;
using GD_HTQL.WebApi.Data.Models.Dtos.CongViec;
using GD_HTQL.WebApi.Data.Models.Dtos.DaiLy;
using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;
using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;
using GD_HTQL.WebApi.Data.Models.Dtos.NghiPhep;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaCungCap;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.Dtos.TacGia;
using GD_HTQL.WebApi.Data.Models.Dtos.VanBan;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.EF.TacGiaModels;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;

namespace GD_HTQL.WebApi.Data.Models
{
    public class AppMapperProfile : Profile
    {
        public AppMapperProfile()
        {
            CreateMap<DMTinhDto, DMTinh>();
            CreateMap<DMTinh, DMTinhDto>();

            CreateMap<DMHuyenDto, DMHuyen>();
            CreateMap<DMHuyen, DMHuyenDto>();

            CreateMap<DMXaDto, DMXa>();
            CreateMap<DMXa, DMXaDto>();

            CreateMap<NSCanBoDto, NSCanBo>();
            CreateMap<NSCanBo, NSCanBoDto>();

            CreateMap<NSCoQuanDto, NSCoQuan>();
            CreateMap<NSCoQuan, NSCoQuanDto>();

            CreateMap<QuanLyTuongTacDto,QuanLyTuongTac>();
            CreateMap<QuanLyTuongTac, QuanLyTuongTacDto>();

            CreateMap<SanPhamDto, SanPham>();
            CreateMap<SanPham, SanPhamDto>();

            CreateMap<SanPhamViewDto, SanPham>();
            CreateMap<SanPham, SanPhamViewDto>();

            CreateMap<DuToanDto, DuToan>();
            CreateMap<DuToan, DuToanDto>();

            CreateMap<CongTyDto, CongTy>();
            CreateMap<CongTy, CongTyDto>();

            CreateMap<PhongBanDto, PhongBan>();
            CreateMap<PhongBan, PhongBanDto>();

            CreateMap<ChucVuDto, ChucVu>();
            CreateMap<ChucVu, ChucVuDto>();
           
            CreateMap<FileDinhKemDto, FileDinhKem>();
            CreateMap<FileDinhKem, FileDinhKemDto>();

            CreateMap<NhanVienDto, NhanVien>();
            CreateMap<NhanVien, NhanVienDto>();

            CreateMap<LoaiVanBanDto, LoaiVanBan>();
            CreateMap<LoaiVanBan, LoaiVanBanDto>();

            CreateMap<VanBanDto, VanBan>();
            CreateMap<VanBan, VanBanDto>();

            CreateMap<LoaiSanPhamDto, LoaiSanPham>();
            CreateMap<LoaiSanPham, LoaiSanPhamDto>();

            CreateMap<KhoiLopDto, KhoiLop>();
            CreateMap<KhoiLop, KhoiLopDto>();

            CreateMap<ThongTu, ThongTuDto>();
            CreateMap<ThongTuDto, ThongTu>();

            CreateMap<MonHocDto, MonHoc>();
            CreateMap<MonHoc, MonHocDto>();

            CreateMap<DonViTinhDto, DonViTinh>();
            CreateMap<DonViTinh, DonViTinhDto>();

            CreateMap<NhaCungCap, NhaCungCapDto>();
            CreateMap<NhaCungCapDto, NhaCungCap>();

            CreateMap<NhaThauDto, NhaThau>();
            CreateMap<NhaThau, NhaThauDto>();

            CreateMap<NhaThau, NhaThauViewModel>();
            CreateMap<NhaThauViewModel, NhaThau>();

            CreateMap<LoaiNhaCungCapDto, LoaiNhaCungCap>();
            CreateMap<LoaiNhaCungCap, LoaiNhaCungCapDto>();

            CreateMap<LoaiNhaThauDto, LoaiNhaThau>();
            CreateMap<LoaiNhaThau, LoaiNhaThauDto>();

            CreateMap<ViTri, ViTriDto>();
            CreateMap<ViTriDto, ViTri>();

            CreateMap<DaiLyDto, DaiLy>();
            CreateMap<DaiLy, DaiLyDto>();

            CreateMap<NguonVon, NguonVonDto>();
            CreateMap<NguonVonDto, NguonVon>();

            CreateMap<NT_GiaTriCoHoiDto, NT_GiaTriCoHoi>();
            CreateMap<NT_GiaTriCoHoi, NT_GiaTriCoHoiDto>();

            CreateMap<TacGiaDto, TacGia>();
            CreateMap<TacGia, TacGiaDto>();

            CreateMap<KHCT_Ngay, KHCT_NgayDto>();
            CreateMap<KHCT_NgayDto, KHCT_Ngay>();

            CreateMap<KHCT_Tuan, KHCT_TuanDto>();
            CreateMap<KHCT_TuanDto, KHCT_Tuan>();

            CreateMap<KHCT_Thang, KHCT_ThangDto>();
            CreateMap<KHCT_ThangDto, KHCT_Thang>();

            CreateMap<CongViec, CongViecDto>();
            CreateMap<CongViecDto, CongViec>();

            CreateMap<NhomCongViec, NhomCongViecDto>();
            CreateMap<NhomCongViecDto, NhomCongViec>();

            CreateMap<QuanLyTuongTac, TuongTacDetails>();
            CreateMap<TuongTacDetails, QuanLyTuongTac>();

            CreateMap<BuocThiTruong, BuocThiTruongDto>();
            CreateMap<BuocThiTruongDto, BuocThiTruong>();

            CreateMap<GiaTriCoHoiViewModel, NT_GiaTriCoHoi>();
            CreateMap<NT_GiaTriCoHoi, GiaTriCoHoiViewModel>();

            CreateMap<KHCT_Ngay, KHCT_Ngay_ViewModel>();
            CreateMap<KHCT_Ngay_ViewModel, KHCT_Ngay>();

            CreateMap<KHCT_NguoiDiCung, KHCT_NguoiDiCungDto>();
            CreateMap<KHCT_NguoiDiCungDto, KHCT_NguoiDiCung>();

            CreateMap<KHCT_NoiDung, KHCT_NoiDungDto>();
            CreateMap<KHCT_NoiDungDto, KHCT_NoiDung>();

            CreateMap<KHCT_ChiPhi, KHCT_ChiPhiDto>();
            CreateMap<KHCT_ChiPhiDto, KHCT_ChiPhi>();

            CreateMap<KHCT_Xe, KHCT_XeDto>();
            CreateMap<KHCT_XeDto, KHCT_Xe>();

            CreateMap<KHCT_Ngay_LichSu, KHCT_Ngay_LichSuDto>();
            CreateMap<KHCT_Ngay_LichSuDto, KHCT_Ngay_LichSu>();

            CreateMap<KHCT_ChiPhiView, KHCT_ChiPhi>();
            CreateMap<KHCT_ChiPhi, KHCT_ChiPhiView>();

            CreateMap<NhanVien, NhanVienShortView>();
            CreateMap<NhanVienShortView, NhanVien>();

            CreateMap<KHCT_ChiPhiKhac, KHCT_ChiPhiKhacDto>();
            CreateMap<KHCT_ChiPhiKhacDto, KHCT_ChiPhiKhac>();

            CreateMap<KHCT_Ngay, KHCT_Ngay_ShortView>();
            CreateMap<KHCT_Ngay_ShortView, KHCT_Ngay>();

            CreateMap<NT_DuToan, NT_DuToanDto>();
            CreateMap<NT_DuToanDto, NT_DuToan>();

            CreateMap<NT_DuToan, NT_DuToanView>();
            CreateMap<NT_DuToanView, NT_DuToan>();

            CreateMap<NghiPhep, NghiPhepDto>();
            CreateMap<NghiPhepDto, NghiPhep>();

            CreateMap<NSDuToan, NSDuToanDto>();
            CreateMap<NSDuToanDto, NSDuToan>();

            CreateMap<NSDuToan, NSDuToanView>();
            CreateMap<NSDuToanView, NSDuToan>();

            CreateMap<NSCoQuan, NSCoQuanViewModel>();
            CreateMap<NSCoQuanViewModel, NSCoQuan>();

            CreateMap<QuanLyTuongTac, QLTuongTacView>();
            CreateMap<QLTuongTacView, QuanLyTuongTac>();

            CreateMap<FileDinhKem, FileDinhKemView>();
            CreateMap<FileDinhKemView, FileDinhKem>();

            CreateMap<LichSuCongViec, LichSuCongViecDto>();
            CreateMap<LichSuCongViecDto, LichSuCongViec>();

            CreateMap<LichSuCongViec, LichSuCongViecView>();
            CreateMap<LichSuCongViecView, LichSuCongViec>();

            CreateMap<NghiPhep_LichSu, NghiPhep_LichSuDto>();
            CreateMap<NghiPhep_LichSuDto, NghiPhep_LichSu>();

            CreateMap<NghiPhep_LichSu, NghiPhep_LichSuView>();
            CreateMap<NghiPhep_LichSuView, NghiPhep_LichSuDto>();

            CreateMap<NghiPhep, NghiPhepViewModel>();
            CreateMap<NghiPhepViewModel, NghiPhep>();

            CreateMap<FileCongViec, FileCongViecDto>();
            CreateMap<FileCongViecDto, FileCongViec>();
        }
    }
}

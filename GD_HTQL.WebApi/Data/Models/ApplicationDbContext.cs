﻿using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.EF.TacGiaModels;
using GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
namespace GD_HTQL.WebApi.Data.Models
{
    public class ApplicationDbContext :IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        // Danh mục địa phương
        public DbSet<DMKhuVuc> DMKhuVuc { get; set; }
        public DbSet<DMTinh> DMTinh { get; set; }
        public DbSet<DMHuyen> DMHuyen { get; set; }
        public DbSet<DMXa> DMXa { get; set; }       
        public DbSet<NSCoQuan> NSCoQuan { get; set; }
        public DbSet<NSCanBo> NSCanBo { get; set; }
        public DbSet<QuanLyTuongTac> QuanLyTuongTac { get; set; }
        public DbSet<NhaThau> NhaThau { get; set; }
        public DbSet<NhaCungCap> NhaCungCap { get; set; }
        public DbSet<TacGia> TacGia { get; set; }
        public DbSet<NhanVien> NhanVien { get; set; }
        public DbSet<BuocThiTruong> BuocThiTruong { get; set; }
        public DbSet<ApplicationUser> ApplicationUser { get; set; }

        // Sản phẩm
        public DbSet<KhoiLop> KhoiLop { get; set; }
        public DbSet<MonHoc> MonHoc { get; set; }
        public DbSet<LoaiSanPham> LoaiSanPham { get; set; }
        public DbSet<SanPham> SanPham { get; set; }
        public DbSet<DuToan> DuToan { get; set; }
        public DbSet<SanPhamDuToan> SanPhamDuToan { get; set; }

        //Hành chính nhân sự
        public DbSet<CongTy> CongTy { get; set; }
        public DbSet<PhongBan> PhongBan { get; set; }
        public DbSet<ChucVu> ChucVu { get; set; }
        public DbSet<ChucVuNhanVien> ChucVuNhanVien { get; set; }

        // Quản lý văn bản
        public DbSet<LoaiVanBan> LoaiVanBan { get; set; }
        public DbSet<VanBan> VanBan { get; set; }
        public DbSet<FileVanBan> FileVanBan { get; set; }
        public DbSet<FileDinhKem> FileDinhKem { get; set; }

        // Quản lý công việc
        public DbSet<NhomCongViec> NhomCongViec { get; set; }
        public DbSet<CongViec> CongViec { get; set; }
        public DbSet<TrangThaiCongViec> TrangThaiCongViec { get; set; }
        public DbSet<LSGiaoViec> LSGiaoViec { get; set; }
        public DbSet<LichSuCongViec> LichSuCongViec { get; set; }
        public DbSet<LichSuDuToan> LichSuDuToan { get; set; }
        public DbSet<TrangThaiDuToan> TrangThaiDuToan { get; set; }

        public DbSet<LoaiNhaCungCap> LoaiNhaCungCap { get; set; }
        public DbSet<LoaiNhaThau> LoaiNhaThau { get; set; }
        public DbSet<ViTri> ViTri { get; set; }
        public DbSet<LoaiDaiLy> LoaiDaiLy { get; set; }
        public DbSet<DaiLy> DaiLy { get; set; }
        public DbSet<QuanLyNhanVien> QuanLyNhanVien { get; set; }
        public DbSet<SanPhamKhoiLop> SanPhamKhoiLop { get; set; }
        public DbSet<SanPhamMonHoc> SanPhamMonHoc { get; set; }
        public DbSet<ThongTu> ThongTu { get; set; }
        public DbSet<SanPhamThongTu> SanPhamThongTu { get; set; }
        public DbSet<TinhNhanVien> TinhNhanVien { get; set; }
        public DbSet<DonViTinh> DonViTinh { get; set; }
        public DbSet<NguonVon> NguonVon {  get; set; }  
        public DbSet<NT_GiaTriCoHoi> NT_GiaTriCoHoi { get; set; }
        public DbSet<KHCT_Thang> KHCT_Thang { get; set; }
        public DbSet<KHCT_Tuan> KHCT_Tuan { get; set; }
        public DbSet<KHCT_Ngay> KHCT_Ngay { get; set; }
        public DbSet<DoUuTien> DoUuTien { get; set; }
        public DbSet<KHCT_Ngay_TrangThai> KHCT_Ngay_TrangThai { get; set; }
        public DbSet<KHCT_Ngay_LichSu> KHCT_Ngay_LichSu { get; set; }
        public DbSet<CtyDuToan> CtyDuToan { get; set; }
        public DbSet<MonHocThongTu> MonHocThongTu { get; set; }
        public DbSet<NT_DiaBanHoatDong> NT_DiaBanHoatDong { get; set; }
        public DbSet<NT_LoaiHopTac> NT_LoaiHopTac { get; set; }
        public DbSet<NT_The> NT_The { get; set; }
        public DbSet<KHCT_NoiDung> KHCT_NoiDung { get; set; }
        public DbSet<KHCT_NguoiDiCung> KHCT_NguoiDiCung { get; set; }
        public DbSet<KHCT_Xe> KHCT_Xe { get; set; }
        public DbSet<KHCT_ChiPhi> KHCT_ChiPhi { get; set; }
        public DbSet<KHCT_LoaiChiPhi> KHCT_LoaiChiPhi { get; set; }
        public DbSet<KHCT_HangMucChiPhi> KHCT_HangMucChiPhi { get; set; }
        public DbSet<KHCT_ChiPhiKhac> KHCT_ChiPhiKhac { get; set; }

        public DbSet<NT_LoaiSanPham> NT_LoaiSanPham { get; set; }
        public DbSet<NT_DuToan> NT_DuToan { get; set; }
        public DbSet<NT_TrangThaiDuToan> NT_TrangThaiDuToan { get; set; }
        public DbSet<NghiPhep> NghiPhep { get; set; }
        public DbSet<NghiPhep_LichSu> NghiPhep_LichSu { get; set; }
        public DbSet<NghiPhep_TrangThai> NghiPhep_TrangThai { get; set; }
        public DbSet<NSDuToan> NSDuToan { get; set; }
        public DbSet<NSNguonVon> NSNguonVon { get; set; }
        public DbSet<FileLoai> FileLoai { get; set; }
        public DbSet<ThongBaoCauHinh> ThongBaoCauHinh { get; set; }
        public DbSet<ThongBaoNhanVien> ThongBaoNhanVien { get; set; }
        public DbSet<FileCongViec> FileCongViec { get; set; }
        public DbSet<NghiPhepLoai> NghiPhepLoai { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<DMKhuVuc>()
                        .HasMany(e => e.DMTinh)
                        .WithOne(e => e.DMKhuVuc)
                        .HasForeignKey(e => e.KhuVucID);

            modelBuilder.Entity<DMTinh>()
                        .HasMany(e => e.DMHuyen)
                        .WithOne(e => e.DMTinh)
                        .HasForeignKey(e => e.TinhID)
                        .HasPrincipalKey(e => e.TinhID);

            modelBuilder.Entity<DMHuyen>()
                        .HasMany(e => e.DMXa)
                        .WithOne(e => e.DMHuyen)
                        .HasForeignKey(e => e.HuyenID)
                        .HasPrincipalKey(e => e.HuyenID);

            modelBuilder.Entity<NSCoQuan>()
                        .HasMany(e => e.NSCanBo)
                        .WithOne(e => e.NSCoQuan)
                        .HasForeignKey(e => e.CoQuanID);                    

            modelBuilder.Entity<NSCoQuan>()
                        .HasMany(e => e.TuongTac)
                        .WithOne(e => e.NSCoQuan)
                        .HasForeignKey(e => e.CoQuanID);                             

            modelBuilder.Entity<SanPham>()
                       .HasMany(e => e.ThongTu)
                       .WithMany(e => e.SanPham)
                       .UsingEntity<SanPhamThongTu>();

            modelBuilder.Entity<SanPham>()
                        .HasMany(e => e.MonHoc)
                        .WithMany(e => e.SanPham)
                        .UsingEntity<SanPhamMonHoc>();

            modelBuilder.Entity<SanPham>()
                        .HasMany(e => e.KhoiLop)
                        .WithMany(e => e.SanPham)
                        .UsingEntity<SanPhamKhoiLop>();

            // Config nhân viên
            modelBuilder.Entity<NhanVien>()
                        .HasMany(e => e.TuongTac)
                        .WithOne(e => e.NhanVien)
                        .HasForeignKey(e => e.NhanVienID);
                              
            modelBuilder.Entity<NhanVien>()
                        .HasOne(u => u.User)
                        .WithOne(e => e.NhanVien)
                        .HasForeignKey<NhanVien>(p => p.UserID).IsRequired();

            modelBuilder.Entity<NhanVien>()
                        .HasMany(e => e.FileDinhKem)
                        .WithOne(e => e.NhanVien)
                        .HasForeignKey(e => e.NhanVienID);

            modelBuilder.Entity<NhanVien>()
                        .HasMany(e => e.ChucVu)
                        .WithMany(e => e.NhanVien)
                        .UsingEntity<ChucVuNhanVien>();
                      
            //End Config nhân viên

            modelBuilder.Entity<CongTy>()
                        .HasMany(e => e.PhongBan)
                        .WithOne(e => e.CongTy)
                        .HasForeignKey(e => e.CongTyID);                       

            modelBuilder.Entity<PhongBan>()
                        .HasMany(e => e.ChucVu)
                        .WithOne(e => e.PhongBan)
                        .HasForeignKey(e => e.PhongBanID);                      

            // Mối quan hệ loại văn bản và văn bản
            modelBuilder.Entity<LoaiVanBan>()
                        .HasMany(e => e.VanBan)
                        .WithOne(e => e.LoaiVanBan)
                        .HasForeignKey(e => e.LoaiVanBanID);
                       
            // Mối quan hệ văn bản và file
            modelBuilder.Entity<VanBan>()
                        .HasMany(e => e.FileVanBan)
                        .WithOne(e => e.VanBan)
                        .HasForeignKey(e => e.VanBanID);                    

            modelBuilder.Entity<CongViec>()
                        .HasMany(e => e.LichSuCongViec)
                        .WithOne(e => e.CongViec)
                        .HasForeignKey(e => e.CongViecID);

            // Mối quan hệ nhóm công việc và công việc
            modelBuilder.Entity<NhomCongViec>()
                        .HasMany(e => e.CongViec)
                        .WithOne(u => u.NhomCongViec)
                        .HasForeignKey(e => e.NhomCongViecID);

            // Mối quan hệ trạng thái và dự toán
            modelBuilder.Entity<TrangThaiDuToan>()
                        .HasMany(e => e.LichSuDuToan)
                        .WithOne(e => e.TrangThaiDuToan)
                        .HasForeignKey(e => e.TrangThaiDuToanID);

            modelBuilder.Entity<NhanVien>()
                        .HasMany(e => e.LichSuDuToan)
                        .WithOne(e => e.NhanVien)
                        .HasForeignKey(e => e.NhanVienID);

            modelBuilder.Entity<DuToan>()
                        .HasMany(e => e.LichSuDuToan)
                        .WithOne(e => e.DuToan)
                        .HasForeignKey(e => e.DuToanID);

            modelBuilder.Entity<LoaiSanPham>()
                        .HasMany(e => e.SanPham)
                        .WithOne(e => e.LoaiSanPham)
                        .HasForeignKey(e => e.LoaiSanPhamID);

            modelBuilder.Entity<DMHuyen>()
                        .HasMany(e => e.NSCoQuan)
                        .WithOne(e => e.Huyen)
                        .HasForeignKey(e => e.HuyenID);

            modelBuilder.Entity<DMTinh>()
                        .HasMany(e => e.NhaThau)
                        .WithOne(e => e.Tinh)
                        .HasForeignKey(e => e.TinhID);

            modelBuilder.Entity<DMTinh>()
                        .HasMany(e => e.NhaCungCap)
                        .WithOne(e => e.Tinh)
                        .HasForeignKey(e => e.TinhID);

            modelBuilder.Entity<LoaiNhaCungCap>()
                        .HasMany(e => e.NhaCungCap)
                        .WithOne(e => e.LoaiNhaCungCap)
                        .HasForeignKey(e => e.LoaiNCCID);

            modelBuilder.Entity<LoaiNhaThau>()
                        .HasMany(e => e.NhaThau)
                        .WithOne(e => e.LoaiNhaThau)
                        .HasForeignKey(e => e.LoaiNTID);

            modelBuilder.Entity<LoaiDaiLy>()
                        .HasMany(e => e.DaiLy)
                        .WithOne(e => e.LoaiDaiLy)
                        .HasForeignKey(e => e.LoaiDLID);

            modelBuilder.Entity<DMTinh>()
                        .HasMany(e => e.DaiLy)
                        .WithOne(e => e.Tinh)
                        .HasForeignKey(e => e.TinhID);

            modelBuilder.Entity<DonViTinh>()
                        .HasMany(e => e.SanPham)
                        .WithOne(e => e.DonViTinh)
                        .HasForeignKey(e => e.DVTID);

            modelBuilder.Entity<NhaThau>()
                        .HasMany(e => e.NT_GiaTriCoHoi)
                        .WithOne(e => e.NhaThau)
                        .HasForeignKey(e => e.NhaThauID);

            modelBuilder.Entity<NguonVon>()
                       .HasMany(e => e.NT_GiaTriCoHoi)
                       .WithOne(e => e.NguonVon)
                       .HasForeignKey(e => e.NguonVonID);

            modelBuilder.Entity<KHCT_Thang>()
                      .HasMany(e => e.KHCT_Tuan)
                      .WithOne(e => e.KHCT_Thang)
                      .HasForeignKey(e => e.ThangID);

            modelBuilder.Entity<KHCT_Tuan>()
                      .HasMany(e => e.KHCT_Ngay)
                      .WithOne(e => e.KHCT_Tuan)
                      .HasForeignKey(e => e.TuanID);

            modelBuilder.Entity<KHCT_Ngay>()
                       .HasMany(e => e.KHCT_Ngay_LichSu)
                       .WithOne(e => e.KHCT_Ngay)
                       .HasForeignKey(e => e.NgayID);

            //modelBuilder.Entity<NT_LoaiSanPham>().HasKey(sc => new { sc.LoaiSanPhamID, sc.GTCHID });

            modelBuilder.Entity<LoaiSanPham>()
                        .HasMany(e => e.NT_GiaTriCoHoi)
                        .WithMany(e => e.LoaiSanPham)
                        .UsingEntity<NT_LoaiSanPham>();

            modelBuilder.Entity<NhaThau>()
                     .HasMany(e => e.NT_DuToan)
                     .WithOne(e => e.NhaThau)
                     .HasForeignKey(e => e.NhaThauID);

            modelBuilder.Entity<NSCoQuan>()
                   .HasMany(e => e.NSDuToan)
                   .WithOne(e => e.NSCoQuan)
                   .HasForeignKey(e => e.CoQuanID);


            base.OnModelCreating(modelBuilder);
        }


        // FuzzySearch
        [DbFunction(name:"SOUNDEX",IsBuiltIn =true)]
        public string FuzzySearch(string query)
        {
            throw new NotImplementedException();
        }
    }
}

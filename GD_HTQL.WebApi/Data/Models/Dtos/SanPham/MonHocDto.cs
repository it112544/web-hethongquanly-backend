﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.SanPham
{
    public class MonHocDto
    {
        public int MonHocID { get; set; }
        public required string TenMonHoc { get; set; }

    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.SanPham
{
    public class DonViTinhDto
    {
        public int DVTID { get; set; }
        public required string TenDVT { get; set; }
    }
}

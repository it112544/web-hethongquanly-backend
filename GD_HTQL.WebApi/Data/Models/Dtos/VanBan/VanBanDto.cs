﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.VanBan
{
    public class VanBanDto
    {
        public int VanBanID { get; set; }
        public string TenVanBan { get; set; }
        public string? SoVanBan { get; set; }
        public string NoiDung { get; set; }
        public string? TrichYeu { get; set; }
        public DateTime? NgayBanHanh { get; set; }
        public DateTime? NgayHetHan { get; set; }
        public string? NguoiKy { get; set; }
        public int LoaiVanBanID { get; set; }
    }
}

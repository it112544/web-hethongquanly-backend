﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.VanBan
{
    public class LoaiVanBanDto
    {
        public int LoaiVanBanID { get; set; }
        public string TenLoaiVanBan { get; set; }
        public string? MoTa { get; set; }
    }
}

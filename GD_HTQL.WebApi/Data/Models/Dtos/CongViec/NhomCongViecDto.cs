﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.CongViec
{
    public class NhomCongViecDto
    {
        public int NhomCongViecID { get; set; }
        public required string TenNhomCongViec { get; set; }
        public string? Mota { get; set; }
    }
}

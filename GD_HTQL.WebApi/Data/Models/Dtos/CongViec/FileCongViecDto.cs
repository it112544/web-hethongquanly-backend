﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.CongViec
{
    public class FileCongViecDto
    {
        public int FileID { get; set; }
        public required string FileName { get; set; }
        public string? FileType { get; set; }
        public string? FileUrl { get; set; }
        public int? Loai { get; set; }
    }
}

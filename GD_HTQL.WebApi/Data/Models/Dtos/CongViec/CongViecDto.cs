﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.CongViec
{
    public class CongViecDto
    {
        public int CongViecID { get; set; }
        public required string TenCongViec { get; set; }
        public string? MoTa { get; set; }
        public DateTime? NgayBatDau { get; set; }
        public DateTime? NgayKetThuc { get; set; }
        public int NhomCongViecID { get; set; }
        public int UuTienID { get; set; }
        public int CongTyID { get; set; }
        public int PhongBanID { get; set; }
        public int ChuVuID { get; set; }
        public int NguoiThucHienID { get; set; }
        public string? NguoiThucHienTen { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.NhaThau
{
    public class LoaiNhaThauDto
    {
        public int LoaiNTID { get; set; }
        public required string TenLoai { get; set; }
    }
}

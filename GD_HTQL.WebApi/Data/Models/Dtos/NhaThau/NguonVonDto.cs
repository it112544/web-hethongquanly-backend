﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.NhaThau
{
    public class NguonVonDto
    {
        public int NguonVonID { get; set; }
        public required string TenNguonVon { get; set; }
    }
}

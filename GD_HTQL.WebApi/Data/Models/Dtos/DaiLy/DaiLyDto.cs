﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.DaiLy
{
    public class DaiLyDto
    {
        public int DaiLyID { get; set; }
        public required string TenDaiLy { get; set; }
        public string? MaSoThue { get; set; }
        public string? DiaChi { get; set; }
        public string? NguoiDaiDien { get; set; }
        public string? NDDChucVu { get; set; }
        public string? NDDSoDienThoai { get; set; }
        public string? GhiChu { get; set; }
        public bool? Active { get; set; }
        public int TinhID { get; set; }
        public string? NhanVienPhuTrach { get; set; }
        public string? ChucVu { get; set; }
        public string? SoDienThoai { get; set; }
        public int LoaiDLID { get; set; }
    }
}

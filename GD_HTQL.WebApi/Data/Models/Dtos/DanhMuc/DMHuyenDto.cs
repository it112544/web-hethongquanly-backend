﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc
{
    public class DMHuyenDto
    {
        public int HuyenID { get; set; }
        public string TenHuyen { get; set; }
        public int TinhID { get; set; }
    }
}

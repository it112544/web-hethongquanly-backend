﻿using GD_HTQL.WebApi.Data.Models.ViewModels;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.Dtos.NghiPhep
{
    public class NghiPhep_LichSuDto
    {
        public int LSID { get; set; }
        public int NghiPhepID { get; set; }
        public int TrangThaiID { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace GD_HTQL.WebApi.Data.Models.Dtos.TacGia
{
    public class TacGiaDto
    {
        public int TacGiaID { get; set; }
        public string TenTacGia { get; set; }
        public DateTime? NgaySinh { get; set; }
        public int? GioiTinh { get; set; }
        public string? CCCD { get; set; }
        public string? SoDienThoai { get; set; }
        public string? Email { get; set; }
        public string? DonViCongTac { get; set; }
        public string? ChucVuTacGia { get; set; }
        public string? MonChuyenNghanh { get; set; }
        public bool? Active { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.NhaCungCap
{
    public class LoaiNhaCungCapDto
    {
        public int LoaiNCCID { get; set; }
        public required string TenLoai { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.NhaCungCap
{
    public class NhaCungCapDto
    {
        public int NhaCungCapID { get; set; }
        public required string TenCongTy { get; set; }
        public string? MaSoThue { get; set; }
        public int TinhID { get; set; }
        public string? DiaChi { get; set; }
        public string? NguoiDaiDien { get; set; }
        public string? NDDChucVu { get; set; }
        public string? NDDSoDienThoai { get; set; }
        public string? GhiChu { get; set; }
        public string? NhanVienPhuTrach { get; set; }
        public string? ChucVu { get; set; }
        public string? SoDienThoai { get; set; }
        public int LoaiNCCID { get; set; }
    }
}

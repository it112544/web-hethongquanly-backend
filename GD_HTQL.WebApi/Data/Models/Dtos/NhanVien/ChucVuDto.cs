﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.NhanVien
{
    public class ChucVuDto
    {
        public int ChucVuID { get; set; }
        public string TenChucVu { get; set; }
        public string? ChiTiet { get; set; }
        public int PhongBanID { get; set; }
    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.NhanVien
{
    public class CongTyDto
    {
        public int CongTyID { get; set; }
        public string TenCongTy { get; set; }
        public string MaSoThue { get; set; }
        public int TinhID { get; set; }
        public string? DiaChi { get; set; }
    }
}

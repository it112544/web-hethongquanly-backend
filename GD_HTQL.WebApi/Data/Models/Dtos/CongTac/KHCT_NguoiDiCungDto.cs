﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.CongTac
{
    public class KHCT_NguoiDiCungDto
    {
        public int ID { get; set; }
        public int NhanVienID { get; set; }
        public string? TenNhanVien { get; set; }
        public string? TenChuVu { get; set; }
        public string? TenPhongBan { get; set; }
        public string? TenCongTy { get; set; }
        public int NgayID { get; set; }
    }
}

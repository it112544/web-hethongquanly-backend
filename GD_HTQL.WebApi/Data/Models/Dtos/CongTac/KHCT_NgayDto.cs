﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.CongTac
{
    public class KHCT_NgayDto
    {
        public int NgayID { get; set; }
        //public int NguoiTaoID { get; set; } 
        //public string? HoTen { get; set; }
        //public int ChucVuID { get; set; }
        //public string? TenChucVu { get; set; }
        //public int PhongBanID { get; set; }
        //public string? TenPhongBan { get; set; }
        //public int CongTyID { get; set; }
        //public string? TenCongTy { get; set; }
        public DateTime tuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public string? MucDich { get; set; }
        public int? TuanID { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet { get; set; }
        public int? KHCTCongTyID { get; set; }
        public string? KHCTTenCongTy { get; set; }
    }
}

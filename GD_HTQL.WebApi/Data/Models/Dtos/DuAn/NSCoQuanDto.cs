﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.DuAn
{
    public class NSCoQuanDto
    {
        public int CoQuanID { get; set; }
        public required string TenCoQuan { get; set; }
        public string? MaSoThue { get; set; }
        public int HuyenID { get; set; }
        public int? TinhID { get; set; }
        public int? XaID { get; set; }
        public string? DiaChi { get; set; }
    }
}

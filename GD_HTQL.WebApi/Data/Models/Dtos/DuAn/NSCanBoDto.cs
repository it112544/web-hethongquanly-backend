﻿namespace GD_HTQL.WebApi.Data.Models.Dtos.DuAn
{
    public class NSCanBoDto
    {
        public int CanBoID { get; set; }
        public string HoVaTen { get; set; }
        public int GioiTinh { get; set; }
        public string? ChucVu { get; set; }
        public string? SoDienThoai { get; set; }
        public string? Email { get; set; }
        public int CoQuanID { get; set; }
    }
}

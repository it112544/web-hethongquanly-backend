﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.Dtos.DuAn
{
    public class NSDuToanDto
    {
        public int DuToanID { get; set; }
        public required string TenDuToan { get; set; }
        public string? FileCoQuan { get; set; }
        public string? TenFileCoQuan { get; set; }
        public string? FileCongTy { get; set; }
        public string? TenFileCongTy { get; set; }
        public decimal? DoanhThuDuKien { get; set; }
        public string? GhiChu { get; set; }
        public int BuocThiTruongID { get; set; }
        public int CoQuanID { get; set; }
        public bool? KetQua { get; set; }
        public string? LyDoThatBai { get; set; }
        public decimal? DoanhThuThucTe { get; set; }
        public DateTime? ThoiGian { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class QuanLyNhanVienCustom
    {
        public required List<int> lstNhanVienID { get; set; }
        public int QuanLyID { get; set; }
    }
}

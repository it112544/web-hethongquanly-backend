﻿namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class DateOfWeekModel
    {
        public int Tuan { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;

namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class NhanVienModel
    {
        public required NhanVienDto NhanVienDto { get; set; }
        public List<FileDinhKemDto>? FileDinhKemDto { get; set; }
    }
}

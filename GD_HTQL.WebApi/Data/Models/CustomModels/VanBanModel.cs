﻿using GD_HTQL.WebApi.Data.Models.Dtos.VanBan;

namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class VanBanModel
    {
        public VanBanDto VanBanDto { get; set; }
        public List<IFormFile>? lstFile { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;

namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class SanPhamModel
    {
        public required SanPhamDto SanPhamDto { get; set;}
        public IFormFile? HinhAnh { get; set; }

    }
}

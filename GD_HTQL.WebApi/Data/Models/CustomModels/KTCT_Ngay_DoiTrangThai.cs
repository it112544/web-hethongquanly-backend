﻿namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class KTCT_Ngay_DoiTrangThai
    {
        public int NgayID { get; set; }
        public int TrangThaiID { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet {  get; set; }
    }
}

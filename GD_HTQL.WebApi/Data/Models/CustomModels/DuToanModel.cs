﻿using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;

namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class DuToanModel
    {      
        public DuToanDto DuToanDto { get; set; }
        public int NhanVienID { get; set; }
    }
}

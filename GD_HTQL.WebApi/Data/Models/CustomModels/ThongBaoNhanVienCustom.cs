﻿namespace GD_HTQL.WebApi.Data.Models.CustomModels
{
    public class ThongBaoNhanVienCustom
    {
        public required List<int> lstNhanVienID { get; set; }
        public int ThongBaoID { get; set; }
    }
}

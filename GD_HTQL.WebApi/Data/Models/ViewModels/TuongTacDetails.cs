﻿using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class TuongTacDetails
    {
        public int TuongTacID { get; set; }
        public virtual NSCoQuanDto? NSCoQuan { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string? ThongTinLienHe { get; set; }
        public string? ThongTinTiepXuc { get; set; }
        public string? CanBoTiepXuc { get; set; }
        public string? NhomHangQuanTam { get; set; }
        public string? GhiChu { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? DoanhThuDuKien { get; set; }
        public int NhanVienID { get; set; }
        public virtual BuocThiTruongDto? BuocThiTruong { get; set; }
        public virtual NhanVienShortView? NhanVien { get; set; }
        public int? NguonVonID { get; set; }
        public virtual NSNguonVon? NSNguonVon { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
        public virtual ICollection<ChucVuView>? lstChucVuView { get; set; } = new List<ChucVuView>();
    }
}

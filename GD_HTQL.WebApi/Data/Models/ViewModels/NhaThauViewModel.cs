﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NhaThauViewModel
    {
        public int NhaThauID { get; set; }       
        public required string TenCongTy { get; set; }       
        public string? MaSoThue { get; set; }      
        public string? DiaChi { get; set; }      
        public string? NguoiDaiDien { get; set; }     
        public string? NDDChucVu { get; set; }       
        public string? NDDSoDienThoai { get; set; }
        public string? GhiChu { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
        public int TinhID { get; set; }
        public virtual DMTinhDto? Tinh { get; set; }      
        public string? NhanVienPhuTrach { get; set; } 
        public string? ChucVu { get; set; }      
        public string? SoDienThoai { get; set; }
        public int LoaiNTID { get; set; }
        public virtual LoaiNhaThauDto? LoaiNhaThau { get; set; }
        public int? NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVienDto? NhanVien { get; set; }           
        public string? Email { get; set; }
        public int? LoaiHopTacID { get; set; }
        public virtual NT_LoaiHopTac? NT_LoaiHopTac { get; set; }
        public int? DiaBanID { get; set; }
        public virtual NT_DiaBanHoatDong? NT_DiaBanHoatDong { get; set; }
        public virtual List<NT_DuToanView>? NT_DuToan {  get; set; }
        public virtual List<GiaTriCoHoiViewModel>? NT_GiaTriCoHoi { get; set; }
    }
}

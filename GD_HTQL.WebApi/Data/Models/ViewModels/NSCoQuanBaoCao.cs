﻿using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NSCoQuanBaoCao
    {
        public int CoQuanID { get; set; }
        public string? TenCoQuan { get; set; }      
        public DateTime? ThoiGian { get; set; }
        public decimal? DoanhThuThucTe { get; set; }
        public decimal? DoanhThuDuKien { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
        public virtual BuocThiTruongDto? BuocThiTruong { get; set; }
        public virtual NhanVienInfo? NhanVien { get; set; }

    }
}

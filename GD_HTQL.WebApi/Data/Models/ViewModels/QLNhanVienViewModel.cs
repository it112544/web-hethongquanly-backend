﻿namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class QLNhanVienViewModel
    {
        public int NhanVienID { get; set; }
        public required string TenNhanVien {  get; set; }
        public string? SoDienThoai { get; set; }
        public string? Email {  get; set; }
    }
}

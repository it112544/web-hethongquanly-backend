﻿namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NhanVienInfo
    {
        public int NhanVienID { get; set; }
        public required string TenNhanVien { get; set; }
        public virtual ICollection<ChucVuView> lstChucVuView { get; set; } = new List<ChucVuView>();
    }
}

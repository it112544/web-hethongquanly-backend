﻿using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class SanPhamViewModel
    {
        public int SanPhamID { get; set; }        
        public required string MaSanPham { get; set; }      
        public required string TenSanPham { get; set; }     
        public required string TieuChuanKyThuat { get; set; }      
        public string? HangSanXuat { get; set; }    
        public string? XuatXu { get; set; }
        public int? BaoHanh { get; set; }
        public int? NamSanXuat { get; set; }      
        public string? NhaXuatBan { get; set; }
        public DonViTinhDto? DonViTinh { get; set; }
        public Decimal? GiaVon { get; set; }     
        public Decimal? GiaDaiLy { get; set; }      
        public Decimal? GiaTTR { get; set; }      
        public Decimal? GiaTH_TT { get; set; }       
        public Decimal? GiaCDT { get; set; }      
        public Decimal? Thue { get; set; }
        public int? SoLuong { get; set; }
        public string? DoiTuongSuDung { get; set; }
        public string? ThuongHieu { get; set; }
        public LoaiSanPhamDto? LoaiSanPham { get; set; }
        public string? HinhAnh { get; set; }
        public string? Model { get; set; }
        public virtual ICollection<MonHocDto>? MonHoc { get; set; }
        public virtual ICollection<KhoiLopDto>? KhoiLop { get; set; }
        public virtual ICollection<ThongTuDto>? ThongTu { get; set; }
    }
}

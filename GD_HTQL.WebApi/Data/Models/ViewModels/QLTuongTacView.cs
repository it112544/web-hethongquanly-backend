﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.Dtos.DuAn;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class QLTuongTacView
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TuongTacID { get; set; }         
        public DateTime? ThoiGian { get; set; }
        public string? ThongTinLienHe { get; set; }
        public string? ThongTinTiepXuc { get; set; }
        public string? CanBoTiepXuc { get; set; }
        public string? NhomHangQuanTam { get; set; }
        public string? GhiChu { get; set; }
        public decimal? DoanhThuDuKien { get; set; }
        public int BuocThiTruongID { get; set; }
        public virtual BuocThiTruongDto? BuocThiTruong { get; set; }
        public int NhanVienID { get; set; }
        public virtual NhanVienShortView? NhanVien { get; set; }
        public int? NguonVonID { get; set; }
        public virtual NSNguonVon? NSNguonVon { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class ThongTuViewModel
    {
        public int ThongTuID { get; set; }
        public required string TenThongTu { get; set; }
        public string? MoTa { get; set; }
       public List<SanPhamDto>? lstSanPham { get; set; }
        public List<MonHocDto>? lstMonHoc { get; set; }
    }
}

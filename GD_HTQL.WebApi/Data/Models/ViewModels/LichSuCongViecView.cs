﻿using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class LichSuCongViecView
    {
       
        public int LSID { get; set; }
        public int NhanVienID { get; set; }     
        public virtual NhanVienShortView? NhanVien { get; set; }
        public string? MoTa { get; set; }
        public DateTime ThoiGian { get; set; }
        public int TrangThaiID { get; set; }
        public virtual TrangThaiCongViec? TrangThaiCongViec { get; set; }
        public int? TienDo { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Dtos.CongViec;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class CongViecViewModel
    {
        public int CongViecID { get; set; }      
        public  string? TenCongViec { get; set; }
        public string? MoTa { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayKetThuc { get; set; }
        public int UuTienID { get; set; }     
        public string? TenDoUuTien { get; set; }
        public int NhomCongViecID { get; set; }
        public string? TenNhomCongViec { get; set; }
        public int CongTyID { get; set; }
        public string? TenCongTy { get; set; }
        public int PhongBanID { get; set; }
        public string? TenPhongBan { get; set; }
        public int ChuVuID { get; set; }
        public string? TenChucVu { get; set; }
        public int NguoiTaoID { get; set; }
        public string? TenNguoiTao { get; set; }
        public int NguoiThucHienID { get; set; }
        public string? NguoiThucHienTen { get; set; }
        public virtual List<FileCongViecDto>? FileCongViec { get; set; }
        public virtual List<LichSuCongViecView>? LichSuCongViec { get; set; }
    }
}

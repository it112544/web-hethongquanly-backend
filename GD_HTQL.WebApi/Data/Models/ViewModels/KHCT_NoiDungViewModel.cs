﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class KHCT_NoiDungViewModel
    {
        public int ID { get; set; }
        public string? NoiDen { get; set; }
        public string? TenBuocThiTruong { get; set; }
        public string? ChiTiet { get; set; }
        public DateTime? NgayThucHien { get; set; }
        public string? GhiChu { get; set; }
        public int CoQuanID { get; set; }
        public virtual NSCoQuan? NSCoQuan { get; set; }
        public int NgayID { get; set; }
    }
}

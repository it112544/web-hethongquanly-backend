﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NhanVienShortView
    {
        public int NhanVienID { get; set; }
        public required string TenNhanVien { get; set; }
        public string? SoDienThoai { get; set; }
        public string? Email { get; set; }
    }
}

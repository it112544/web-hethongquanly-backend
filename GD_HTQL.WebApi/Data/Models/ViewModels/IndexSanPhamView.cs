﻿namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class IndexSanPhamView
    {
        public int index { get; set; }
        public string? productName { get; set; }
        public virtual List<SanPhamViewDto>? SanPhamView { get; set;}
    }
}

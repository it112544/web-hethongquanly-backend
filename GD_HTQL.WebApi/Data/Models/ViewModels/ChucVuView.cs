﻿using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class ChucVuView
    {
        public ChucVuDto? lstChucVu { get; set; }
        public PhongBanDto? lstPhongBan { get; set; }
        public CongTyDto? lstCongTy { get; set; }
    }
}

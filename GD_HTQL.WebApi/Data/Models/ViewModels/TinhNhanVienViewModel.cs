﻿namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class TinhNhanVienViewModel
    {
        public int ID { get; set; }
        public int NhanVienID { get; set; }
        public int TinhID { get; set; }
        public string? TenTinh { get; set; }
    }
}

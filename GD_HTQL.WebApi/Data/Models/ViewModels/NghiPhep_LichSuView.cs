﻿using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;

namespace GD_HTQL.WebApi.Data.Models.ViewModels
{
    public class NghiPhep_LichSuView
    {
        public int LSID { get; set; }
        public int NhanVienID { get; set; }
        public virtual NhanVienShortView? NhanVien { get; set; }
        public DateTime ThoiGan { get; set; }
        public int TrangThaiID { get; set; }
        public virtual NghiPhep_TrangThai? NghiPhep_TrangThai { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet { get; set; }
    }
}

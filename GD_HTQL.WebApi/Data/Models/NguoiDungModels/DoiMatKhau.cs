﻿using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Models.NguoiDungModels
{
    public class DoiMatKhau
    {
        public int nhanVienID { get; set; }
        public required string MatKhauCu { get; set; }     
        public required string MatKhauMoi { get; set; }      
        
    }
}

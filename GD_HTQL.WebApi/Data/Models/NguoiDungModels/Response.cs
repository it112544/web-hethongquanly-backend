﻿namespace GD_HTQL.WebApi.Models.NguoiDungModels
{
    public class Response
    {
        public string? Status { get; set; }
        public string? Message { get; set; }
    }
}

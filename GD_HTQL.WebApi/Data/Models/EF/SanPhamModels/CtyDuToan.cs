﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class CtyDuToan
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        [StringLength(225)]
        public required string TenCongTy { get; set; }
        public string? HinhAnh { get; set; }
    }
}

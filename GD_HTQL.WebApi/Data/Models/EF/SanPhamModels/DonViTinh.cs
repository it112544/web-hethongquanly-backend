﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class DonViTinh
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int DVTID { get; set; }
        public required string TenDVT { get; set; }
        public virtual ICollection<SanPham>? SanPham { get; set; }
    }
}

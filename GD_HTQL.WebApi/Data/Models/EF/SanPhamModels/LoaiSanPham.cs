﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class LoaiSanPham
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LoaiSanPhamID { get; set; }
        [StringLength(225)]
        public required string TenLoaiSanPham { get; set; }
        public virtual ICollection<SanPham>? SanPham { get; set; }
        public virtual ICollection<NT_GiaTriCoHoi>? NT_GiaTriCoHoi { get; set; }
    }
}

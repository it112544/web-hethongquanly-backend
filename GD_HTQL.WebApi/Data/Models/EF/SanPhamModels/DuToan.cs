﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class DuToan : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int DuToanID { get; set; }
        [StringLength(225)]
        public required string TenDuToan { get; set; }
        public string? GhiChu { get; set; }
        [DefaultValue(true)]
        public bool Active { get; set; }
        public string? SanPhamDuToan { get; set; }
        [StringLength(50)]
        public string? LoaiGia { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? TongGiaVon { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? TongGiaBan { get; set; }
        public virtual ICollection<LichSuDuToan>? LichSuDuToan { get; set; }
        public int? NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
    }
}

﻿namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class SanPhamThongTu
    {
        public int SanPhamID { get; set; }
        public int ThongTuID { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class SanPham : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int SanPhamID { get; set; }
        [StringLength(225)]
        public required string MaSanPham { get; set; }
        [StringLength(225)]
        public required string TenSanPham { get; set; }

        [DisplayFormat(HtmlEncode = true)]
        public required string TieuChuanKyThuat { get; set; }
        [StringLength(225)]
        public string? HangSanXuat { get; set; }
        [StringLength(225)]
        public string? XuatXu { get; set; }
        public int? BaoHanh { get; set; }
        public int? NamSanXuat { get; set; }
        [StringLength(225)]
        public string? NhaXuatBan { get; set; }

        [Column(TypeName = "decimal(18,0)")]
        public decimal? GiaVon { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? GiaDaiLy { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? GiaTTR { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? GiaTH_TT { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? GiaCDT { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? Thue { get; set; }
        public int SoLuong { get; set; }
        public string? DoiTuongSuDung { get; set; }
        public string? ThuongHieu { get; set; }
        public int? LoaiSanPhamID { get; set; }
        public string? HinhAnh { get; set; }
        public string? Model { get; set; }
        public int? DVTID { get; set; }
        public virtual DonViTinh? DonViTinh { get; set; }
        public virtual LoaiSanPham? LoaiSanPham { get; set; }
        public virtual ICollection<MonHoc>? MonHoc { get; set; }
        public virtual ICollection<KhoiLop>? KhoiLop { get; set; }
        public virtual ICollection<ThongTu>? ThongTu { get; set; }
        [DefaultValue(true)]
        public bool Active { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace GD_HTQL.WebApi.Data.Models.EF.SanPhamModels
{
    public class ThongTu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ThongTuID { get; set; }
        [StringLength(150)]
        public required string TenThongTu { get; set; }
        public virtual ICollection<SanPham>? SanPham { get; set; }

        [DefaultValue(true)]
        public bool Active { get; set; }
        public string? MoTa { get; set; }
    }
}

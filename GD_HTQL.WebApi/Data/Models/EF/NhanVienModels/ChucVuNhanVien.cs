﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.NhanVienModels
{
    public class ChucVuNhanVien
    {
        public int ChucVuID { get; set; }
        public int NhanVienID { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.NhanVienModels
{
    public class ChucVu : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ChucVuID { get; set; }
        [StringLength(50)]
        public required string TenChucVu { get; set; }
        public string? ChiTiet { get; set; }
        public int PhongBanID { get; set; }
        public bool Active { get; set; }
        public required virtual PhongBan PhongBan { get; set; }
        public virtual ICollection<NhanVien>? NhanVien { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.TacGiaModels
{
    public class TacGia : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TacGiaID { get; set; }
        [StringLength(225)]
        public required string TenTacGia { get; set; }
        public DateTime? NgaySinh { get; set; }
        public int? GioiTinh { get; set; }
        [StringLength(50)]
        public string? CCCD { get; set; }
        [StringLength(50)]
        public string? SoDienThoai { get; set; }
        [StringLength(225)]
        public string? Email { get; set; }
        [StringLength(225)]
        public string? DonViCongTac { get; set; }
        [StringLength(225)]
        public string? ChucVuTacGia { get; set; }
        [StringLength(225)]
        public string? MonChuyenNghanh { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
        public int? NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
    }
}

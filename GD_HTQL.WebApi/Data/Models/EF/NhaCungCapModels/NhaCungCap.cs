﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels
{
    public class NhaCungCap : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NhaCungCapID { get; set; }
        [StringLength(225)]
        public required string TenCongTy { get; set; }
        [StringLength(225)]
        public string? MaSoThue { get; set; }
        public string? DiaChi { get; set; }
        [StringLength(500)]
        public string? NguoiDaiDien { get; set; }
        [StringLength(225)]
        public string? NDDChucVu { get; set; }
        [StringLength(225)]
        public string? NDDSoDienThoai { get; set; }
        public string? Ghichu { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
        public int TinhID { get; set; }
        public virtual DMTinh? Tinh { get; set; }
        [StringLength(500)]
        public string? NhanVienPhuTrach { get; set; }
        [StringLength(225)]
        public string? ChucVu { get; set; }
        [StringLength(225)]
        public string? SoDienThoai { get; set; }
        public int LoaiNCCID { get; set; }
        public virtual LoaiNhaCungCap? LoaiNhaCungCap { get; set; }
        public int? NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
    }
}

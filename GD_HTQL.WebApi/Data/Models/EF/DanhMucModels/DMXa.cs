﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.DanhMucModels
{
    public class DMXa
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int XaID { get; set; }
        [StringLength(50)]
        public required string TenXa { get; set; }
        public int HuyenID { get; set; }
        public required virtual DMHuyen DMHuyen { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace GD_HTQL.WebApi.Data.Models.EF.DanhMucModels
{
    public class DMHuyen
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int HuyenID { get; set; }
        [StringLength(50)]
        public string TenHuyen { get; set; }
        public int TinhID { get; set; }
        public required virtual DMTinh DMTinh { get; set; }
        public virtual ICollection<DMXa>? DMXa { get; set; }
        public virtual ICollection<NSCoQuan>? NSCoQuan { get; set; }
    }
}

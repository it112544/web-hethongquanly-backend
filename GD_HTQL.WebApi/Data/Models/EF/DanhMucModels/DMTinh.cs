﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;

namespace GD_HTQL.WebApi.Data.Models.EF.DanhMucModels
{
    public class DMTinh
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TinhID { get; set; }
        [StringLength(50)]
        public string TenTinh { get; set; }
        public int KhuVucID { get; set; }
        public virtual DMKhuVuc? DMKhuVuc { get; set; }
        public virtual ICollection<DMHuyen>? DMHuyen { get; set; }
        public virtual ICollection<NhaThau>? NhaThau { get; set; }
        public virtual ICollection<NhaCungCap>? NhaCungCap { get; set; }
        public virtual ICollection<DaiLy>? DaiLy { get; set; }
    }
}

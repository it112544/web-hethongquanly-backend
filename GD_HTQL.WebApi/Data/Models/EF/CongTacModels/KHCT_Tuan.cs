﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.CongTacModels
{
    public class KHCT_Tuan : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TuanID { get; set; }
        [StringLength(500)]
        public string? TieuDe { get; set; }
        public string? NoiDung { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
        public int ThangID { get; set; }
        public int Tuan_Thang { get; set; }
        public int? Tuan_Nam { get; set; }
        public int Nam { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public bool? IsApprove { get; set; }
        public int? NguoiTaoID { get; set; }
        public int? NguoiDuyetID { get; set; }
        [StringLength(50)]
        public string? NguoiDuyet { get; set; }
        public virtual KHCT_Thang? KHCT_Thang { get; set; }
        public virtual ICollection<KHCT_Ngay>? KHCT_Ngay { get; set; }
    }
}

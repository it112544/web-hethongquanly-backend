﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;

namespace GD_HTQL.WebApi.Data.Models.EF.CongTacModels
{
    public class KHCT_NoiDung
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        [StringLength(500)]
        public string? NoiDen { get; set; }
        [StringLength(225)]
        public string? TenBuocThiTruong { get; set; }
        public string? ChiTiet { get; set; }
        public DateTime? NgayThucHien { get; set; }
        public string? GhiChu { get; set; }
        public int CoQuanID { get; set; }
        [ForeignKey("CoQuanID")]
        public virtual NSCoQuan? NSCoQuan { get; set; }
        public int NgayID { get; set; }
        [ForeignKey("NgayID")]
        public virtual KHCT_Ngay? KHCT_Ngay { get; set; }
    }
}

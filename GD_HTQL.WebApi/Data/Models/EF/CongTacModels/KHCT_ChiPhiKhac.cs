﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.CongTacModels
{
    public class KHCT_ChiPhiKhac
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        [StringLength(500)]
        public string? HangMuc { get; set; }
        public int SoLuong { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? DonGia { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? ThanhTien { get; set; }
        public string? GhiChu { get; set; }
        public int NgayID { get; set; }
        [ForeignKey("NgayID")]
        public virtual KHCT_Ngay? KHCT_Ngay { get; set; }
    }
}

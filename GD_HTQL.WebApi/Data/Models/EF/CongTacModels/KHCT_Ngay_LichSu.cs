﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;

namespace GD_HTQL.WebApi.Data.Models.EF.CongTacModels
{
    public class KHCT_Ngay_LichSu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LSID { get; set; }
        public int NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
        public int NgayID { get; set; }
        public virtual KHCT_Ngay? KHCT_Ngay { get; set; }
        public DateTime ThoiGan { get; set; }
        public int TrangThaiID { get; set; }

        [ForeignKey("TrangThaiID")]
        public virtual KHCT_Ngay_TrangThai? KHCT_Ngay_TrangThai { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet { get; set; }
    }
}

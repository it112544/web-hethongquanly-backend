﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.CongTacModels
{
    public class KHCT_Thang : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ThangID { get; set; }
        [StringLength(500)]
        public string? TieuDe { get; set; }
        public string? NoiDung { get; set; }
        public int Thang { get; set; }
        public int Nam { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public bool? IsApprove { get; set; }
        public int? NguoiTaoID { get; set; }
        public int? NguoiDuyetID { get; set; }
        [StringLength(50)]
        public string? NguoiDuyet { get; set; }

        [DefaultValue(true)]
        public bool? Active { get; set; }
        public virtual ICollection<KHCT_Tuan>? KHCT_Tuan { get; set; }
    }
}

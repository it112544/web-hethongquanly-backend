﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.CongTacModels
{
    public class KHCT_NguoiDiCung
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }
        public int NhanVienID { get; set; }
        [StringLength(500)]
        public string? TenNhanVien { get; set; }
        public string? TenChuVu { get; set; }
        public string? TenPhongBan { get; set; }
        public string? TenCongTy { get; set; }
        public int NgayID { get; set; }
        [ForeignKey("NgayID")]
        public virtual KHCT_Ngay? KHCT_Ngay { get; set; }
    }
}

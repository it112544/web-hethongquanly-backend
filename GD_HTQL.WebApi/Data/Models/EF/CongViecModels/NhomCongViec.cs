﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.CongViecModels
{
    public class NhomCongViec : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NhomCongViecID { get; set; }
        [StringLength(225)]
        public required string TenNhomCongViec { get; set; }
        public string? Mota { get; set; }
        public virtual ICollection<CongViec>? CongViec { get; set; }
        [DefaultValue(true)]
        public bool Active { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;

namespace GD_HTQL.WebApi.Data.Models
{
    public class FileCongViec
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int FileID { get; set; }
        [StringLength(225)]
        public required string FileName { get; set; }
        [StringLength(225)]
        public string? FileType { get; set; }
        [StringLength(225)]
        public string? FileUrl { get; set; }
        public int? Loai { get; set; }
        public int CongViecID { get; set; }
        [ForeignKey("CongViecID")]
        public virtual CongViec? CongViec { get; set; }
    }
}

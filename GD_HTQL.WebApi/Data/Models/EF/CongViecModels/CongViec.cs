﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.CongViecModels
{
    public class CongViec : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int CongViecID { get; set; }
        [StringLength(225)]
        public required string TenCongViec { get; set; }
        public string? MoTa { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayKetThuc { get; set; }
        public int UuTienID { get; set; }
        [ForeignKey("UuTienID")]
        public virtual DoUuTien? DoUuTien { get; set; }
        public int CongTyID { get; set; }
        public int NhomCongViecID { get; set; }
        public virtual NhomCongViec? NhomCongViec { get; set; }
        public int PhongBanID { get; set; }
        public int ChuVuID { get; set; }
        public int NguoiTaoID { get; set; }
        public int NguoiThucHienID { get; set; }
        [StringLength(50)]
        public string? NguoiThucHienTen { get; set; }

        [DefaultValue(true)]
        public bool Active { get; set; }
        public virtual ICollection<LichSuCongViec>? LichSuCongViec { get; set; }
    }
}


﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaThauModels
{
    public class NT_LoaiSanPham
    {
        [ForeignKey(nameof(NT_GiaTriCoHoi))]
        public int GTCHID { get; set; }
        public virtual NT_GiaTriCoHoi? NT_GiaTriCoHoi { get; set; }
        [ForeignKey(nameof(LoaiSanPham))]
        public int LoaiSanPhamID { get; set; }
        public virtual LoaiSanPham? LoaiSanPham { get; set; }




    }
}

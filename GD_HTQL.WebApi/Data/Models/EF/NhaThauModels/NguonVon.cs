﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaThauModels
{
    public class NguonVon
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NguonVonID { get; set; }
        [StringLength(50)]
        public required string TenNguonVon { get; set; }
        public virtual ICollection<NT_GiaTriCoHoi>? NT_GiaTriCoHoi { get; set; }
    }
}

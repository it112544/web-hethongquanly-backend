﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaThauModels
{
    public class NhaThau : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NhaThauID { get; set; }
        [StringLength(225)]
        public required string TenCongTy { get; set; }
        [StringLength(225)]
        public string? MaSoThue { get; set; }
        [StringLength(225)]
        public string? DiaChi { get; set; }

        [StringLength(500)]
        public string? NguoiDaiDien { get; set; }
        [StringLength(225)]
        public string? NDDChucVu { get; set; }
        [StringLength(225)]
        public string? NDDSoDienThoai { get; set; }
        public string? GhiChu { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
        public int? TinhID { get; set; }
        public virtual DMTinh? Tinh { get; set; }
        [StringLength(500)]
        public string? NhanVienPhuTrach { get; set; }
        [StringLength(225)]
        public string? ChucVu { get; set; }
        [StringLength(225)]
        public string? SoDienThoai { get; set; }
        public int LoaiNTID { get; set; }
        public virtual LoaiNhaThau? LoaiNhaThau { get; set; }
        public int? NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
        public virtual ICollection<NT_GiaTriCoHoi>? NT_GiaTriCoHoi { get; set; }
        public virtual ICollection<NT_DuToan>? NT_DuToan { get; set; }
        // Thêm mới
        [StringLength(500)]
        public string? Email { get; set; }
        public int? LoaiHopTacID { get; set; }
        [ForeignKey("LoaiHopTacID")]
        public virtual NT_LoaiHopTac? NT_LoaiHopTac { get; set; }
        public int? DiaBanID { get; set; }
        [ForeignKey("DiaBanID")]
        public virtual NT_DiaBanHoatDong? NT_DiaBanHoatDong { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.NhaThauModels
{
    public class NT_DuToan : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int DuToanID { get; set; }
        [StringLength(225)]
        public required string TenDuToan { get; set; }
        [StringLength(500)]
        public string? DiaChi { get; set; }
        [StringLength(225)]
        public string? FileNhaThau { get; set; }
        [StringLength(225)]
        public string? TenFileNhaThau { get; set; }
        [StringLength(225)]
        public string? FileCongTy { get; set; }
        [StringLength(225)]
        public string? TenFileCongTy { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? DoanhThuDuKien { get; set; }
        public string? GhiChu { get; set; }
        public int? TinhID { get; set; }
        [ForeignKey("TinhID")]
        public virtual DMTinh? Tinh { get; set; }
        public int TrangThaiID { get; set; }
        [ForeignKey("TrangThaiID")]
        public virtual NT_TrangThaiDuToan? NT_TrangThaiDuToan { get; set; }
        public int NhaThauID { get; set; }
        public virtual NhaThau? NhaThau { get; set; }
        public bool? KetQua { get; set; }
        [StringLength(500)]
        public string? LyDoThatBai { get; set; }
        public int NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.DuAnModels
{
    public class QuanLyTuongTac : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TuongTacID { get; set; }
        public int CoQuanID { get; set; }
        public virtual NSCoQuan? NSCoQuan { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string? ThongTinLienHe { get; set; }
        public string? ThongTinTiepXuc { get; set; }
        public string? CanBoTiepXuc { get; set; }
        public string? NhomHangQuanTam { get; set; }
        public string? GhiChu { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? DoanhThuDuKien { get; set; }
        public int BuocThiTruongID { get; set; }
        public virtual BuocThiTruong? BuocThiTruong { get; set; }
        public int NhanVienID { get; set; }
        public virtual NhanVien? NhanVien { get; set; }
        public int? NguonVonID { get; set; }
        [ForeignKey("NguonVonID")]
        public virtual NSNguonVon? NSNguonVon { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
    }
}

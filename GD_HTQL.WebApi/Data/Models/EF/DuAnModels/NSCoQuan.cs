﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.DuAnModels
{
    public class NSCoQuan : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int CoQuanID { get; set; }

        [StringLength(225)]
        public required string TenCoQuan { get; set; }
        [StringLength(50)]
        public string? MaSoThue { get; set; }
        public int? TinhID { get; set; }
        public int? HuyenID { get; set; }
        public int? XaID { get; set; }
        public virtual DMHuyen? Huyen { get; set; }
        [StringLength(225)]
        public string? DiaChi { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
        public virtual ICollection<NSCanBo>? NSCanBo { get; set; }
        public virtual ICollection<QuanLyTuongTac>? TuongTac { get; set; }
        public int? NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
        public virtual ICollection<NSDuToan>? NSDuToan { get; set; }
    }
}

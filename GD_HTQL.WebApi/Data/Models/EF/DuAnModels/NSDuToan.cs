﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.Abstract;

namespace GD_HTQL.WebApi.Data.Models.EF.DuAnModels
{
    public class NSDuToan : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int DuToanID { get; set; }
        [StringLength(225)]
        public required string TenDuToan { get; set; }
        [StringLength(225)]
        public string? FileCoQuan { get; set; }
        [StringLength(225)]
        public string? TenFileCoQuan { get; set; }
        [StringLength(225)]
        public string? FileCongTy { get; set; }
        [StringLength(225)]
        public string? TenFileCongTy { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? DoanhThuDuKien { get; set; }
        public string? GhiChu { get; set; }
        public DateTime? ThoiGian { get; set; }
        public int BuocThiTruongID { get; set; }
        [ForeignKey("BuocThiTruongID")]
        public virtual BuocThiTruong? BuocThiTruong { get; set; }
        public int CoQuanID { get; set; }
        public virtual NSCoQuan? NSCoQuan { get; set; }
        public int NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
        public bool? KetQua { get; set; }
        [StringLength(500)]
        public string? LyDoThatBai { get; set; }
        [Column(TypeName = "decimal(18,0)")]
        public decimal? DoanhThuThucTe { get; set; }
        public DateTime? ThoiGianKetThucDuKien { get; set; }
    }
}

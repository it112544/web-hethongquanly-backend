﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.DuAnModels
{
    public class NSCanBo : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int CanBoID { get; set; }
        [StringLength(225)]
        public required string HoVaTen { get; set; }
        public int GioiTinh { get; set; }
        public string? ChucVu { get; set; }
        [StringLength(50)]
        public string? SoDienThoai { get; set; }
        [StringLength(225)]
        public string? Email { get; set; }
        public int CoQuanID { get; set; }
        public bool? Active { get; set; }
        public virtual NSCoQuan NSCoQuan { get; set; }
    }
}

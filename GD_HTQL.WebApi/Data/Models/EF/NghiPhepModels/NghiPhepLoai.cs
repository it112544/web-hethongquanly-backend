﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels
{
    public class NghiPhepLoai
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LoaiID { get; set; }
        [StringLength(225)]
        public required string TenLoaiNghiPhep { get; set; }
    }
}

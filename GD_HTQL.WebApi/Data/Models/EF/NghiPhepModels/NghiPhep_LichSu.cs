﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;

namespace GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels
{
    public class NghiPhep_LichSu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LSID { get; set; }
        public int NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }
        public int NghiPhepID { get; set; }
        [ForeignKey("NghiPhepID")]
        public virtual NghiPhep? NghiPhep { get; set; }
        public DateTime ThoiGan { get; set; }
        public int TrangThaiID { get; set; }

        [ForeignKey("TrangThaiID")]
        public virtual NghiPhep_TrangThai? NghiPhep_TrangThai { get; set; }
        public int? NguoiDuyetID { get; set; }
        public string? TenNguoiDuyet { get; set; }
    }
}

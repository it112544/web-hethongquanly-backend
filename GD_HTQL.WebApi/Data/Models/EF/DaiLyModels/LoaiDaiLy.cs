﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.DaiLyModels
{
    public class LoaiDaiLy
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LoaiDLID { get; set; }
        [StringLength(50)]
        public required string TenLoai { get; set; }
        public virtual ICollection<DaiLy>? DaiLy { get; set; }
    }
}

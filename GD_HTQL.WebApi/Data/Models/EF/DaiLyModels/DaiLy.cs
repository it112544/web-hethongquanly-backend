﻿using GD_HTQL.WebApi.Data.Models.Abstract;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GD_HTQL.WebApi.Data.Models.EF.DaiLyModels
{
    public class DaiLy : CommonAbstract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int DaiLyID { get; set; }
        [StringLength(225)]
        public required string TenDaiLy { get; set; }
        [StringLength(225)]
        public string? MaSoThue { get; set; }
        [StringLength(225)]
        public string? DiaChi { get; set; }
        [StringLength(500)]
        public string? NguoiDaiDien { get; set; }
        [StringLength(225)]
        public string? NDDChucVu { get; set; }
        [StringLength(225)]
        public string? NDDSoDienThoai { get; set; }
        public string? GhiChu { get; set; }
        [DefaultValue(true)]
        public bool? Active { get; set; }
        public int TinhID { get; set; }
        public virtual DMTinh? Tinh { get; set; }
        [StringLength(500)]
        public string? NhanVienPhuTrach { get; set; }
        [StringLength(225)]
        public string? ChucVu { get; set; }
        [StringLength(225)]
        public string? SoDienThoai { get; set; }
        public int LoaiDLID { get; set; }
        public virtual LoaiDaiLy? LoaiDaiLy { get; set; }
        public int? NhanVienID { get; set; }
        [ForeignKey("NhanVienID")]
        public virtual NhanVien? NhanVien { get; set; }

    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels
{
    public class ThongBaoCauHinh
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ThongBaoID { get; set; }
        [StringLength(50)]
        public required string MaThongBao { get; set; }
        public required string TenNhom { get; set; }
        public required string NoiDung { get; set; }
        public bool Active { get; set; }
    }
}

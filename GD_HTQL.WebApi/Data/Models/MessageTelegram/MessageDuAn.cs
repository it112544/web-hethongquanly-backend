﻿namespace GD_HTQL.WebApi.Data.Models.MessageTelegram
{
    public class MessageDuAn
    {
        public required string NguoiNhap { get; set; }
        public required string TenCoQuan { get; set; }
        public required string HanhDong { get; set; }
        public required string LoaiBaoCao { get; set; }
    }
}

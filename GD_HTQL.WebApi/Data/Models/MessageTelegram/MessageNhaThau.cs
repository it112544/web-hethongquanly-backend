﻿namespace GD_HTQL.WebApi.Data.Models.MessageTelegram
{
    public class MessageNhaThau
    {
        public required string TieuDe { get; set; }
        public required string TenNhaThau { get; set; }
        public required string TenNhanVien { get; set; }
        public required string TenDuToan { get; set; }
        public required string TenTinh { get; set; }
        public required string TenLoaiBaoGia { get; set; }
        public required string DoanhThu { get; set; }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NhaThauController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public NhaThauController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ??
                throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNhaThau")]
        public async Task<IActionResult> GetNhaThau()
        {
          
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupKhachHang(currentUser) == true)
            {
                var lstNhaThau = await _unitOfWork.NhaThauRepos.Find(x => x.Active == true);
                return Ok(lstNhaThau);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstNhaThau = await _unitOfWork.NhaThauRepos.GetbyNhanVien(_nhanVienID);
                    return Ok(lstNhaThau);
                }
            }         
        }

        [HttpGet]
        [Route("GetDanhSach")]
        public async Task<IActionResult> GetDanhSach()
        {
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupKhachHang(currentUser) == true)
            {
                var lstNhaThau = await _unitOfWork.NhaThauRepos.GetDanhSach(0);
                return Ok(lstNhaThau);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstNhaThau = await _unitOfWork.NhaThauRepos.GetDanhSach(_nhanVienID);
                    return Ok(lstNhaThau);
                }
            }
        }

        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetNhaThauByTuKhoa(string? Key, int tinhID)
        {
            return Ok(await _unitOfWork.NhaThauRepos.GetNhaThauByTuKhoa(Key, tinhID));
        }
        [HttpGet]
        [Route("GetNhaThauById/{Id}")]
        public IActionResult GetNhaThauById(int Id)
        {
            return Ok(_unitOfWork.NhaThauRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddNhaThau")]
        public async Task<IActionResult> AddNhaThau(NhaThauDto nhaThauDto)
        {
            if (!String.IsNullOrWhiteSpace(nhaThauDto.MaSoThue) && _unitOfWork.NhaThauRepos.CheckMaSoThue(nhaThauDto.MaSoThue, 0) == false)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã số thuế đã tồn tại");
            }
            else
            {
                var objNhaThau = _mapper.Map<NhaThau>(nhaThauDto);
                objNhaThau.Active = true;
                objNhaThau.CreateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                objNhaThau.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                objNhaThau.NhanVienID = _nhanVienID;
                var result = await _unitOfWork.NhaThauRepos.Add(objNhaThau);
                _unitOfWork.Save();
                if (result.NhaThauID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {                  
                    return Ok(result);

                }
              
            }          
        }
        [HttpPut]
        [Route("UpdateNhaThau")]
        public IActionResult UpdateNhaThau(NhaThauDto nhaThauDto)
        {

            if (!String.IsNullOrWhiteSpace(nhaThauDto.MaSoThue) && _unitOfWork.NhaThauRepos.CheckMaSoThue(nhaThauDto.MaSoThue, nhaThauDto.NhaThauID) == false)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã số thuế đã tồn tại đã tồn tại");
            }
            else
            {
                var objNhaThau = _mapper.Map<NhaThau>(nhaThauDto);
                var obj = _unitOfWork.NhaThauRepos.GetById(objNhaThau.NhaThauID);
                objNhaThau.Active = true;
                objNhaThau.CreateDate = obj.CreateDate;
                objNhaThau.CreateBy = obj.CreateBy;
                objNhaThau.UpdateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                objNhaThau.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                objNhaThau.NhanVienID = _nhanVienID;
                _unitOfWork.NhaThauRepos.Update(objNhaThau);
                _unitOfWork.Save();
                return Ok("Cập nhật thành công");
            }
           
        }
        [HttpDelete]
        [Route("DeleteNhaThau")]
        public async Task<JsonResult> DeleteNhaThau(int id)
        {
            var objNhaThau = _unitOfWork.NhaThauRepos.GetById(id);
            // Xóa giá trị cơ hội nhà thầu 
            var objGTCH = await _unitOfWork.NT_GiaTriCoHoiRepos.Find(x => x.NhaThauID == id);
            if (objGTCH.Count() >0)
            {
                _unitOfWork.NT_GiaTriCoHoiRepos.RemoveRange(objGTCH);
                _unitOfWork.Save();
            }
            _unitOfWork.NhaThauRepos.Remove(objNhaThau);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objNhaThau = _unitOfWork.NhaThauRepos.GetById(ID);
            if (objNhaThau.Active == true)
            {
                objNhaThau.Active = false;
            }
            else
            {
                objNhaThau.Active = true;
            }
            var currentUser = HttpContext.User;
            objNhaThau.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objNhaThau.ActiveDate = DateTime.Now;
            _unitOfWork.NhaThauRepos.Update(objNhaThau);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpGet]
        [Route("GetDsThe")]
        public async Task<IActionResult> GetDsThe()
        {
            return Ok(await _unitOfWork.NhaThauRepos.GetDsThe());
        }
        [HttpGet]
        [Route("GetDsNguonVon")]
        public async Task<IActionResult> GetDsNguonVon()
        {
            return Ok(await _unitOfWork.NhaThauRepos.GetDsNguonVon());
        }
        [HttpGet]
        [Route("GetDsDiaBan")]
        public async Task<IActionResult> GetDsDiaBanHoatDong()
        {
            return Ok(await _unitOfWork.NhaThauRepos.GetDsDiaBanHoatDong());
        }
        [HttpGet]
        [Route("GetDsLoaiHopTac")]
        public async Task<IActionResult> GetDsLoaiHopTac()
        {
            return Ok(await _unitOfWork.NhaThauRepos.GetDsLoaiHopTac());
        }

        [HttpGet]
        [Route("CheckMaSoThue")]
        public IActionResult CheckMaSoThue(string maSoThue, int nhaThauID)
        {
            if (string.IsNullOrWhiteSpace(maSoThue)) 
            {
                bool Check = _unitOfWork.NhaThauRepos.CheckMaSoThue(maSoThue, nhaThauID);
                if (Check)
                {
                    return Ok(new { message = true });
                }
                else
                {
                    return Ok(new { message = false });
                }
            }
            else
            {
                return Ok(new { message = true });
            }        
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThongBaoCauHinhController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ThongBaoCauHinhController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IEnumerable<ThongBaoCauHinh>> GetAll()
        {
            var result = await _unitOfWork.ThongBaoCauHinhRepos.GetAll();
            return result;
        }
       
        [HttpGet]
        [Route("GetById/{Id}")]
        public ThongBaoCauHinh GetById(int Id)
        {
            return _unitOfWork.ThongBaoCauHinhRepos.GetById(Id);
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> AddThongBaoCauHinh(ThongBaoCauHinh thongBao)
        {
           
            var result = await _unitOfWork.ThongBaoCauHinhRepos.Add(thongBao);
            _unitOfWork.Save();
            if (result.ThongBaoID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            return Ok(result);
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Update(ThongBaoCauHinh thongBao)
        {      
            _unitOfWork.ThongBaoCauHinhRepos.Update(thongBao);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var objDelete = _unitOfWork.ThongBaoCauHinhRepos.GetById(id);
            // Xóa thông báo nhân viên
            var objTBNV = await _unitOfWork.ThongBaoNhanVienRepos.Find(x => x.ThongBaoID == id);
            if (objTBNV != null)
            {
                _unitOfWork.ThongBaoNhanVienRepos.RemoveRange(objTBNV);
                _unitOfWork.Save();
            }
            _unitOfWork.ThongBaoCauHinhRepos.Remove(objDelete);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objThongBaoCauHinh = _unitOfWork.ThongBaoCauHinhRepos.GetById(ID);
            if (objThongBaoCauHinh.Active == true)
            {
                objThongBaoCauHinh.Active = false;
            }
            else
            {
                objThongBaoCauHinh.Active = true;
            }        
            _unitOfWork.ThongBaoCauHinhRepos.Update(objThongBaoCauHinh);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

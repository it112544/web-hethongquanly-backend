﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Data.Models.Dtos.DaiLy;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DaiLyController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public DaiLyController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetDaiLy()
        {           
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupKhachHang(currentUser) == true)
            {
                var lstDaiLy = await _unitOfWork.DaiLyRepos.Find(x => x.Active == true);
                return Ok(lstDaiLy);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstDaiLy = await _unitOfWork.DaiLyRepos.GetbyNhanVien(_nhanVienID);
                    return Ok(lstDaiLy);
                }
            }                     
        }
        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetDaiLyByTuKhoa(string? Key, int tinhID)
        {
            return Ok(await _unitOfWork.DaiLyRepos.GetDaiLyByTuKhoa(Key, tinhID));
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetDaiLyById(int Id)
        {
            return Ok(_unitOfWork.DaiLyRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> AddDaiLy(DaiLyDto daiLyDto)
        {
            if (!String.IsNullOrWhiteSpace(daiLyDto.MaSoThue) && _unitOfWork.NhaThauRepos.CheckMaSoThue(daiLyDto.MaSoThue, 0) == false)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã số thuế đã tồn tại đã tồn tại");
            }
            else
            {
                var objDaiLy = _mapper.Map<DaiLy>(daiLyDto);
                objDaiLy.Active = true;
                objDaiLy.CreateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                objDaiLy.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                objDaiLy.NhanVienID = _nhanVienID;
                var result = await _unitOfWork.DaiLyRepos.Add(objDaiLy);
                _unitOfWork.Save();
                if (result.DaiLyID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                return Ok(result);
            }         
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult UpdateDaiLy(DaiLyDto daiLyDto)
        {
            if (!String.IsNullOrWhiteSpace(daiLyDto.MaSoThue) && _unitOfWork.NhaThauRepos.CheckMaSoThue(daiLyDto.MaSoThue, daiLyDto.DaiLyID) == false)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã số thuế đã tồn tại đã tồn tại");
            }
            else
            {
                var objDaiLy = _mapper.Map<DaiLy>(daiLyDto);
                objDaiLy.Active = true;
                var obj = _unitOfWork.DaiLyRepos.GetById(objDaiLy.DaiLyID);

                objDaiLy.CreateDate = obj.CreateDate;
                objDaiLy.CreateBy = obj.CreateBy;
                objDaiLy.UpdateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                objDaiLy.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
                objDaiLy.NhanVienID = _nhanVienID;
                _unitOfWork.DaiLyRepos.Update(objDaiLy);
                _unitOfWork.Save();
                return Ok("Cập nhật thành công");
            }
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult DeleteDaiLy(int id)
        {
            var objDaiLy = _unitOfWork.DaiLyRepos.GetById(id);
            _unitOfWork.DaiLyRepos.Remove(objDaiLy);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objDaiLy = _unitOfWork.DaiLyRepos.GetById(ID);
            if (objDaiLy.Active == true)
            {
                objDaiLy.Active = false;
            }
            else
            {
                objDaiLy.Active = true;
            }
            var currentUser = HttpContext.User;
            objDaiLy.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objDaiLy.ActiveDate = DateTime.Now;
            _unitOfWork.DaiLyRepos.Update(objDaiLy);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpGet]
        [Route("CheckMaSoThue")]
        public IActionResult CheckMaSoThue(string maSoThue, int daiLyID)
        {
            if (string.IsNullOrWhiteSpace(maSoThue))
            {
                bool Check = _unitOfWork.DaiLyRepos.CheckMaSoThue(maSoThue, daiLyID);
                if (Check)
                {
                    return Ok(new { message = true });
                }
                else
                {
                    return Ok(new { message = false });
                }
            }
            else
            {
                return Ok(new { message = true });
            }
        }
    }
}

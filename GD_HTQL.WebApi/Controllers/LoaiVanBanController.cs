﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.VanBan;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LoaiVanBanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public LoaiVanBanController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetLoaiVanBan")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.LoaiVanBanRepos.GetAll());
        }
        [HttpGet]
        [Route("GetLoaiVanBanById/{Id}")]
        public IActionResult GetLoaiVanBanById(int Id)
        {
            return Ok( _unitOfWork.LoaiVanBanRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddLoaiVanBan")]
        public async Task<IActionResult> Post(LoaiVanBanDto loaiVanBanDto)
        {
            var objLoaiVanBan = _mapper.Map<LoaiVanBan>(loaiVanBanDto);
            var result = await _unitOfWork.LoaiVanBanRepos.Add(objLoaiVanBan);
            _unitOfWork.Save();
            if (result.LoaiVanBanID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("UpdateLoaiVanBan")]
        public IActionResult Put(LoaiVanBanDto loaiVanBanDto)
        {
            var objLoaiVanBan = _mapper.Map<LoaiVanBan>(loaiVanBanDto);
            _unitOfWork.LoaiVanBanRepos.Update(objLoaiVanBan);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]      
        [Route("DeleteLoaiVanBan")]
        public JsonResult Delete(int id)
        {
            var objLoaiVanBan = _unitOfWork.LoaiVanBanRepos.GetById(id);
            _unitOfWork.LoaiVanBanRepos.Remove(objLoaiVanBan);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

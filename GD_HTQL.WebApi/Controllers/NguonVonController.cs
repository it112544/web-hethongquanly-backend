﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NguonVonController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public NguonVonController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ??
                throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.NguonVonRepos.GetAll());
        }       

        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetById(int Id)
        {
            return Ok(_unitOfWork.NguonVonRepos.GetById(Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(NguonVonDto NguonVonDto)
        {
            var objNguonVon = _mapper.Map<NguonVon>(NguonVonDto);
            var result = await _unitOfWork.NguonVonRepos.Add(objNguonVon);
            _unitOfWork.Save();
            if (result.NguonVonID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok(result);
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(NguonVonDto NguonVonDto)
        {            
            var objNguonVon = _mapper.Map<NguonVon>(NguonVonDto);       
            _unitOfWork.NguonVonRepos.Update(objNguonVon);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objNguonVon = _unitOfWork.NguonVonRepos.GetById(id);
            _unitOfWork.NguonVonRepos.Remove(objNguonVon);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

﻿using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DbInitializerController : ControllerBase
    {
        private readonly IDbInitializer _DbInitializer;
        public DbInitializerController(IDbInitializer DbInitializer)
        {
            _DbInitializer = DbInitializer ??
                throw new ArgumentNullException(nameof(DbInitializer));
        }
        [HttpGet]
        [Route("GetDbInitializer")]
        public async Task<IActionResult> Get()
        {
             _DbInitializer.Initialize();
            return Ok();
        }
    }
}

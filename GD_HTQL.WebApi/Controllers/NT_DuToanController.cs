﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NT_DuToanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
        public NT_DuToanController(IUnitOfWork unitOfWork, IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;   
           _thongbao = thongbao;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.NT_DuToanRepos.GetDanhSach(0));
        }

        [HttpGet]
        [Route("GetbyNhaThauID")]
        public async Task<IActionResult> GetbyNhaThauID(int nhaThauID)
        {
            return Ok(await _unitOfWork.NT_DuToanRepos.GetDanhSach(nhaThauID));
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetbyId(int Id)
        {
            return Ok(_unitOfWork.NT_DuToanRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(NT_DuToanDto NT_DuToanDto)
        {
           
            var objNT_DuToan = _mapper.Map<NT_DuToan>(NT_DuToanDto);          
            objNT_DuToan.CreateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objNT_DuToan.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            objNT_DuToan.NhanVienID = ClassCommon.GetUserID(currentUser);

            if (!string.IsNullOrEmpty(NT_DuToanDto.FileCongTy) && !string.IsNullOrEmpty(NT_DuToanDto.TenFileCongTy))
            {
                objNT_DuToan.FileCongTy = await _unitOfWork.NT_DuToanRepos.SaveFile(NT_DuToanDto.FileCongTy, NT_DuToanDto.TenFileCongTy);
            }

            if (!string.IsNullOrEmpty(NT_DuToanDto.FileNhaThau) && !string.IsNullOrEmpty(NT_DuToanDto.TenFileNhaThau))
            {
                objNT_DuToan.FileNhaThau = await _unitOfWork.NT_DuToanRepos.SaveFile(NT_DuToanDto.FileNhaThau, NT_DuToanDto.TenFileNhaThau);
            }
            var result = await _unitOfWork.NT_DuToanRepos.Add(objNT_DuToan);

            _unitOfWork.Save();
            if (result.DuToanID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                try
                {
                    string tieude = objNT_DuToan.CreateBy.ToUpper() + "ĐÃ TẠO MỚI MỘT DỰ TOÁN";
                    await _thongbao.SendTeleNhaThau_DuToan(result.DuToanID, tieude);
                }
                catch
                {
                    return Ok(result);
                }
                return Ok(result);
            }
        }
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put(NT_DuToanDto NT_DuToanDto)
        {         
            var objNT_DuToan = _mapper.Map<NT_DuToan>(NT_DuToanDto);
            var obj = _unitOfWork.NT_DuToanRepos.GetById(objNT_DuToan.DuToanID);
            
            objNT_DuToan.CreateDate = obj.CreateDate;
            objNT_DuToan.CreateBy = obj.CreateBy;
            objNT_DuToan.NhanVienID = obj.NhanVienID;
            objNT_DuToan.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objNT_DuToan.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);

            // Kiểm tra xem file có giống file trước đó không 

            if(NT_DuToanDto.TenFileCongTy != obj.TenFileCongTy)
            {
                if (!string.IsNullOrEmpty(NT_DuToanDto.FileCongTy) && !string.IsNullOrEmpty(NT_DuToanDto.TenFileCongTy))
                {
                    objNT_DuToan.FileCongTy = await _unitOfWork.NT_DuToanRepos.SaveFile(NT_DuToanDto.FileCongTy, NT_DuToanDto.TenFileCongTy);
                    if (!string.IsNullOrEmpty(obj.FileCongTy))
                    {
                        _unitOfWork.NT_DuToanRepos.DeleteFile(obj.FileCongTy);
                    }                 
                }
                else
                {
                    objNT_DuToan.FileCongTy = "";
                    objNT_DuToan.TenFileCongTy = "";
                }
            }

            if (NT_DuToanDto.TenFileNhaThau != obj.TenFileNhaThau)
            {
                if (!string.IsNullOrEmpty(NT_DuToanDto.FileNhaThau) && !string.IsNullOrEmpty(NT_DuToanDto.TenFileNhaThau))
                {
                    objNT_DuToan.FileNhaThau = await _unitOfWork.NT_DuToanRepos.SaveFile(NT_DuToanDto.FileNhaThau, NT_DuToanDto.TenFileNhaThau);
                    if (!string.IsNullOrEmpty(obj.FileNhaThau))
                    {
                        _unitOfWork.NT_DuToanRepos.DeleteFile(obj.FileNhaThau);
                    }                   
                }
                else
                {
                    objNT_DuToan.FileNhaThau = "";
                    objNT_DuToan.TenFileNhaThau = "";
                }
            }             
            _unitOfWork.NT_DuToanRepos.Update(objNT_DuToan);
            _unitOfWork.Save();
            try
            {
                string tieude = objNT_DuToan.UpdateBy.ToUpper() + "ĐÃ CẬP NHẬT MỘT DỰ TOÁN";
                await _thongbao.SendTeleNhaThau_DuToan(objNT_DuToan.DuToanID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }      
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var currentUser = HttpContext.User;
            var objNT_DuToan = _unitOfWork.NT_DuToanRepos.GetById(id);
            // Xóa file
            if (!string.IsNullOrEmpty(objNT_DuToan.FileNhaThau))
            {
                _unitOfWork.NT_DuToanRepos.DeleteFile(objNT_DuToan.FileNhaThau);
            }
            if (!string.IsNullOrEmpty(objNT_DuToan.FileNhaThau))
            {
                _unitOfWork.NT_DuToanRepos.DeleteFile(objNT_DuToan.FileNhaThau);
            }
            try
            {
                string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + "ĐÃ XÓA MỘT DỰ TOÁN";
                await _thongbao.SendTeleNhaThau_DuToan(id, tieude);
            }
            catch
            {
                _unitOfWork.NT_DuToanRepos.Remove(objNT_DuToan);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }         
            _unitOfWork.NT_DuToanRepos.Remove(objNT_DuToan);
            _unitOfWork.Save();         
            return new JsonResult("Xóa thành công");
        }
        [HttpGet]
        [Route("GetTrangThai")]
        public async Task<IActionResult> GetTrangThai()
        {
            return Ok(await _unitOfWork.NT_DuToanRepos.GetTrangThai());
        }
    }
}

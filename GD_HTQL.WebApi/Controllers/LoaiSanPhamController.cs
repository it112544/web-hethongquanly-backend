﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LoaiSanPhamController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public LoaiSanPhamController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.LoaiSanPhamRepos.GetAll());
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public  IActionResult GetLoaiSanPhamById(int Id)
        {
            return Ok( _unitOfWork.LoaiSanPhamRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(LoaiSanPhamDto loaiSanPhamDto)
        {
            var objLoaiSanPham = _mapper.Map<LoaiSanPham>(loaiSanPhamDto);
            var result = await _unitOfWork.LoaiSanPhamRepos.Add(objLoaiSanPham);
            _unitOfWork.Save();
            if (result.LoaiSanPhamID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(LoaiSanPhamDto loaiSanPhamDto)
        {
            var objLoaiSanPham = _mapper.Map<LoaiSanPham>(loaiSanPhamDto);
            _unitOfWork.LoaiSanPhamRepos.Update(objLoaiSanPham);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objLoaiSanPham = _unitOfWork.LoaiSanPhamRepos.GetById(id);
            _unitOfWork.LoaiSanPhamRepos.Remove(objLoaiSanPham);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

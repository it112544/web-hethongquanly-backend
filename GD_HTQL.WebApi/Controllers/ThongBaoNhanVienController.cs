﻿using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ThongBaoNhanVienController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public ThongBaoNhanVienController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IEnumerable<ThongBaoNhanVien>> GetALl()
        {
            var result = await _unitOfWork.ThongBaoNhanVienRepos.GetAll();
            return result;
        }

        [HttpGet]
        [Route("GetNhanVien")]
        public async Task<IActionResult> GetNhanVien(int ThongBaoID)
        {
            var result = await _unitOfWork.ThongBaoNhanVienRepos.Find(x => x.ThongBaoID == ThongBaoID);
            if (result.Count() > 0)
            {
                var lstNhanVienID = result.Select(x => x.NhanVienID).ToList();
                var qLNhanVienViewModels = await _unitOfWork.ThongBaoNhanVienRepos.GetNhanVien(lstNhanVienID);
                return Ok(qLNhanVienViewModels);
            }
            else
            {
                return Ok("Bạn chưa thêm nhân viên để nhận thông báo");
            }
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public ThongBaoNhanVien GetById(int Id)
        {
            return _unitOfWork.ThongBaoNhanVienRepos.GetById(Id);
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add(ThongBaoNhanVien objThongBaoNhanVien)
        {
            var obj = await _unitOfWork.ThongBaoNhanVienRepos.Find(x => x.ThongBaoID == objThongBaoNhanVien.ThongBaoID && x.NhanVienID == objThongBaoNhanVien.NhanVienID);
            if (obj.Count() == 0)
            {
                var result = await _unitOfWork.ThongBaoNhanVienRepos.Add(objThongBaoNhanVien);
                _unitOfWork.Save();
                if (result.Id == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                return Ok(result);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Quản lý nhân viên đã tồn tại");
            }
        }

        [HttpPost]
        [Route("AddMulti")]
        public IActionResult AddMulti(ThongBaoNhanVienCustom objThongBaoNhanVien)
        {
            try
            {
                _unitOfWork.ThongBaoNhanVienRepos.AddMulti(objThongBaoNhanVien);
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }

        }

        [HttpPut]
        [Route("Update")]
        public IActionResult Update(ThongBaoNhanVien objThongBaoNhanVien)
        {
            _unitOfWork.ThongBaoNhanVienRepos.Update(objThongBaoNhanVien);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objDelete = _unitOfWork.ThongBaoNhanVienRepos.GetById(id);
            _unitOfWork.ThongBaoNhanVienRepos.Remove(objDelete);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
        [HttpDelete]
        [Route("Delete/{ThongBaoID}/{nhanVienID}")]
        public async Task<JsonResult> Delete(int ThongBaoID, int nhanVienID)
        {
            var objDelete = await _unitOfWork.ThongBaoNhanVienRepos.Find(x => x.ThongBaoID == ThongBaoID && x.NhanVienID == nhanVienID);
            if (objDelete == null)
            {
                return new JsonResult("Không tìm thấy dữ liệu cần xóa");
            }
            else
            {
                _unitOfWork.ThongBaoNhanVienRepos.RemoveRange(objDelete);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
        }
    }
}

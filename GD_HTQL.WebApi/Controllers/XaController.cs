﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class XaController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public XaController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetXaByHuyenID/{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            var result = await _unitOfWork.DMXaRepos.Find(x => x.HuyenID == Id);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetXaById/{Id}")]
        public IActionResult GetXaById(int Id)
        {
            return Ok(_unitOfWork.DMXaRepos.GetById(Id));
        }
      
        [HttpPost]
        [Route("AddXa")]
        public async Task<IActionResult> Post(DMXaDto xaDto)
        {
            var objXa = _mapper.Map<DMXa>(xaDto);
            var result = await _unitOfWork.DMXaRepos.Add(objXa);
            _unitOfWork.Save();
            if (result.XaID== 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("UpdateXa")]
        public IActionResult Put(DMXaDto xaDto)
        {
            var objXa = _mapper.Map<DMXa>(xaDto);
            _unitOfWork.DMXaRepos.Update(objXa);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("DeleteXa")]
        public JsonResult Delete(int id)
        {
            var objXa = _unitOfWork.DMXaRepos.GetById(id);
            _unitOfWork.DMXaRepos.Remove(objXa);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

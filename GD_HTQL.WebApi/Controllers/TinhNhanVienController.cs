﻿using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TinhNhanVienController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public TinhNhanVienController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IEnumerable<TinhNhanVien>> GetALl()
        {
            var result = await _unitOfWork.TinhNhanVienRepos.GetAll();
            return result;
        }

        [HttpGet]
        [Route("GetByNhanVien")]
        public async Task<IActionResult> GetByNhanVien(int NhanVienID)
        {
            var result = await _unitOfWork.TinhNhanVienRepos.GetByNhanVien(NhanVienID);
            return Ok(result);           
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public TinhNhanVien GetTinhNhanVienById(int Id)
        {
            return _unitOfWork.TinhNhanVienRepos.GetById(Id);
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add(TinhNhanVien objTinhNhanVien)
        {
            // Kiểm tra trùng 
            var obj = await _unitOfWork.TinhNhanVienRepos.Find(x => x.TinhID == objTinhNhanVien.TinhID && x.NhanVienID == objTinhNhanVien.NhanVienID);
            if (obj.Count() == 0)
            {
                var result = await _unitOfWork.TinhNhanVienRepos.Add(objTinhNhanVien);

                _unitOfWork.Save();
                if (result.ID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                return Ok(result);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Tỉnh nhân viên đã tồn tại");
            }          
        }

        [HttpPost]
        [Route("AddMulti")]
        public IActionResult AddMulti(List<TinhNhanVien> objTinhNhanVien)
        {
            _unitOfWork.TinhNhanVienRepos.AddRange(objTinhNhanVien);
            _unitOfWork.Save();        
            return Ok();
        }

        [HttpPut]
        [Route("Update")]
        public IActionResult Update(TinhNhanVien objTinhNhanVien)
        {
            _unitOfWork.TinhNhanVienRepos.Update(objTinhNhanVien);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objDelete = _unitOfWork.TinhNhanVienRepos.GetById(id);
            _unitOfWork.TinhNhanVienRepos.Remove(objDelete);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }

        [HttpDelete]
        [Route("Delete/{tinhID}/{nhanVienID}")]
        public async Task<JsonResult> Delete(int tinhID, int nhanVienID)
        {
            var objDelete = await _unitOfWork.TinhNhanVienRepos.Find(x=>x.TinhID ==tinhID && x.NhanVienID == nhanVienID);
            if (objDelete == null) 
            {
                return new JsonResult("Không tìm thấy dữ liệu cần xóa");
            }
            else
            {
                _unitOfWork.TinhNhanVienRepos.RemoveRange(objDelete);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
        }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TrangThaiCongViecController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public TrangThaiCongViecController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.TrangThaiCongViecRepos.GetAll());
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetById(int Id)
        {
            return Ok(_unitOfWork.TrangThaiCongViecRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddLoaiSanPham")]
        public async Task<IActionResult> Post(TrangThaiCongViec objTrangThai)
        {           
            var result = await _unitOfWork.TrangThaiCongViecRepos.Add(objTrangThai);
            _unitOfWork.Save();
            if (result.TrangThaiID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("UpdateLoaiSanPham")]
        public IActionResult Put(TrangThaiCongViec objTrangThai)
        {           
            _unitOfWork.TrangThaiCongViecRepos.Update(objTrangThai);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("DeleteLoaiSanPham")]
        public JsonResult Delete(int id)
        {
            var objTrangThai = _unitOfWork.TrangThaiCongViecRepos.GetById(id);
            _unitOfWork.TrangThaiCongViecRepos.Remove(objTrangThai);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

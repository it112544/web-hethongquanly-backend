﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.NguoiDungModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public RolesController(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration,
            IUnitOfWork unitOfWork,
            IMapper mapper)
           {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
           }
        [HttpGet]
        [Route("GetRole")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _roleManager.Roles.ToListAsync());

        }
        [HttpGet]
        [Route("GetRoleByID")]
        public async Task<IActionResult> GetRoleByID(string ID)
        {
            return Ok(await _roleManager.FindByIdAsync(ID));
        }
        [HttpGet]
        [Route("GetRoleByName")]
        public async Task<IActionResult> GetRoleByName(string RoleName)
        {
            return Ok(await _roleManager.FindByNameAsync(RoleName));
        }
        [HttpPost]
        [Route("AddRole")]
        public IActionResult Post(string RoleName)
        {
            if (!_roleManager.RoleExistsAsync(RoleName).GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole(RoleName)).GetAwaiter().GetResult();
                return Ok("Thêm mới thành công");
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Nhóm quyền đã tồn tại trong hệ thống");
            }            
        }

        [HttpPost]
        [Route("UpdateRole")]
        public async Task<IActionResult> UpdataRole(string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);
            if (role !=null)
            {
                var result = await _roleManager.UpdateAsync(role);
                if (!result.Succeeded)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    return Ok("Cập nhật thành công");
                }
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Không tìm thấy " + roleName + " trong hệ thống");
            }          
        }

        [HttpDelete]    
        [Route("DeleteRole")]
        public async Task<IActionResult> Delete(string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);
            if (role != null)
            {
                var result = await _roleManager.DeleteAsync(role);
                if (!result.Succeeded)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    return Ok("Xóa thành công");
                }
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Không tìm thấy " +roleName + " trong hệ thống" );
            }           
        }
        [HttpPost]
        [Route("AddRoleClaims/{roleName}")]
        public async Task<IActionResult> AddRoleClaims(string roleName, ClaimModel claimModel)
        {
            var roleResult = await _roleManager.FindByNameAsync(roleName);          
            if (roleResult == null)
            {
                roleResult = new IdentityRole(roleName);
                await _roleManager.CreateAsync(roleResult);
            }
            var roleClaimList = (await _roleManager.GetClaimsAsync(roleResult)).Select(p => p.Type);
            if (!roleClaimList.Contains(claimModel.ClaimType))
            {
              var obj =  await _roleManager.AddClaimAsync(roleResult, new Claim(claimModel.ClaimType, claimModel.ClaimValue));
            }   
            return Ok(roleClaimList);
        }

        [HttpPost]
        [Route("RemoveRoleClaims/{roleName}")]
        public async Task<IActionResult> RemoveRoleClaims(string roleName, ClaimModel claimModel)
        {
            var roleResult = await _roleManager.FindByNameAsync(roleName);
            if (roleResult == null)
            {
                var roleClaimList = (await _roleManager.GetClaimsAsync(roleResult)).Select(p => p.Type);
                if (!roleClaimList.Contains(claimModel.ClaimType))
                {
                    var obj = await _roleManager.RemoveClaimAsync(roleResult, new Claim(claimModel.ClaimType, claimModel.ClaimValue));
                }
                return Ok();
            }
            else
            {
                return Ok("Nhóm quyền không tồn tại trong hệ thống");
            }          
        }

        [HttpPost]
        [Route("AddUserClaims/{userName}")]
        public async Task<IActionResult> AddUserClaims(string userName, ClaimModel claimModel)
        {
            var userResult = await _userManager.FindByNameAsync(userName);
            if (userResult != null)
            {
                var userClaimList = (await _userManager.GetClaimsAsync(userResult)).Select(p => p.Type);
                if (!userClaimList.Contains(claimModel.ClaimType))
                {
                    var obj = await _userManager.AddClaimAsync(userResult, new Claim(claimModel.ClaimType, claimModel.ClaimValue));
                }
            }          
            return Ok();
        }

        [HttpPost]
        [Route("RemoveUserClaims/{userName}")]
        public async Task<IActionResult> RemoveUserClaims(string userName, ClaimModel claimModel)
        {
            var userResult = await _userManager.FindByNameAsync(userName);
            if (userResult != null)
            {
                var userClaimList = (await _userManager.GetClaimsAsync(userResult)).Select(p => p.Type);
                if (!userClaimList.Contains(claimModel.ClaimType))
                {
                    await _userManager.RemoveClaimAsync(userResult, new Claim(claimModel.ClaimType, claimModel.ClaimValue));
                   
                }
            }
            return Ok();
        }

        [HttpPost]
        [Route("AddUserToRole")]
        public async Task<IActionResult> AddUserToRole(int nhanvienID, string roleName)
        {
            string _userID = _unitOfWork.NhanVienRepos.GetById(nhanvienID).UserID;
            var userResult = await _userManager.FindByIdAsync(_userID);
            var roleResult = await _roleManager.FindByNameAsync(roleName);
           
            if (userResult != null)
            {
                if (roleResult != null)
                {
                    if (!await _userManager.IsInRoleAsync(userResult, roleName))
                    {
                        _userManager.AddToRoleAsync(userResult, roleName).GetAwaiter().GetResult();
                        return Ok("Thêm người dùng vào nhóm quyền thành công");
                    }
                    else
                    {
                        return Ok("Người dùng đã thuộc nhóm quyền này");
                    }                     
                }
                else
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Nhóm quyền không tồn tại");
                }
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            }                                      
        }
        [HttpPost]
        [Route("RemoveUserToRole")]
        public async Task<IActionResult> RemoveUserToRole(int nhanvienID, string roleName)
        {
            string _userID = _unitOfWork.NhanVienRepos.GetById(nhanvienID).UserID;
            var userResult = await _userManager.FindByIdAsync(_userID);
            var roleResult = await _roleManager.FindByNameAsync(roleName);
            if (userResult != null)
            {
                if (roleResult != null)
                {
                    _userManager.RemoveFromRoleAsync(userResult, roleName).GetAwaiter().GetResult();
                    return Ok("Xóa người dùng khỏi nhóm quyền thành công");
                }
                else
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Nhóm quyền không tồn tại");
                }
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            }
        }
        [HttpGet]
        [Route("GetAllRoleOfUser")]
        public async Task<IActionResult> GetAllRoleOfUser(int nhanVienID)
        {
            string _userID = _unitOfWork.NhanVienRepos.GetById(nhanVienID).UserID;
            var userResult = await _userManager.FindByIdAsync(_userID);   
          
            if (userResult != null)
            {
                var rolesList = _userManager.GetRolesAsync(userResult);              
                return Ok(rolesList.Result);             
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Người dùng không tồn tại");
            }
        }

        [HttpGet]
        [Route("GetAllUserOfRole")]
        public async Task<IActionResult> GetAllUserOfRole(string roleName)
        {
            //string _userID = _unitOfWork.NhanVienRepos.GetById(nhanVienID).UserID;
            var roleList = await _roleManager.FindByNameAsync(roleName);
            if (roleList != null)
            {
                var obj = await _userManager.GetUsersInRoleAsync(roleName);
                if(obj != null)
                {
                    var objUserID = obj.Select(x => x.Id).ToList();
                    var objNhanVien = await _unitOfWork.NhanVienRepos.Find(x => objUserID.Contains(x.UserID) && x.Active == true);
                    var objNhanVienShort = _mapper.Map<List<NhanVienShortView>>(objNhanVien);
                    return Ok(objNhanVienShort);
                }
                else
                {
                    return Ok("Không có nhân viên thuộc nhóm quyền này");
                }
                                           
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Nhóm quyền không tồn tại");
            }           
        }
    }
}

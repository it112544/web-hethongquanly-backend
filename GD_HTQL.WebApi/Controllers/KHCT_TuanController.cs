﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.Dtos.CongTac;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KHCT_TuanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
        public KHCT_TuanController(IUnitOfWork unitOfWork, IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _thongbao = thongbao;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IEnumerable<KHCT_Tuan>> GetAll()
        {
            var result = await _unitOfWork.KHCT_TuanRepos.Find(x => x.Active == true);
            return result;
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public KHCT_Tuan GetById(int Id)
        {
            return _unitOfWork.KHCT_TuanRepos.GetById(Id);
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add(KHCT_TuanDto KHCT_TuanDto)
        {
            var objDate = _unitOfWork.KHCT_TuanRepos.GetDateOfWeekModel(KHCT_TuanDto.ThangID, KHCT_TuanDto.Tuan_Thang);
            var objKHCT_Tuan = _mapper.Map<KHCT_Tuan>(KHCT_TuanDto);
            objKHCT_Tuan.Active = true;
            objKHCT_Tuan.CreateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objKHCT_Tuan.IsApprove = null;
            objKHCT_Tuan.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            objKHCT_Tuan.NguoiTaoID = ClassCommon.GetUserID(currentUser);
            if (objDate !=null)
            {
                objKHCT_Tuan.Tuan_Nam = objDate.Tuan;
                //objKHCT_Tuan.TuNgay = objDate.TuNgay;
                //objKHCT_Tuan.DenNgay = objDate.DenNgay;
            }                    
            var result = await _unitOfWork.KHCT_TuanRepos.Add(objKHCT_Tuan);
            _unitOfWork.Save();
            if (result.TuanID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                try
                {
                    var objThang = _unitOfWork.KHCT_ThangRepos.GetById(result.ThangID);
                    string tieude = objKHCT_Tuan.CreateBy.ToUpper() + " VỪA TẠO MỚI KẾ HOẠCH CÔNG TÁC TUẦN " + result.Tuan_Thang + " THÁNG " + objThang.Thang;
                    await _thongbao.SendTeleKHCT_Tuan(result.TuanID, tieude);
                }
                catch
                {
                    return Ok(result);
                }
                return Ok(result);
            }         
        }
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(KHCT_TuanDto KHCT_TuanDto)
        {
            var objKHCT_Tuan = _mapper.Map<KHCT_Tuan>(KHCT_TuanDto);
            var obj = _unitOfWork.KHCT_TuanRepos.GetById(objKHCT_Tuan.TuanID);
            objKHCT_Tuan.Active = true;
            objKHCT_Tuan.CreateDate = obj.CreateDate;
            objKHCT_Tuan.CreateBy = obj.CreateBy;
            objKHCT_Tuan.NguoiTaoID = obj.NguoiTaoID;
            objKHCT_Tuan.UpdateDate = DateTime.Now;
            objKHCT_Tuan.Tuan_Nam = obj.Tuan_Nam;
            var currentUser = HttpContext.User;
            objKHCT_Tuan.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            //objKHCT_Tuan.NhanVienID = ClassCommon.GetUserID(currentUser);
            _unitOfWork.KHCT_TuanRepos.Update(objKHCT_Tuan);
            _unitOfWork.Save();
            try
            {
                var objThang = _unitOfWork.KHCT_ThangRepos.GetById(objKHCT_Tuan.ThangID);
                string tieude = objKHCT_Tuan.UpdateBy.ToUpper() + " VỪA CẬP NHẬT KẾ HOẠCH CÔNG TÁC TUẦN " + objKHCT_Tuan.Tuan_Thang + " THÁNG " + objThang.Thang;
                await _thongbao.SendTeleKHCT_Tuan(objKHCT_Tuan.TuanID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }
            return Ok("Cập nhật thành công");
        }
        [HttpPost]
        [Route("Duyet/{ID}")]
        public async Task<IActionResult> Duyet(int ID, bool trangThai)
        {
            var objKHCT_Tuan = _unitOfWork.KHCT_TuanRepos.GetById(ID);
            objKHCT_Tuan.IsApprove = trangThai;         
            var currentUser = HttpContext.User;
            objKHCT_Tuan.NguoiDuyet = ClassCommon.GetTenNhanVien(currentUser);
            objKHCT_Tuan.NguoiDuyetID = ClassCommon.GetUserID(currentUser);
            _unitOfWork.KHCT_TuanRepos.Update(objKHCT_Tuan);
            _unitOfWork.Save();
            try
            {
                string _trangthai = "";
                if (trangThai == true)
                {
                    _trangthai = "DUYỆT";
                }
                else
                {
                    _trangthai = "TỪ CHỐI";
                }
                string tieude = objKHCT_Tuan.NguoiDuyet.ToUpper() + " VỪA " + _trangthai + " KẾ HOẠCH CÔNG TÁC TUẦN " + objKHCT_Tuan.Tuan_Thang + " CỦA " + objKHCT_Tuan.CreateBy.ToUpper();
                await _thongbao.SendTeleKHCT_Tuan(objKHCT_Tuan.TuanID, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }

            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var currentUser = HttpContext.User;           
            var objDelete = _unitOfWork.KHCT_TuanRepos.GetById(id);
            var objKHCT_Ngay =await _unitOfWork.KHCT_NgayRepos.Find(x => x.TuanID == id);
            if (objKHCT_Ngay.Count() == 0)
            {
                try
                {
                    string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " VỪA XÓA KẾ HOẠCH CÔNG TÁC TUẦN " + objDelete.Tuan_Nam;
                    await _thongbao.SendTeleKHCT_Thang(objDelete.ThangID, tieude);
                }
                catch
                {
                    _unitOfWork.KHCT_TuanRepos.Remove(objDelete);
                    _unitOfWork.Save();
                }
                _unitOfWork.KHCT_TuanRepos.Remove(objDelete);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
            else
            {
                return new JsonResult("Đã có kế hoạch ngày cho tuần này. Vui lòng xóa kế hoạch ngày trước");
            }        
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objKHCT_Tuan = _unitOfWork.KHCT_TuanRepos.GetById(ID);
            if (objKHCT_Tuan.Active == true)
            {
                objKHCT_Tuan.Active = false;
            }
            else
            {
                objKHCT_Tuan.Active = true;
            }
            var currentUser = HttpContext.User;
            objKHCT_Tuan.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objKHCT_Tuan.ActiveDate = DateTime.Now;
            _unitOfWork.KHCT_TuanRepos.Update(objKHCT_Tuan);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Data.Models.Dtos.DaiLy;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoaiDaiLyController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public LoaiDaiLyController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.LoaiDaiLyRepos.GetAll());
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetLoaiDaiLyById(int Id)
        {
            return Ok(_unitOfWork.LoaiDaiLyRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(LoaiDaiLyDto LoaiDaiLyDto)
        {
            var objLoaiDaiLy = _mapper.Map<LoaiDaiLy>(LoaiDaiLyDto);
            var result = await _unitOfWork.LoaiDaiLyRepos.Add(objLoaiDaiLy);
            _unitOfWork.Save();
            if (result.LoaiDLID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(LoaiDaiLyDto LoaiDaiLyDto)
        {
            var objLoaiDaiLy = _mapper.Map<LoaiDaiLy>(LoaiDaiLyDto);
            _unitOfWork.LoaiDaiLyRepos.Update(objLoaiDaiLy);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objLoaiDaiLy = _unitOfWork.LoaiDaiLyRepos.GetById(id);
            _unitOfWork.LoaiDaiLyRepos.Remove(objLoaiDaiLy);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

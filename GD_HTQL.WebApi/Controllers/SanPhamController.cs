﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SanPhamController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public SanPhamController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetSanPham()
        {
            var objSanPham = _unitOfWork.SanPhamRepos.GetInclude();
            return Ok(objSanPham);
        }

        [HttpGet]
        [Route("GetByThongTu/{thongTuID}")]
        public IActionResult GetByThongTu(int thongTuID)
        {
            var objSanPham = _unitOfWork.SanPhamRepos.GetByThongTu(thongTuID);
            return Ok(objSanPham);
        }
        [HttpGet]
        [Route("GetDanhSach")]
        public async Task<IActionResult> GetDanhSach(string? tukhoa, int loaiID, int monhocID, int khoilopID, int pageCurrent, int pageSize)
        {
            return Ok(await _unitOfWork.SanPhamRepos.GetDanhSach( tukhoa,loaiID,monhocID,khoilopID,pageCurrent,pageSize));
        }

        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetByTuKhoa(string? Key)
        {
            return Ok(await _unitOfWork.SanPhamRepos.GetByTuKhoa(Key));
        }

        [HttpGet]
        [Route("GetLike")]
        public  IActionResult GetLike(string tenSanPham)
        {
            return Ok( _unitOfWork.SanPhamRepos.GetLikeTenSanPham(tenSanPham));
        }


        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetSanPhamById(int Id)
        {
            return Ok(_unitOfWork.SanPhamRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> AddSanPham(SanPhamDto sanphamDto)
        {
            if ( _unitOfWork.SanPhamRepos.CheckMaSanPham(sanphamDto.MaSanPham,0) == true)
            {
                var objSanPham = _mapper.Map<SanPham>(sanphamDto);
                objSanPham.Active = true;
                objSanPham.KhoiLop = null;
                objSanPham.MonHoc = null;
                objSanPham.ThongTu = null;
                  
                objSanPham.CreateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                objSanPham.CreateBy = ClassCommon.GetTenNhanVien(currentUser);              
                var result = await _unitOfWork.SanPhamRepos.Add(objSanPham);
                _unitOfWork.Save();
                if (result.SanPhamID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    // Lưu môn học
                    if(sanphamDto.MonHoc != null && sanphamDto.MonHoc.Count() > 0)
                    {
                        var obj = sanphamDto.MonHoc.Select(x => x.MonHocID).ToList();
                        await _unitOfWork.SanPhamRepos.ThemMonHoc(obj, result.SanPhamID,"add");
                    }
                    // Lưu Khoi lop
                    if (sanphamDto.KhoiLop != null && sanphamDto.KhoiLop.Count() > 0)
                    {
                        var obj = sanphamDto.KhoiLop.Select(x => x.KhoiLopID).ToList();
                        await _unitOfWork.SanPhamRepos.ThemKhoiLop(obj, result.SanPhamID,"add");
                    }
                    // Lưu Thong Tu
                    if (sanphamDto.ThongTu != null && sanphamDto.ThongTu.Count() > 0)
                    {
                        var obj = sanphamDto.ThongTu.Select(x => x.ThongTuID).ToList();
                        await _unitOfWork.SanPhamRepos.ThemThongTu(obj, result.SanPhamID,"add");
                    }
                    return Ok(result);
                }
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã sản phẩm đã tồn tại");
            }
            
        }
        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> UpdateSanPham( SanPhamDto sanphamDto)
        {
            if (_unitOfWork.SanPhamRepos.CheckMaSanPham(sanphamDto.MaSanPham, sanphamDto.SanPhamID) == true)
            {
                var objSanPham = _mapper.Map<SanPham>(sanphamDto);
                objSanPham.Active = true;
                objSanPham.KhoiLop = null;
                objSanPham.MonHoc = null;
                objSanPham.ThongTu = null;

                var objSP = _unitOfWork.SanPhamRepos.GetById(objSanPham.SanPhamID);
                var currentUser = HttpContext.User;
                objSanPham.CreateDate = objSP.CreateDate;
                objSanPham.CreateBy = objSP.CreateBy;
                objSanPham.UpdateDate = DateTime.Now;
                objSanPham.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
                _unitOfWork.SanPhamRepos.Update(objSanPham);
                _unitOfWork.Save();
                // Cập nhật Thông Tư, Môn học, Khối lớp
                // Lưu môn học
                if (sanphamDto.MonHoc != null && sanphamDto.MonHoc.Count() > 0)
                {
                    var obj = sanphamDto.MonHoc.Select(x => x.MonHocID).ToList();
                    await _unitOfWork.SanPhamRepos.ThemMonHoc(obj, sanphamDto.SanPhamID, "update");
                }
                else
                {
                    _unitOfWork.SanPhamRepos.XoaSanPhamMonHoc(objSanPham.SanPhamID);

                }
                // Lưu Khoi lop
                if (sanphamDto.KhoiLop != null && sanphamDto.KhoiLop.Count() > 0)
                {
                    var obj = sanphamDto.KhoiLop.Select(x => x.KhoiLopID).ToList();
                    await _unitOfWork.SanPhamRepos.ThemKhoiLop(obj, sanphamDto.SanPhamID, "update");
                }
                else
                {
                    _unitOfWork.SanPhamRepos.XoaSanPhamKhoiLop(objSanPham.SanPhamID);
                }

                // Lưu Thong Tu
                if (sanphamDto.ThongTu != null && sanphamDto.ThongTu.Count() > 0)
                {
                    var obj = sanphamDto.ThongTu.Select(x => x.ThongTuID).ToList();
                    await _unitOfWork.SanPhamRepos.ThemThongTu(obj, sanphamDto.SanPhamID, "update");
                }
                else
                {
                    _unitOfWork.SanPhamRepos.XoaSanPhamThongTu(objSanPham.SanPhamID);
                }
                return Ok("Cập nhật thành công");
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã sản phẩm đã tồn tại");
            }          
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult DeleteSanPham(int id)
        {
            var objSanPham = _unitOfWork.SanPhamRepos.GetById(id);

            _unitOfWork.SanPhamRepos.XoaSanPhamMonHoc(id);
            _unitOfWork.SanPhamRepos.XoaSanPhamThongTu(id);
            _unitOfWork.SanPhamRepos.XoaSanPhamKhoiLop(id);
            _unitOfWork.SanPhamRepos.Remove(objSanPham);
            _unitOfWork.Save();

            return new JsonResult("Xóa thành công");
        }

        [HttpGet]
        [Route("CheckMaSanPham")]
        public  IActionResult CheckMaSanPham(string MaSanPham,int sanphamID)
        {

            bool Check =  _unitOfWork.SanPhamRepos.CheckMaSanPham(MaSanPham, sanphamID);
            if (Check)
            {
                return Ok(new { message = true });
            }
            else
            {
                return Ok(new { message = false });
            }
        }
        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objSanPham = _unitOfWork.SanPhamRepos.GetById(ID);
            if (objSanPham.Active == true)
            {
                objSanPham.Active = false;
            }
            else
            {
                objSanPham.Active = true;
            }
            objSanPham.ActiveDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objSanPham.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            _unitOfWork.SanPhamRepos.Update(objSanPham);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

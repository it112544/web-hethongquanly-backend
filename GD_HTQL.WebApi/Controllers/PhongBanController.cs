﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PhongBanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public PhongBanController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.PhongBanRepos.GetAll());
        }
        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetPhongBanByTuKhoa(string Key, int CongTyID)
        {
            return Ok(await _unitOfWork.PhongBanRepos.GetPhongBanByTuKhoa(Key, CongTyID));
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetPhongBanById(int Id)
        {
            return Ok(_unitOfWork.PhongBanRepos.GetById(Id));
        }
        [HttpGet]
        [Route("GetByCty/{Id}")]
        public async Task<IActionResult> GetByCty(int Id)
        {
            return Ok(await _unitOfWork.PhongBanRepos.Find(x=>x.CongTyID ==Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(PhongBanDto phongBanDto)
        {
            var objPhongBan = _mapper.Map<PhongBan>(phongBanDto);
            objPhongBan.Active = true;
            objPhongBan.CreateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objPhongBan.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            var result = await _unitOfWork.PhongBanRepos.Add(objPhongBan);
            _unitOfWork.Save();
            if (result.PhongBanID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok(result);
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(PhongBanDto phongBanDto)
        {
            var objPhongBan = _mapper.Map<PhongBan>(phongBanDto);
            var obj = _unitOfWork.PhongBanRepos.GetById(objPhongBan.PhongBanID);
            objPhongBan.Active = true;
            objPhongBan.CreateDate = obj.CreateDate;
            objPhongBan.CreateBy = obj.CreateBy;
            objPhongBan.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objPhongBan.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            _unitOfWork.PhongBanRepos.Update(objPhongBan);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var objPhongBan = _unitOfWork.PhongBanRepos.GetById(id);

            var objChuVu = await _unitOfWork.ChucVuRepos.Find(x => x.PhongBanID == id);
            if (objChuVu.Count() == 0)
            {
                _unitOfWork.PhongBanRepos.Remove(objPhongBan);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
            else
            {
                return new JsonResult("Phòng ban đã phát sinh dữ liệu vui lòng xóa các dữ liệu có liên quan trước");
            }
         
        }
        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objPhongBan = _unitOfWork.PhongBanRepos.GetById(ID);
            if (objPhongBan.Active == true)
            {
                objPhongBan.Active = false;
            }
            else
            {
                objPhongBan.Active = true;
            }
            var currentUser = HttpContext.User;
            objPhongBan.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objPhongBan.ActiveDate = DateTime.Now;
            _unitOfWork.PhongBanRepos.Update(objPhongBan);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

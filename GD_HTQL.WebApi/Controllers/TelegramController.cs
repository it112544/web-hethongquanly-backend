﻿using GD_HTQL.WebApi.Telegram;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TelegramController : ControllerBase
    {
        private readonly WTelegramService WT;
        public TelegramController(WTelegramService wt)
        {
            WT = wt;
        }

        [HttpPost]
        [Route("SetCode")]
        public async Task<IActionResult> SetCode(string code)
        {
            string LoginStatus = await WT.DoLogin(code);
            return Ok(LoginStatus);
        }
    }
}

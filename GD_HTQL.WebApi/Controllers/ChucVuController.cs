﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ChucVuController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ChucVuController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {          
            return Ok(await _unitOfWork.ChucVuRepos.GetAll());
        }
        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetChucVuByTuKhoa(string Key, int _PhongBanID)
        {
            return Ok(await _unitOfWork.ChucVuRepos.GetChucVuByTuKhoa(Key,_PhongBanID));
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetChucVuById(int Id)
        {
            return Ok(_unitOfWork.ChucVuRepos.GetById(Id));
        }

        [HttpGet]
        [Route("GetByPhongBan/{Id}")]
        public async Task<IActionResult> GetByPhongBan(int Id)
        {
            return Ok(await _unitOfWork.ChucVuRepos.Find(x=>x.PhongBanID ==Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(ChucVuDto chucVuDto)
        {
            var objChucVu = _mapper.Map<ChucVu>(chucVuDto);
            objChucVu.Active = true;
            objChucVu.CreateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objChucVu.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            var result = await _unitOfWork.ChucVuRepos.Add(objChucVu);
            _unitOfWork.Save();
            if (result.ChucVuID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok(result);
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(ChucVuDto chucVuDto)
        {
            var objChucVu = _mapper.Map<ChucVu>(chucVuDto);
            var obj = _unitOfWork.ChucVuRepos.GetById(objChucVu.ChucVuID);
            objChucVu.Active = true;
            objChucVu.CreateDate = obj.CreateDate;
            objChucVu.CreateBy = obj.CreateBy;
            objChucVu.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objChucVu.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            _unitOfWork.ChucVuRepos.Update(objChucVu);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]      
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var objChucVu = _unitOfWork.ChucVuRepos.GetById(id);

            var objChuVuNhanVien =await _unitOfWork.ChucVuRepos.GetChucVuNhanVien(id);
            if (objChuVuNhanVien.Count() == 0)
            {
                _unitOfWork.ChucVuRepos.Remove(objChucVu);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
            else
            {
                return new JsonResult("Chức vụ đã phát sinh dữ liệu vui lòng xóa các dữ liệu có liên quan trước");
            }         
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objChucVu = _unitOfWork.ChucVuRepos.GetById(ID);
            if (objChucVu.Active == true)
            {
                objChucVu.Active = false;
            }
            else
            {
                objChucVu.Active = true;
            }
            var currentUser = HttpContext.User;
            objChucVu.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objChucVu.ActiveDate = DateTime.Now;
            _unitOfWork.ChucVuRepos.Update(objChucVu);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaThau;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NT_GiaTriCoHoiController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
  
        public NT_GiaTriCoHoiController(IUnitOfWork unitOfWork, IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork ??
                throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper;
            _thongbao = thongbao;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.NT_GiaTriCoHoiRepos.GetAll());
        }

        [HttpGet]
        [Route("GetNhanVien")]
        public async Task<IActionResult> GetNhanVien()
        {
            return Ok(await _unitOfWork.NT_GiaTriCoHoiRepos.GetNhanVien());
        }

        [HttpGet]
        [Route("GetDetails")]
        public async Task<IActionResult> GetDetails()
        {
            return Ok(await _unitOfWork.NT_GiaTriCoHoiRepos.GetDetails(0,0));
        }

        [HttpGet]
        [Route("GetbyNhanVien")]
        public async Task<IActionResult> GetbyNhanVien(int nhanvienID)
        {
            return Ok(await _unitOfWork.NT_GiaTriCoHoiRepos.Find(x => x.NhanVienID == nhanvienID));
        }
              
        [HttpGet]
        [Route("GetByNhaThau")]
        public async Task<IActionResult> GetByNhaThau(int nhaThauID)
        {
            return Ok(await _unitOfWork.NT_GiaTriCoHoiRepos.GetDetails(nhaThauID,0));
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetById(int Id)
        {
            return Ok(_unitOfWork.NT_GiaTriCoHoiRepos.GetById(Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(NT_GiaTriCoHoiDto NT_GiaTriCoHoiDto)
        {
            // Kiểm tra nhân viên báo cáo tiếp xúc                
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            if (_nhanVienID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Nhân viên không được tìm thấy");
            }
            else
            {
                var objNT_GiaTriCoHoi = _mapper.Map<NT_GiaTriCoHoi>(NT_GiaTriCoHoiDto);
                objNT_GiaTriCoHoi.LoaiSanPham = null;
                objNT_GiaTriCoHoi.NhanVienID = _nhanVienID;
                objNT_GiaTriCoHoi.Active = true;
                objNT_GiaTriCoHoi.CreateDate = DateTime.Now;             
                objNT_GiaTriCoHoi.CreateBy = ClassCommon.GetTenNhanVien(currentUser);              
                var result = await _unitOfWork.NT_GiaTriCoHoiRepos.Add(objNT_GiaTriCoHoi);
                _unitOfWork.Save();
                if (result.GTCHID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    //Thêm loại sản phẩm quan tâm
                    // Lưu môn học
                    if (NT_GiaTriCoHoiDto.LoaiSanPhamDto.Count() > 0)
                    {
                        var obj = NT_GiaTriCoHoiDto.LoaiSanPhamDto.Select(x => x.LoaiSanPhamID).ToList();
                        await _unitOfWork.NT_GiaTriCoHoiRepos.ThemLoaiSanPham(obj, result.GTCHID);
                    }
                    var objResult = await _unitOfWork.NT_GiaTriCoHoiRepos.GetDetails(0, result.GTCHID);
                    string tieude = objNT_GiaTriCoHoi.CreateBy.ToUpper() + " ĐÃ VỪA THÊM MỚI BÁO CÁO TIẾP XÚC";
                    await _thongbao.SendTeleNhaThau_CoHoi(result.GTCHID, tieude);

                    return Ok(objResult);
                }
            }
        }

        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Put(NT_GiaTriCoHoiDto NT_GiaTriCoHoiDto)
        {
            var objNT_GiaTriCoHoi = _mapper.Map<NT_GiaTriCoHoi>(NT_GiaTriCoHoiDto);
            var obj = _unitOfWork.NT_GiaTriCoHoiRepos.GetById(objNT_GiaTriCoHoi.GTCHID);
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);         
            objNT_GiaTriCoHoi.NhanVienID = _nhanVienID;
            objNT_GiaTriCoHoi.Active = true;      
            objNT_GiaTriCoHoi.CreateDate = obj.CreateDate;
            objNT_GiaTriCoHoi.CreateBy = obj.CreateBy;
            objNT_GiaTriCoHoi.UpdateDate = DateTime.Now;
            objNT_GiaTriCoHoi.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            _unitOfWork.NT_GiaTriCoHoiRepos.Update(objNT_GiaTriCoHoi);
            _unitOfWork.Save();
            // Cập nhật Thông Tư, Môn học, Khối lớp
            // Lưu môn học
            _unitOfWork.NT_GiaTriCoHoiRepos.XoaLoaiSanPham(NT_GiaTriCoHoiDto.GTCHID);
            if (NT_GiaTriCoHoiDto.LoaiSanPhamDto.Count() > 0)
            {
                var objLoai = NT_GiaTriCoHoiDto.LoaiSanPhamDto.Select(x => x.LoaiSanPhamID).ToList();
                await _unitOfWork.NT_GiaTriCoHoiRepos.ThemLoaiSanPham(objLoai, NT_GiaTriCoHoiDto.GTCHID);
            }
           
            string tieude = objNT_GiaTriCoHoi.UpdateBy.ToUpper()+ " ĐÃ VỪA CẬP NHẬT BÁO CÁO TIẾP XÚC";
            await _thongbao.SendTeleNhaThau_CoHoi(objNT_GiaTriCoHoi.GTCHID, tieude);
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var currentUser = HttpContext.User;
            var objCoHoi = _unitOfWork.NT_GiaTriCoHoiRepos.GetById(id);          
            if (objCoHoi != null)
            {
                string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + " ĐÃ VỪA XÓA BÁO CÁO TIẾP XÚC";
                await _thongbao.SendTeleNhaThau_CoHoi(id, tieude);

                _unitOfWork.NT_GiaTriCoHoiRepos.XoaLoaiSanPham(id);
                 _unitOfWork.NT_GiaTriCoHoiRepos.Remove(objCoHoi);
                 _unitOfWork.Save();

                return new JsonResult("Xóa thành công");
            }
            else
            {
                return new JsonResult("Id không được tìm thấy");
            }        
        }
    }
}

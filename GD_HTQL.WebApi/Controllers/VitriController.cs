﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VitriController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public VitriController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            var result = await _unitOfWork.ViTriRepos.GetAll();
            return Ok(result);
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetVitriById(int Id)
        {
            return Ok(_unitOfWork.ViTriRepos.GetById(Id));
        }
        [HttpGet]
        [Route("GetByNhanVien/{Id}")]
        public async Task <IActionResult> GetByNhanVien(int Id)
        {
            return Ok(await _unitOfWork.ViTriRepos.Find(x=>x.NhanVienID == Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(ViTriDto vitriDto)
        {
            var objViTri = _mapper.Map<ViTri>(vitriDto);
            var result = await _unitOfWork.ViTriRepos.Add(objViTri);
            _unitOfWork.Save();
            if (result.ViTriID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(ViTriDto vitriDto)
        {
            var objViTri = _mapper.Map<ViTri>(vitriDto);
            _unitOfWork.ViTriRepos.Update(objViTri);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objVitri = _unitOfWork.ViTriRepos.GetById(id);
            _unitOfWork.ViTriRepos.Remove(objVitri);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using GD_HTQL.WebApi.Services.IRepository;
using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Data.Models.Dtos.NghiPhep;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NghiPhepController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISendTelegramRepository _thongbao;
        public NghiPhepController(IUnitOfWork unitOfWork, IMapper mapper, ISendTelegramRepository thongbao)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _thongbao = thongbao;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupCongViec(currentUser) == true)
            {
                var lstCongViec = await _unitOfWork.NghiPhepRepos.GetDanhSach(0);
                return Ok(lstCongViec);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstCongViec = await _unitOfWork.NghiPhepRepos.GetDanhSach(0);
                    return Ok(lstCongViec);
                }
            }
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public NghiPhep GetById(int Id)
        {
            return _unitOfWork.NghiPhepRepos.GetById(Id);
        }
        [HttpGet]
        [Route("GetDetailsById/{Id}")]
        public async Task<IActionResult> GetDetailsById(int Id)
        {
            return Ok(await _unitOfWork.NghiPhepRepos.GetDetailsByID(Id));
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add(NghiPhepDto NghiPhepDto)
        {
            var currentUser = HttpContext.User;
            int _nhanvienID = ClassCommon.GetUserID(currentUser);
            string _tennhanvien = ClassCommon.GetTenNhanVien(currentUser);
            var objNghiPhep = _mapper.Map<NghiPhep>(NghiPhepDto);        
            objNghiPhep.CreateDate = DateTime.Now;

            objNghiPhep.CreateBy = _tennhanvien;
            objNghiPhep.NguoiTaoID = _nhanvienID;

            // Lấy thông tin nhân viên 
            var objNhanVien = await _unitOfWork.NhanVienRepos.GetDetails(_nhanvienID);
            if (objNhanVien != null)
            {
                objNghiPhep.HoTen = objNhanVien.TenNhanVien;
                if (objNhanVien.lstChucVuView.Count() > 0)
                {
                    var objChuVuView = objNhanVien.lstChucVuView.FirstOrDefault();
                    if (objChuVuView != null)
                    {
                        objNghiPhep.ChucVuID = objChuVuView.lstChucVu == null ? 0 : objChuVuView.lstChucVu.ChucVuID;
                        objNghiPhep.TenChucVu = objChuVuView.lstChucVu == null ? "" : objChuVuView.lstChucVu.TenChucVu;

                        objNghiPhep.PhongBanID = objChuVuView.lstPhongBan == null ? 0 : objChuVuView.lstPhongBan.PhongBanID;
                        objNghiPhep.TenPhongBan = objChuVuView.lstPhongBan == null ? "" : objChuVuView.lstPhongBan.TenPhongBan;

                        objNghiPhep.CongTyID = objChuVuView.lstCongTy == null ? 0 : objChuVuView.lstCongTy.CongTyID;
                        objNghiPhep.TenCongTy = objChuVuView.lstCongTy == null ? "" : objChuVuView.lstCongTy.TenCongTy;
                    }
                }
            }
            var result = await _unitOfWork.NghiPhepRepos.Add(objNghiPhep);
            _unitOfWork.Save();
            if (result.NghiPhepID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                // Add lịch sử duyệt nghỉ phép     
                NghiPhep_LichSu LichSu = new NghiPhep_LichSu();
                LichSu.NghiPhepID = result.NghiPhepID;
                LichSu.NhanVienID = result.NguoiTaoID;
                LichSu.ThoiGan = DateTime.Now;
                LichSu.TrangThaiID = 1;
                LichSu.NguoiDuyetID = NghiPhepDto.NguoiDuyetID;
                LichSu.TenNguoiDuyet = NghiPhepDto.TenNguoiDuyet;
                var resultLichSu = await _unitOfWork.NghiPhep_LichSuRepos.Add(LichSu);
                _unitOfWork.Save();
                try
                {
                    string tieude = "ĐƠN XIN NGHỈ PHÉP";
                    await _thongbao.SendTelegramNghiPhep(result, tieude);
                }
                catch
                {
                    return Ok(result);
                }              
                return Ok(result);
            }
        }

        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update(NghiPhepDto NghiPhepDto)
        {
            var objNghiPhep = _mapper.Map<NghiPhep>(NghiPhepDto);
            var obj = _unitOfWork.NghiPhepRepos.GetById(objNghiPhep.NghiPhepID);
           
            objNghiPhep.CreateDate = obj.CreateDate;
            objNghiPhep.CreateBy = obj.CreateBy;

            objNghiPhep.ChucVuID = obj.ChucVuID;
            objNghiPhep.TenChucVu = obj.TenChucVu;
            objNghiPhep.PhongBanID = obj.PhongBanID;
            objNghiPhep.TenPhongBan = obj.TenPhongBan;
            objNghiPhep.CongTyID = obj.CongTyID;
            objNghiPhep.TenCongTy = obj.TenCongTy;
            objNghiPhep.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objNghiPhep.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            objNghiPhep.NguoiTaoID = ClassCommon.GetUserID(currentUser);
            _unitOfWork.NghiPhepRepos.Update(objNghiPhep);
            _unitOfWork.Save();
            try
            {
                string tieude = string.IsNullOrEmpty(objNghiPhep.HoTen) ? "" : objNghiPhep.HoTen.ToUpper() + ": CẬP NHẬT ĐƠN XIN NGHỈ PHÉP";
                await _thongbao.SendTelegramNghiPhep(objNghiPhep, tieude);
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }         
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var objDelete = _unitOfWork.NghiPhepRepos.GetById(id);
            // Xóa lịch sử duyệt
            var objLichSu = await _unitOfWork.NghiPhep_LichSuRepos.Find(x => x.NghiPhepID == id);
            if (objLichSu.Count() > 0)
            {
                _unitOfWork.NghiPhep_LichSuRepos.RemoveRange(objLichSu);
                _unitOfWork.Save();
            }
            try
            {
                string tieude = string.IsNullOrEmpty(objDelete.HoTen) ? "" : objDelete.HoTen.ToUpper() + ": XÓA ĐƠN XIN NGHỈ PHÉP";
                await _thongbao.SendTelegramNghiPhep(objDelete, tieude);
            }
            catch (Exception ex)
            {
                _unitOfWork.NghiPhepRepos.Remove(objDelete);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }         
            _unitOfWork.NghiPhepRepos.Remove(objDelete);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }

        [HttpPut]
        [Route("CapNhatTrangThai")]
        public async Task<IActionResult> CapNhatTrangThai(NghiPhep_LichSuDto lsDto)
        {
            var currentUser = HttpContext.User;
            int _nhanVienID = ClassCommon.GetUserID(currentUser);
            // Add lịch sử kế hoạch công tác ngày      
            var objLichSu = _mapper.Map<NghiPhep_LichSu>(lsDto);
            objLichSu.NhanVienID = _nhanVienID;
            objLichSu.ThoiGan = DateTime.Now;         
            var resultLichSu = await _unitOfWork.NghiPhep_LichSuRepos.Add(objLichSu);
            _unitOfWork.Save();               
            try
            {                
                var objPhep = _unitOfWork.NghiPhepRepos.GetById(objLichSu.NghiPhepID);
                if (objPhep !=null)
                {
                    string tieude = ClassCommon.GetTenNhanVien(currentUser).ToUpper() + ": ĐÃ DUYỆT ĐƠN XIN NGHỈ PHÉP CỦA " + objPhep.HoTen.ToUpper();
                    await _thongbao.SendTelegramNghiPhep(objPhep, tieude);
                }              
            }
            catch
            {
                return Ok("Cập nhật thành công");
            }
            return Ok("Cập nhật thành công");
        }
      
        [HttpGet]
        [Route("GetTrangThai")]
        public async Task<IActionResult> GetTrangThai()
        {
            var result = await _unitOfWork.NghiPhepRepos.GetTrangThai();
            return Ok(result);
        }
        [HttpGet]
        [Route("GetLoai")]
        public async Task<IActionResult> GetLoai()
        {
            var result = await _unitOfWork.NghiPhepRepos.GetLoaiNghiPhep();
            return Ok(result);
        }
        [HttpGet]
        [Route("GetByNhanVien")]
        public async Task<IActionResult> GetByNhanVien(int nhanvienID)
        {
            var result = await _unitOfWork.NghiPhepRepos.GetByNhanVien(nhanvienID);
            return Ok(result);
        }
    }
}

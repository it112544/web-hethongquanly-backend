﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CongTyController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CongTyController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.CongTyRepos.GetAll());
        }
        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetCongTyByTuKhoa(string Key, int TinhID)
        {
            // Quy ước TinhID = 0 lấy tất cả các cơ quan
            return Ok(await _unitOfWork.CongTyRepos.GetCongTyByTuKhoa(Key, TinhID));
        }
        [HttpGet]
        [Route("GetNhanVien")]
        public async Task<IActionResult> GetNhanVien( int ctyID)
        {
            // Quy ước TinhID = 0 lấy tất cả các cơ quan
            return Ok(await _unitOfWork.CongTyRepos.GetNhanVien(ctyID));
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetCongTyById(int Id)
        {
            return Ok(_unitOfWork.CongTyRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(CongTyDto congTyDto)
        {
            var objCongty = _mapper.Map<CongTy>(congTyDto);
            objCongty.Active = true;
            objCongty.CreateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objCongty.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            var result = await _unitOfWork.CongTyRepos.Add(objCongty);

            _unitOfWork.Save();
            if (result.CongTyID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok(result);
            }
        }
        [HttpPut]
        [Route("Update")]
        public IActionResult Put(CongTyDto congTyDto)
        {
            var objCongty = _mapper.Map<CongTy>(congTyDto);
            var obj = _unitOfWork.CongTyRepos.GetById(objCongty.CongTyID);
            objCongty.Active = true;       
            objCongty.CreateDate = obj.CreateDate;
            objCongty.CreateBy = obj.CreateBy;
            objCongty.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;         
            objCongty.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
            _unitOfWork.CongTyRepos.Update(objCongty);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]        
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var objCongty = _unitOfWork.CongTyRepos.GetById(id);
            var objPhongBan = await _unitOfWork.PhongBanRepos.Find(x => x.CongTyID == id);
            if (objPhongBan.Count() == 0)
            {
                _unitOfWork.CongTyRepos.Remove(objCongty);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
            else
            {
                return new JsonResult("Công ty đã phát sinh dữ liệu vui lòng xóa các dữ liệu có liên quan trước");
            }                    
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objCongty = _unitOfWork.CongTyRepos.GetById(ID);
            if (objCongty.Active == true)
            {
                objCongty.Active = false;
            }
            else
            {
                objCongty.Active = true;
            }
            var currentUser = HttpContext.User;
            objCongty.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objCongty.ActiveDate = DateTime.Now;
            _unitOfWork.CongTyRepos.Update(objCongty);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

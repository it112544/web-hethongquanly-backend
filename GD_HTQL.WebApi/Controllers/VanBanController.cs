﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VanBanController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public VanBanController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetVanBan")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.VanBanRepos.GetAll());
        }
        [HttpGet]
        [Route("GetVanBanByLoai/{LoaiID}")]
        public async Task<IActionResult> GetByLoai(int LoaiID)
        {
            return Ok(await _unitOfWork.VanBanRepos.Find(x => x.LoaiVanBanID == LoaiID));
        }

        [HttpGet]
        [Route("GetVanBanById/{Id}")]
        public IActionResult GetVanBanById(int Id)
        {
            return Ok(_unitOfWork.VanBanRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddVanBan")]
        public async Task<IActionResult> Post( VanBanModel vanBanModel)
        {       
            var objVanBan = _mapper.Map<VanBan>(vanBanModel.VanBanDto);
            var result = await _unitOfWork.VanBanRepos.Add(objVanBan);
            _unitOfWork.Save();
            if (result.VanBanID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                if (vanBanModel.lstFile != null)
                {
                    await _unitOfWork.FileRepos.PostFileVanBan(vanBanModel.lstFile, result.VanBanID);
                }
                return Ok("Thêm mới thành công");
            }        
        }
        [HttpPut]
        [Route("UpdateVanBan")]
        public async Task<IActionResult> Put(VanBanModel vanBanModel)
        {
            var objVanBan = _mapper.Map<VanBan>(vanBanModel.VanBanDto);
             _unitOfWork.VanBanRepos.Update(objVanBan);
            _unitOfWork.Save();
            if (vanBanModel.lstFile != null)
            {
                await _unitOfWork.FileRepos.PostFileVanBan(vanBanModel.lstFile, objVanBan.VanBanID);
            }
            return Ok("Thêm mới thành công");            
        }

        [HttpDelete]      
        [Route("DeleteVanBan")]
        public JsonResult Delete(int id)
        {
            var objVanBan = _unitOfWork.VanBanRepos.GetById(id);
            _unitOfWork.VanBanRepos.Remove(objVanBan);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

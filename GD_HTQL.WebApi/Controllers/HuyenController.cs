﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.DanhMuc;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class HuyenController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public HuyenController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetHuyenByTinhID/{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            var result = await _unitOfWork.DMHuyenRepos.Find(x=>x.TinhID ==Id);
            return Ok(result);
        }
        [HttpGet]
        [Route("GetHuyenById/{Id}")]
        public ActionResult GetHuyenById(int Id)
        {
            return Ok(_unitOfWork.DMHuyenRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddHuyen")]
        public async Task<IActionResult> Post(DMHuyenDto huyenDto)
        {
            var objHuyen = _mapper.Map<DMHuyen>(huyenDto);
            var result = await _unitOfWork.DMHuyenRepos.Add(objHuyen);
            _unitOfWork.Save();
            if (result.HuyenID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("UpdateHuyen")]
        public async Task<IActionResult> Put(DMHuyenDto huyenDto)
        {
            var objHuyen = _mapper.Map<DMHuyen>(huyenDto);
            _unitOfWork.DMHuyenRepos.Update(objHuyen);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("DeleteHuyen")]
        public JsonResult Delete(int id)
        {
            var objHuyen = _unitOfWork.DMHuyenRepos.GetById(id);
            _unitOfWork.DMHuyenRepos.Remove(objHuyen);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

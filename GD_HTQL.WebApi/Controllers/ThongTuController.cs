﻿using AutoMapper;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ThongTuController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ThongTuController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetListDetails")]
        public async Task<IActionResult> GetListDetails()
        {                    
            var result = await _unitOfWork.ThongTuRepos.GetListDetails();
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _unitOfWork.ThongTuRepos.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetThongTuById(int Id)
        {
            return Ok(_unitOfWork.ThongTuRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(ThongTuDto ThongTuDto)
        {
            var objThongTu = _mapper.Map<ThongTu>(ThongTuDto);
            objThongTu.Active = true;
            var result = await _unitOfWork.ThongTuRepos.Add(objThongTu);
            _unitOfWork.Save();
            if (result.ThongTuID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("AddListSanPham")]
        public IActionResult AddListSanPham(AddSanPhamThongTu addSanPham)
        {       
            _unitOfWork.ThongTuRepos.AddListSanPhamThongTu(addSanPham);  
            return Ok("Thêm mới thành công");
        }

        [HttpPut]
        [Route("Update")]
        public IActionResult Put(ThongTuDto ThongTuDto)
        {
            var objThongTu = _mapper.Map<ThongTu>(ThongTuDto);
            objThongTu.Active = true;
            _unitOfWork.ThongTuRepos.Update(objThongTu);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpGet]
        [Route("GetMonHoc/{thongTuID}")]
        public async Task<IActionResult> GetMonHoc(int thongTuID)
        {                       
            return Ok(await _unitOfWork.ThongTuRepos.GetMonHoc(thongTuID));
        }

        [HttpPost]
        [Route("AddListMonHoc")]
        public IActionResult AddListMonHoc(AddMonHocThongTu addMonHoc)
        {
            _unitOfWork.ThongTuRepos.AddListMonHocThongTu(addMonHoc);
            return Ok("Thêm mới thành công");
        }

        [HttpPost]
        [Route("FindSanPham")]
        public async Task<IActionResult> FindSanPham(List<IndexSanPham> indexSanPham)
        {
            var result = await _unitOfWork.ThongTuRepos.FindSanPham(indexSanPham);
            return Ok(result);
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objThongTu = _unitOfWork.ThongTuRepos.GetById(ID);
            if (objThongTu.Active == true)
            {
                objThongTu.Active = false;
            }
            else
            {
                objThongTu.Active = true;
            }            
            _unitOfWork.ThongTuRepos.Update(objThongTu);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            var objThongTu = _unitOfWork.ThongTuRepos.GetById(id);
            _unitOfWork.ThongTuRepos.Remove(objThongTu);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
    }
}

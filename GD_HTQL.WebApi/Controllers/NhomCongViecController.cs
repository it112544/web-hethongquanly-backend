﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.CongViec;
using GD_HTQL.WebApi.Data.Models.EF.CongViecModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NhomCongViecController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public NhomCongViecController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;   
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.NhomCongViecRepos.GetAll());
        }
        [HttpGet]
        [Route("GetById/{Id}")]
        public IActionResult GetNhomCongViecById(int Id)
        {
            return Ok(_unitOfWork.NhomCongViecRepos.GetById(Id));
        }
        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Post(NhomCongViecDto objDto)
        {
            // Kiểm tra nhân viên báo cáo tiếp xúc                        
            var currentUser = HttpContext.User;
            var objNhom = _mapper.Map<NhomCongViec>(objDto);
            objNhom.CreateDate = DateTime.Now;
            objNhom.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
            objNhom.Active = true;
            var result = await _unitOfWork.NhomCongViecRepos.Add(objNhom);
            _unitOfWork.Save();
            if (result.NhomCongViecID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }

        [HttpPut]
        [Route("Update")]
        public IActionResult Put(NhomCongViecDto objDto)
        {
            var objNhom = _mapper.Map<NhomCongViec>(objDto);
            var obj = _unitOfWork.CongViecRepos.GetById(objNhom.NhomCongViecID);
            objNhom.Active = true;
            objNhom.CreateDate = obj.CreateDate;
            objNhom.CreateBy = obj.CreateBy;
            objNhom.UpdateDate = DateTime.Now;
            var currentUser = HttpContext.User;
            objNhom.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);         
            _unitOfWork.NhomCongViecRepos.Update(objNhom);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<JsonResult> Delete(int id)
        {
            var objNhom = _unitOfWork.NhomCongViecRepos.GetById(id);
            var objCongViec =await _unitOfWork.CongViecRepos.Find(x => x.NhomCongViecID == id);
            if (objCongViec.Count() == 0)
            {
                _unitOfWork.NhomCongViecRepos.Remove(objNhom);
                _unitOfWork.Save();
                return new JsonResult("Xóa thành công");
            }
            else
            {
                //_unitOfWork.CongViecRepos.RemoveRange(objCongViec);
                //_unitOfWork.Save();
                return new JsonResult("Đã có công việc thuộc nhóm này, vui lòng xóa công việc trước");
            }                     
        }

        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objNhom = _unitOfWork.NhomCongViecRepos.GetById(ID);
            if (objNhom.Active == true)
            {
                objNhom.Active = false;
            }
            else
            {
                objNhom.Active = true;
            }

            var currentUser = HttpContext.User;
            objNhom.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objNhom.ActiveDate = DateTime.Now;
            _unitOfWork.NhomCongViecRepos.Update(objNhom);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

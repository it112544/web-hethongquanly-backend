﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models.Dtos.NhaCungCap;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NhaCungCapController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public NhaCungCapController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork ??
                throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNhaCungCap")]
        public async Task<IActionResult> GetNhaCungCap()
        {        
            var currentUser = HttpContext.User;
            if (CheckRoles.CheckRoleGroupDoiTac(currentUser) == true)
            {
                var lstNhaCungCap = await _unitOfWork.NhaCungCapRepos.Find(x => x.Active == true);
                return Ok(lstNhaCungCap);
            }
            else
            {
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                if (_nhanVienID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                else
                {
                    var lstNhaCungCap = await _unitOfWork.NhaCungCapRepos.GetbyNhanVien(_nhanVienID);
                    return Ok(lstNhaCungCap);
                }
            }                    
        }
        [HttpGet]
        [Route("GetByTuKhoa")]
        public async Task<IActionResult> GetNhaCungCapByTuKhoa(string? Key ,int tinhID )
        {
            return Ok(await _unitOfWork.NhaCungCapRepos.GetNhaCungCapByTuKhoa(Key, tinhID));
        }
        [HttpGet]
        [Route("GetNhaCungCapById/{Id}")]
        public IActionResult GetNhaCungCapById(int Id)
        {
            return Ok (_unitOfWork.NhaCungCapRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddNhaCungCap")]
        public async Task<IActionResult> AddNhaCungCap(NhaCungCapDto nhaCungCapDto)
        {
            if (!String.IsNullOrWhiteSpace(nhaCungCapDto.MaSoThue) && _unitOfWork.NhaThauRepos.CheckMaSoThue(nhaCungCapDto.MaSoThue, 0) == false)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã số thuế đã tồn tại đã tồn tại");
            }
            else
            {
                var objNhaCungCap = _mapper.Map<NhaCungCap>(nhaCungCapDto);
                objNhaCungCap.Active = true;
                objNhaCungCap.CreateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                objNhaCungCap.CreateBy = ClassCommon.GetTenNhanVien(currentUser);
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                objNhaCungCap.NhanVienID = _nhanVienID;
                var result = await _unitOfWork.NhaCungCapRepos.Add(objNhaCungCap);
                _unitOfWork.Save();
                if (result.NhaCungCapID == 0)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
                }
                return Ok(result);
            }          
        }
        [HttpPut]
        [Route("UpdateNhaCungCap")]
        public IActionResult UpdateNhaCungCap(NhaCungCapDto nhaCungCapDto)
        {
            if (!String.IsNullOrWhiteSpace(nhaCungCapDto.MaSoThue) && _unitOfWork.NhaThauRepos.CheckMaSoThue(nhaCungCapDto.MaSoThue, nhaCungCapDto.NhaCungCapID) == false)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Mã số thuế đã tồn tại đã tồn tại");
            }
            else
            {
                var objNhaCungCap = _mapper.Map<NhaCungCap>(nhaCungCapDto);
                objNhaCungCap.Active = true;
                var obj = _unitOfWork.NhaCungCapRepos.GetById(objNhaCungCap.NhaCungCapID);

                objNhaCungCap.CreateDate = obj.CreateDate;
                objNhaCungCap.CreateBy = obj.CreateBy;
                objNhaCungCap.UpdateDate = DateTime.Now;
                var currentUser = HttpContext.User;
                int _nhanVienID = ClassCommon.GetUserID(currentUser);
                objNhaCungCap.UpdateBy = ClassCommon.GetTenNhanVien(currentUser);
                objNhaCungCap.NhanVienID = _nhanVienID;
                _unitOfWork.NhaCungCapRepos.Update(objNhaCungCap);
                _unitOfWork.Save();
                return Ok("Cập nhật thành công");
            }
         
        }
        [HttpDelete]      
        [Route("DeleteNhaCungCap")]
        public JsonResult DeleteNhaCungCap(int id)
        {
            var objNhaCungCap = _unitOfWork.NhaCungCapRepos.GetById(id);
            _unitOfWork.NhaCungCapRepos.Remove(objNhaCungCap);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objNhaCungCap = _unitOfWork.NhaCungCapRepos.GetById(ID);
            if (objNhaCungCap.Active == true)
            {
                objNhaCungCap.Active = false;
            }
            else
            {
                objNhaCungCap.Active = true;
            }
            var currentUser = HttpContext.User;
            objNhaCungCap.ActiveBy = ClassCommon.GetTenNhanVien(currentUser);
            objNhaCungCap.ActiveDate = DateTime.Now;
            _unitOfWork.NhaCungCapRepos.Update(objNhaCungCap);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpGet]
        [Route("CheckMaSoThue")]
        public IActionResult CheckMaSoThue(string maSoThue, int nccID)
        {
            if (string.IsNullOrWhiteSpace(maSoThue))
            {
                bool Check = _unitOfWork.NhaCungCapRepos.CheckMaSoThue(maSoThue, nccID);
                if (Check)
                {
                    return Ok(new { message = true });
                }
                else
                {
                    return Ok(new { message = false });
                }
            }
            else
            {
                return Ok(new { message = true });
            }
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GD_HTQL.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class KhoiLopController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public KhoiLopController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetKhoiLop")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _unitOfWork.KhoiLopRepos.GetAll());
        }
        [HttpGet]
        [Route("GetKhoiLopById/{Id}")]
        public IActionResult GetKhoiLopById(int Id)
        {
            return Ok(_unitOfWork.KhoiLopRepos.GetById(Id));
        }
        [HttpPost]
        [Route("AddKhoiLop")]
        public async Task<IActionResult> Post(KhoiLopDto khoiLopDto)
        {
            var objKhoiLop = _mapper.Map<KhoiLop>(khoiLopDto);
            var result = await _unitOfWork.KhoiLopRepos.Add(objKhoiLop);
            _unitOfWork.Save();
            if (result.KhoiLopID == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Đã xảy ra lỗi");
            }
            else
            {
                return Ok("Thêm mới thành công");
            }
        }
        [HttpPut]
        [Route("UpdateKhoiLop")]
        public async Task<IActionResult> Put(KhoiLopDto khoiLopDto)
        {
            var objKhoiLop = _mapper.Map<KhoiLop>(khoiLopDto);
            _unitOfWork.KhoiLopRepos.Update(objKhoiLop);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
        [HttpDelete]
        [Route("DeleteKhoiLop")]
        public JsonResult Delete(int id)
        {
            var objKhoiLop = _unitOfWork.KhoiLopRepos.GetById(id);
            _unitOfWork.KhoiLopRepos.Remove(objKhoiLop);
            _unitOfWork.Save();
            return new JsonResult("Xóa thành công");
        }
        [HttpPut]
        [Route("changeActive")]
        public IActionResult changeActive(int ID)
        {
            var objKhoiLop = _unitOfWork.KhoiLopRepos.GetById(ID);
            if (objKhoiLop.Active == true)
            {
                objKhoiLop.Active = false;
            }
            else
            {
                objKhoiLop.Active = true;
            }
            _unitOfWork.KhoiLopRepos.Update(objKhoiLop);
            _unitOfWork.Save();
            return Ok("Cập nhật thành công");
        }
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ILoaiVanBanRepository : IGenericRepository<LoaiVanBan>
    {
    }
}

﻿using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IKHCT_NgayRepository : IGenericRepository<KHCT_Ngay>
    {
        Task AddLichSu(KHCT_Ngay_LichSu objLichSu);
        Task<ICollection<KHCT_Ngay_ShortView>> LstKHCT_NgayView(int nhanVienID , bool week, bool month);
        Task<KHCT_Ngay_ViewModel> GetDetailsByID(int ngayID);
        Task<List<KHCT_Ngay_TrangThai>> GetTrangThai();
    }

}

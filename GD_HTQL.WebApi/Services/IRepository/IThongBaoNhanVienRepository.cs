﻿using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.ThongBaoModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IThongBaoNhanVienRepository : IGenericRepository<ThongBaoNhanVien> 
    {
        Task<IEnumerable<NhanVienShortView>> GetNhanVien(List<int> nhanVienID);
        void AddMulti(ThongBaoNhanVienCustom thongBaoNhanVien);
    }
}

﻿using GD_HTQL.WebApi.Data.Models.Dtos;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IXaRepository : IGenericRepository<DMXa>
    {
    }
}

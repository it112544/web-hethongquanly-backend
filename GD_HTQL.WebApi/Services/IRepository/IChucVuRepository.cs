﻿using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IChucVuRepository : IGenericRepository<ChucVu>
    {       
        Task<IEnumerable<ChucVu>> GetChucVuByTuKhoa(string Key, int PhongBanID);
        bool DoiTrangThai(int ID);
        Task<IEnumerable<ChucVuNhanVien>> GetChucVuNhanVien( int chucVuID);
        Task<IEnumerable<ChucVuNhanVien>> GetByNhanVien(int nhanVienID);
    }
}

﻿using GD_HTQL.WebApi.Data.Migrations;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IDuToanRepository : IGenericRepository<DuToan>
    {      
        Task<IEnumerable<DuToan>> GetDuToanByTuKhoa(string? Key);             
        Task CapNhatTrangThai(int NhanVienID, int TrangThaiID, int DuToanID);
        Task AddLichSuDuToan(LichSuDuToan objLichSu);
        Task DeleteLichSuDuToan(int duToanID);
        Task<IEnumerable<DuToanViewModel>> GetDanhSach();

        Task<IEnumerable<TrangThaiDuToan>> GetDsTrangThai();
    }
}

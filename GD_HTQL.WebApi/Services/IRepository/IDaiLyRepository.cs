﻿using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IDaiLyRepository : IGenericRepository<DaiLy>
    {
        Task<IEnumerable<DaiLy>> GetDaiLyByTuKhoa(string? Key, int tinhID);
        Task<IEnumerable<DaiLy>> GetbyNhanVien(int nhanvienID);
        bool CheckMaSoThue(string? _maSoThue, int _daiLyID);
    }
}

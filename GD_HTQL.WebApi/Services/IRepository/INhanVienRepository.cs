﻿using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using System.Security.Claims;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INhanVienRepository : IGenericRepository<NhanVien>
    {
        Task<IEnumerable<NhanVienViewModel>> GetNhanVien(bool isActive);
        Task<IEnumerable<NhanVien>> GetNhanVienByTuKhoa(string Key);
        Task<NhanVien> GetNhanVienByUserID(string ID);     
        Task<ChucVuNhanVien> ThemChucVu(ChucVuNhanVien objViTriNhanVien);
        bool XoaChucVu(ChucVuNhanVien objViTriNhanVien);
        Task<NhanVien> DoiTrangThai(int ID, ClaimsPrincipal User);
        Task<NhanVienDetails> GetDetails(int ID);
        Task<List<NhanVienDto>> GetByChucVu(int ChucVuID);
        bool CheckChucVuExits(ChucVuNhanVien chucVuNhanVien);
        Task<List<FileLoai>> GetLoaiFile();
    }
}

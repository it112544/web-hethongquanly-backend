﻿using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ICongTyRepository : IGenericRepository<CongTy>
    {   
        // Quy ước TinhID = 0 lấy tất cả các cơ quan
        Task<IEnumerable<CongTy>> GetCongTyByTuKhoa(string Key, int TinhID);
        Task<IEnumerable<NhanVienShortView>> GetNhanVien(int ctyID);
        bool DoiTrangThai(int ID);
    }
}

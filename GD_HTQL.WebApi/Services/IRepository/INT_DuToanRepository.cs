﻿using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INT_DuToanRepository : IGenericRepository<NT_DuToan>
    {
        Task<string> SaveFile(string fileBase64, string fileName);
        Task<List<NT_TrangThaiDuToan>> GetTrangThai();
        Task<List<NT_DuToanView>> GetDanhSach(int nhaThauID);
        void DeleteFile(string path);
    }
}

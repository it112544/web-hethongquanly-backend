﻿using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IKHCT_TuanRepository : IGenericRepository<KHCT_Tuan>
    {
        DateOfWeekModel GetDateOfWeekModel(int thangID, int tuan);
    }
}

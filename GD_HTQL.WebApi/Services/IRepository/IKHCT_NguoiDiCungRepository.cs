﻿using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IKHCT_NguoiDiCungRepository : IGenericRepository<KHCT_NguoiDiCung>
    {
    }
}

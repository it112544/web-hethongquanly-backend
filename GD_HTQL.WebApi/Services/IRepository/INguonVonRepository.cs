﻿using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INguonVonRepository : IGenericRepository<NguonVon>
    {
    }
}

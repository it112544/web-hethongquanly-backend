﻿using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IQuanLyTuongTacRepository : IGenericRepository<QuanLyTuongTac>
    {
        Task<IEnumerable<TuongTacViewModel>> GetQuanLyTuongTac();
        Task<IEnumerable<TuongTacViewModel>> GetQuanLyTuongTacByTuKhoa(string? Key, int tinhID);

        Task<IEnumerable<BuocThiTruong>> GetBuocThiTruong();
        Task<IEnumerable<NSNguonVon>> GetNguonVon();
        Task<IEnumerable<TuongTacDetails>> GetDetails();
    }
}

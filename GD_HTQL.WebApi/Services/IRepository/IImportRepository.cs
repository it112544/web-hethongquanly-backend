﻿using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.EF.TacGiaModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IImportRepository 
    {
        Task<List<NhaCungCap>> ImportNhaCungCap(List<NhaCungCap> nhacungcap);
        Task<List<NhaThau>> ImportNhaThau(List<NhaThau> nhathau);
        Task<List<DaiLy>> ImportDaiLy(List<DaiLy> daily);
        Task<List<TacGia>> ImportTacGia(List<TacGia> tacgia);
        Task<NSCoQuan> ImportCoQuan(NSCoQuan coquan);
        Task<NSCanBo> ImportCanBo(NSCanBo canbo);
        Task<List<QuanLyTuongTac>> ImportTuongTac(List<QuanLyTuongTac> tuongtac);
        Task<List<SanPham>> ImportSanPham(List<SanPham> sanPham);
        Task<int> GetTinhID(string _tenTinh);
        Task<int> GetHuyenID(string _tenHuyen ,int _tinhID);
        Task<int> GetXaID(string tenxa, int huyenID);
        Task<int> GetNhanVienIDFromUserName(string _userName);
        Task<bool> CheckMaSanPham(string _maSanPham);
        Task<int> GetCoQuanIDByName(string _tenCoQuan);
        Task<int> GetMonHocIDByName(string _tenMonHoc);
        Task<int> GetKhoiLopIDByName(string _tenKhoiLop);
        Task<int> GetLoaiSanPhamIDByName(string _tenLoaiSanPham);
        Task<int> GetBuocThiTruongID(string _buocThiTruongTen);
        Task<int> GetLoaiNhaCungCapID(string ten);
        Task<int> GetLoaiNhaThauID(string ten);
        Task<int> GetLoaiHinhHopTacID(string ten);
        Task<int> GetDiaBanHoatDongID(string ten);
        Task<int> GetLoaiDaiLyID(string ten);
        bool CheckDecimal(string _decimal);
        bool CheckDateTime(string _datetime);

        SanPham GetLikeTenSanPham(string tenSanPham);

        Task<bool> CheckMST_NhaThau(string _MST);
        Task<bool> CheckMST_NhaCungCap(string _MST);
        Task<bool> CheckMST_DaiLy(string _MST);
        Task<bool> CheckMST_CoQuan(string _MST);

        Task InsertSanPham_Monhoc(string pMonHoc, int sanphamID);
        Task InsertSanPham_ThongTu(string pThongTu, int sanphamID);
        Task InsertSanPham_Lop(string pLop, int sanphamID);
        Task<int> GetDonViTinh(string TenDonViTinh);
    }
}

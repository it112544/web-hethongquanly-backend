﻿using GD_HTQL.WebApi.Data.Models.Dtos.CongViec;
using GD_HTQL.WebApi.Data.Models.Dtos.NhanVien;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.EF.VanBanModels;
using GD_HTQL.WebApi.Infrastructure;


namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IFileRepository : IGenericRepository<FileDinhKem>
    {
        public Task PostFileVanBan(List<IFormFile> fileData, int VanBanID);
        Task<IEnumerable<FileDinhKem>> GetFileDinhKemByNhanVienID(int TinhID);
        Task<IEnumerable<FileVanBan>> GetFileVanBanByVanBanID(int TinhID);
        bool DeleteFileVanBan(int ID);
        Task AddFileDinhKem(List<FileDinhKemDto> fileData, int NhanVienID);
        Task AddFileCongViec(List<FileCongViecDto> fileData, int congViecID); 
    }
}

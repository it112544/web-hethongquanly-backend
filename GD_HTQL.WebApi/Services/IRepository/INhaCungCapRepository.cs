﻿using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INhaCungCapRepository : IGenericRepository<NhaCungCap>
    {      
        Task<IEnumerable<NhaCungCap>> GetNhaCungCapByTuKhoa(string? Key,int tinhID);
        Task<IEnumerable<NhaCungCap>> GetbyNhanVien(int nhanvienID);
        bool CheckMaSoThue(string? _maSoThue, int _nccID);
    }
}

﻿using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IKHCT_ChiPhiRepository : IGenericRepository<KHCT_ChiPhi>
    {
        Task<IEnumerable<KHCT_HangMucChiPhi>> GetDsHangMuc();
        Task<IEnumerable<KHCT_LoaiChiPhi>> GetDsLoai();
        Task<IEnumerable<KHCT_ChiPhi>> GetDsChiPhi(int KHCT_Id);
    }
}

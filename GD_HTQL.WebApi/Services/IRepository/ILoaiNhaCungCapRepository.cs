﻿using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ILoaiNhaCungCapRepository : IGenericRepository<LoaiNhaCungCap>
    {
    }
}

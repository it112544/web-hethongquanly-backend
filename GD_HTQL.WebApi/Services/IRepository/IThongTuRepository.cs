﻿using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IThongTuRepository : IGenericRepository<ThongTu>
    {
        void AddListSanPhamThongTu( AddSanPhamThongTu addSanPham);
        Task<List<MonHocDto>> GetMonHoc(int thongTuID);
        void AddListMonHocThongTu(AddMonHocThongTu addMonHoc);
        Task<List<ThongTuViewModel>> GetListDetails();
        Task<List<IndexSanPhamView>> FindSanPham(List<IndexSanPham> lstSanPham);
    }
}

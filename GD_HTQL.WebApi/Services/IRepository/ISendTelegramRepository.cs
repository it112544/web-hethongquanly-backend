﻿using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Data.Models.MessageTelegram;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ISendTelegramRepository
    {
        Task SendTeleNhaThau_DuToan(int ID, string tieude);
        Task SendTeleNhaThau_CoHoi(int ID, string tieude);
        Task SendTeleDuAn_CoHoi(int duToanID, string tieude);
        Task SendTeleDuAn_TiepXuc(int duToanID, string tieude);      
        Task SendTele_GiaoViec(int congViecID, string tieude, int lsID);
        Task SendTelegramNghiPhep(NghiPhep mess,string tieude);
        Task SendTeleKHCT_Thang(int ID, string tieude);
        Task SendTeleKHCT_Tuan(int ID, string tieude);
        Task SendTeleKHCT_Ngay(int ID, string tieude);
    }
}

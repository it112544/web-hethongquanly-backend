﻿using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ILoaiDaiLyRepository : IGenericRepository<LoaiDaiLy>
    {
    }
}

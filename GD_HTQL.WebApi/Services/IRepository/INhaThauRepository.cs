﻿using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;


namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface INhaThauRepository : IGenericRepository<NhaThau>
    {      
        Task<IEnumerable<NhaThau>> GetNhaThauByTuKhoa(string? Key,int tinhID);
        Task<IEnumerable<NhaThau>> GetbyNhanVien(int nhanvienID);
        Task<IEnumerable<NhaThauViewModel>> GetDanhSach(int nhaVienID);
        Task<IEnumerable<NT_The>> GetDsThe();
        Task<IEnumerable<NguonVon>> GetDsNguonVon();
        Task<IEnumerable<NT_LoaiHopTac>> GetDsLoaiHopTac();
        Task<IEnumerable<NT_DiaBanHoatDong>> GetDsDiaBanHoatDong();
        bool CheckMaSoThue(string? _maSoThue, int _nhaThauID);

    }
}

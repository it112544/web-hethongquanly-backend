﻿using GD_HTQL.WebApi.Data.Models.EF.TacGiaModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface ITacGiaRepository :IGenericRepository<TacGia> 
    {       
        Task<IEnumerable<TacGia>> GetTacGiaByTuKhoa(string? Key);        
    }
}

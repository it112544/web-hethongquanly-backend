﻿using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;

namespace GD_HTQL.WebApi.Services.IRepository
{
    public interface IQuanLyNhanVienRepository :IGenericRepository<QuanLyNhanVien>
    {
        Task<IEnumerable<QLNhanVienViewModel>> GetQLNhanVien(List<int> nhanVienID);
        void AddMulti(QuanLyNhanVienCustom quanLyNhanVien);
    }
}

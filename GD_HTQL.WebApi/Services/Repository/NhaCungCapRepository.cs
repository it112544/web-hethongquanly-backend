﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NhaCungCapRepository : GenericRepository<NhaCungCap>,INhaCungCapRepository
    {           
        public NhaCungCapRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper)
        {           
        }            
        public async Task<IEnumerable<NhaCungCap>> GetNhaCungCapByTuKhoa(string? Key,int tinhID)
        {
            IEnumerable<NhaCungCap> objNhaCungCap = await _context.NhaCungCap.Where(x=> (tinhID ==0 || x.TinhID == tinhID) && x.Active == true ).OrderByDescending(x => x.NhaCungCapID).ToListAsync();
            if (!string.IsNullOrEmpty(Key))
            {
                objNhaCungCap = objNhaCungCap.AsQueryable().Where(ExpressionNhaCungCapTheoTuKhoa(Key)).OrderByDescending(x => x.NhaCungCapID).ToList();
            }           
            return objNhaCungCap;
        }                
        public Expression<Func<NhaCungCap, bool>> ExpressionNhaCungCapTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenCongTy).ToLower().Contains(_key);
        }
        public async Task<IEnumerable<NhaCungCap>> GetbyNhanVien(int nhaVienID)
        {
            List<NhaCungCap> objNhaCungCap = new List<NhaCungCap>();
            var objTinh_NhanVien = _context.TinhNhanVien.Where(x => x.NhanVienID == nhaVienID).Select(x => x.TinhID).ToList();
            if (objTinh_NhanVien.Count > 0)
            {
                objNhaCungCap = await _context.NhaCungCap.Where(x => objTinh_NhanVien.Contains((int)x.TinhID)).ToListAsync();
            }
            return objNhaCungCap;
        }
        public bool CheckMaSoThue(string? _maSoThue, int _nccID)
        {
            bool result = false;
            var objNhaThau = _context.NhaCungCap.Where(x => x.MaSoThue == _maSoThue && (_nccID == 0 || x.NhaCungCapID != _nccID)).ToList();
            if (objNhaThau.Count() == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}

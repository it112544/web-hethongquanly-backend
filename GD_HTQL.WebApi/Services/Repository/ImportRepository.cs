﻿using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Data.Models.EF.DuAnModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaCungCapModels;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.EF.TacGiaModels;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class ImportRepository : IImportRepository 
    {
        private readonly ApplicationDbContext _context;
        public ImportRepository(ApplicationDbContext context)
        {
            _context = context ??
                throw new ArgumentNullException(nameof(context));         
        }
        public async Task<List<NhaCungCap>> ImportNhaCungCap(List<NhaCungCap> nhacungcap)
        {
            _context.NhaCungCap.AddRange(nhacungcap);
            await _context.SaveChangesAsync();
            return nhacungcap;
        }
        public async Task<List<NhaThau>> ImportNhaThau(List<NhaThau> nhathau)
        {
            _context.NhaThau.AddRange(nhathau);
            await _context.SaveChangesAsync();
            return nhathau;
        }

        public async Task<List<DaiLy>> ImportDaiLy(List<DaiLy> daily)
        {
            _context.DaiLy.AddRange(daily);
            await _context.SaveChangesAsync();
            return daily;
        }
        public async Task<List<TacGia>> ImportTacGia(List<TacGia> tacgia)
        {
            _context.TacGia.AddRange(tacgia);
            await _context.SaveChangesAsync();
            return tacgia;
        }
        public async Task<List<QuanLyTuongTac>> ImportTuongTac(List<QuanLyTuongTac> tuongtac)
        {
            _context.QuanLyTuongTac.AddRange(tuongtac);
            await _context.SaveChangesAsync();
            return tuongtac;
        }
        public async Task<NSCoQuan> ImportCoQuan(NSCoQuan coquan)
        {
            _context.NSCoQuan.Add(coquan);
            await _context.SaveChangesAsync();
            return coquan;
        }
        public async Task<NSCanBo> ImportCanBo(NSCanBo canbo)
        {
            _context.NSCanBo.Add(canbo);
            await _context.SaveChangesAsync();
            return canbo;
        }
        public async Task<int> GetTinhID(string _tenTinh)
        {
            int TinhID = 0;
            if (!string.IsNullOrEmpty(_tenTinh))
            {
                string TenTinh = _tenTinh.ToLower();
                if (TenTinh.Contains("tỉnh"))
                {
                    TenTinh = TenTinh.Replace("tỉnh", string.Empty);
                }
                if (TenTinh.Contains("thành phố"))
                {
                    TenTinh = TenTinh.Replace("thành phố", string.Empty);
                }
                if (TenTinh.Contains("tp"))
                {
                    TenTinh = TenTinh.Replace("tp", string.Empty);
                }
                var objTinh = await _context.DMTinh.ToListAsync();
                if (objTinh.Count() > 0)
                {
                    var obj = objTinh.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenTinh).ToLower().Contains(ClassCommon.RemoveUnicode(TenTinh))).FirstOrDefault();
                    if (obj != null)
                    {
                        TinhID = obj.TinhID;
                    }
                }
            }            
            return TinhID;
        }
        public async Task<int> GetBuocThiTruongID(string _buocThiTruongTen)
        {
            int _BuocThiTruongID = 0;
            if (!string.IsNullOrEmpty(_buocThiTruongTen))
            {
                string buocThiTruongTen = _buocThiTruongTen.ToLower();
               
                var objBuocThiTruong = await _context.BuocThiTruong.ToListAsync();
                if (objBuocThiTruong.Count >0 )
                {
                    var obj = objBuocThiTruong.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.BuocThiTruongTen).ToLower().Contains(ClassCommon.RemoveUnicode(buocThiTruongTen))).FirstOrDefault();
                    if (obj != null)
                    {
                        _BuocThiTruongID = obj.BuocThiTruongID;
                    }
                    
                }
            }
            return _BuocThiTruongID;
        }

        public async Task<int> GetLoaiNhaThauID(string ten)
        {
            int _LoaiNTID = 0;
            if (!string.IsNullOrEmpty(ten))
            {
                string _ten = ClassCommon.RemoveUnicode(ten).ToLower();
                IEnumerable<LoaiNhaThau> objLoai = await _context.LoaiNhaThau.ToListAsync();
                if (objLoai.Count() > 0)
                {
                    var obj = objLoai.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenLoai).ToLower().Contains(_ten)).FirstOrDefault();
                    if (obj != null)
                    {
                        _LoaiNTID = obj.LoaiNTID;
                    }
                }
            }
            return _LoaiNTID;
        }
        public async Task<int> GetLoaiHinhHopTacID(string ten)
        {
            int _loaiHinhHopTac = 0;
            if (!string.IsNullOrEmpty(ten))
            {
                string _ten = ClassCommon.RemoveUnicode(ten).ToLower();
                IEnumerable<NT_LoaiHopTac> objLoai = await _context.NT_LoaiHopTac.ToListAsync();
                if (objLoai.Count() > 0)
                {
                    var obj = objLoai.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenLoaiHopTac).ToLower().Contains(_ten)).FirstOrDefault();
                    if (obj != null)
                    {
                        _loaiHinhHopTac = obj.ID;
                    }
                }
            }
            return _loaiHinhHopTac;
        }

        public async Task<int> GetDiaBanHoatDongID(string ten)
        {
            int _diaBanHoatDongID = 0;
            if (!string.IsNullOrEmpty(ten))
            {
                string _ten = ClassCommon.RemoveUnicode(ten).ToLower();
                IEnumerable<NT_DiaBanHoatDong> objDiaBan = await _context.NT_DiaBanHoatDong.ToListAsync();
                if (objDiaBan.Count() > 0)
                {
                    var obj = objDiaBan.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.DiaBanHoatDong).ToLower().Contains(_ten)).FirstOrDefault();
                    if (obj != null)
                    {
                        _diaBanHoatDongID = obj.ID;
                    }
                }
            }
            return _diaBanHoatDongID;
        }

        public async Task<int> GetLoaiDaiLyID(string ten)
        {
            int _LoaiDLID = 0;
            if (!string.IsNullOrEmpty(ten))
            {
                string _ten = ClassCommon.RemoveUnicode(ten).ToLower();
                IEnumerable<LoaiDaiLy> objLoai = await _context.LoaiDaiLy.ToListAsync();
                if (objLoai.Count() > 0)
                {
                    var obj = objLoai.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenLoai).ToLower().Contains(_ten)).FirstOrDefault();
                    if (obj != null)
                    {
                        _LoaiDLID = obj.LoaiDLID;
                    }
                }
            }
            return _LoaiDLID;
        }

        public async Task<int> GetLoaiNhaCungCapID(string ten)
        {
            int _LoaiNCCID = 0;
            if (!string.IsNullOrEmpty(ten))
            {
                string _ten = ClassCommon.RemoveUnicode(ten).ToLower();
                IEnumerable<LoaiNhaCungCap> objLoai = await  _context.LoaiNhaCungCap.ToListAsync();
                if (objLoai.Count() > 0)
                {
                    var obj = objLoai.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenLoai).ToLower().Contains(_ten)).FirstOrDefault();
                    if (obj != null)
                    {
                        _LoaiNCCID = obj.LoaiNCCID;
                    }
                }
               
            }
            return _LoaiNCCID;
        }

        public async Task<int> GetHuyenID(string _tenHuyen,int _tinhID)
        {
            int huyenID = 0;
            if (!string.IsNullOrEmpty(_tenHuyen))
            {
                string TenHuyen = _tenHuyen.ToLower();
                if (TenHuyen.Contains("q"))
                {
                    TenHuyen = TenHuyen.Replace("q", "quận");
                }
                if (TenHuyen.Contains("h"))
                {
                    TenHuyen = TenHuyen.Replace("h", "huyện");
                }
                if (TenHuyen.Contains("tp"))
                {
                    TenHuyen = TenHuyen.Replace("tp","thành phố");
                }
                if (TenHuyen.Contains("tx"))
                {
                    TenHuyen = TenHuyen.Replace("tx","thị xã");
                }
                var objHuyen = await _context.DMHuyen.Where(x => x.TinhID == _tinhID).ToListAsync();
                if (objHuyen.Count() > 0)
                {
                    var obj = objHuyen.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenHuyen).ToLower().Contains(ClassCommon.RemoveUnicode(TenHuyen))).FirstOrDefault();
                    if (obj != null)
                    {
                        huyenID = obj.HuyenID;
                    }                  
                }
            }           
            return huyenID;
        }

        public async Task<int> GetXaID(string tenxa, int huyenID)
        {
            int _xaID = 0;
            if (!string.IsNullOrEmpty(tenxa))
            {
                string _tenxa = tenxa.ToLower();
                if (_tenxa.Contains("phường"))
                {
                    _tenxa = _tenxa.Replace("phường", string.Empty);
                }
                if (_tenxa.Contains("xã"))
                {
                    _tenxa = _tenxa.Replace("xã", string.Empty);
                }
                if (_tenxa.Contains("thị trấn"))
                {
                    _tenxa = _tenxa.Replace("thị trấn", string.Empty);
                }
                var objXa = await _context.DMXa.Where(x => x.HuyenID == huyenID).ToListAsync();
                if (objXa.Count() > 0)
                {
                    var obj = objXa.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenXa).ToLower().Contains(ClassCommon.RemoveUnicode(_tenxa))).FirstOrDefault();
                    if (obj != null)
                    {
                        _xaID = obj.XaID;
                    }                   
                }
            }
            return _xaID;
        }

        public async Task<bool> CheckMaSanPham(string _MaSanPham)
        {
            bool result = false;
            var objSanPham = await _context.SanPham.Where(x => x.MaSanPham == _MaSanPham).ToListAsync();
            if (objSanPham.Count == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public async Task<List<SanPham>> ImportSanPham(List<SanPham> sanPham)
        {
            _context.SanPham.AddRange(sanPham);
            await _context.SaveChangesAsync();
            return sanPham;
        }

        public async Task<int> GetNhanVienIDFromUserName(string _userName)
        {
            int nhanvienID = 0;
            if (!string.IsNullOrEmpty(_userName))
            {
                var objNhanVien = _context.NhanVien.Where(x => x.User.UserName == _userName).FirstOrDefault();
                if (objNhanVien != null)
                {
                    nhanvienID = objNhanVien.NhanVienID;
                }
            }
           
            return nhanvienID;
        }
        public async Task<int> GetCoQuanIDByName(string _tenCoQuan)
        {
            int coQuanID = 0;
            if (!string.IsNullOrEmpty(_tenCoQuan))
            {
                var objCoQuan = await _context.NSCoQuan.ToListAsync();
                if (objCoQuan.Count() > 0)
                {
                    var obj = objCoQuan.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenCoQuan).ToLower().Contains(ClassCommon.RemoveUnicode(_tenCoQuan))).FirstOrDefault();
                    if (obj != null)
                    {
                        coQuanID = obj.CoQuanID;
                    }                   
                }
            }       
            return coQuanID;
        }

        public async Task<int> GetMonHocIDByName(string TenMonHoc)
        {
            int _monHocID = 0;
            if (!string.IsNullOrEmpty(TenMonHoc))
            {
                string _tenMonHoc = TenMonHoc.ToLower();
                if (_tenMonHoc.Contains("môn"))
                {
                    _tenMonHoc = _tenMonHoc.Replace("môn", string.Empty);
                }

                var objMonHoc = await _context.MonHoc.ToListAsync();
                if (objMonHoc.Count > 0)
                {
                    var obj = objMonHoc.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenMonHoc).ToLower().Contains(ClassCommon.RemoveUnicode(_tenMonHoc))).FirstOrDefault();
                    if (obj != null)
                    {
                        _monHocID = obj.MonHocID;
                    }                  
                }
            }          
            return _monHocID;
        }
        public async Task<int> GetKhoiLopIDByName(string TenKhoiLop)
        {
            int _khoiLopID = 0;
            if (!string.IsNullOrEmpty(TenKhoiLop))
            {
                string _tenKhoiLop = TenKhoiLop.ToLower();              
                var objKhoiLop = await _context.KhoiLop.ToListAsync();
                if (objKhoiLop.Count() >0)
                {
                    var obj = objKhoiLop.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenKhoiLop).ToLower().Contains(ClassCommon.RemoveUnicode(_tenKhoiLop))).FirstOrDefault();
                    if (obj != null)
                    {
                        _khoiLopID = obj.KhoiLopID;
                    }                 
                }
            }           
            return _khoiLopID;
        }
        public async Task<int> GetLoaiSanPhamIDByName(string TenLoaiSanPham)
        {
            int _loaiSanPhamID = 0;
            if (!string.IsNullOrEmpty(TenLoaiSanPham))
            {
                string _tenLoaiSanPham = TenLoaiSanPham.ToLower();
                var objLoaiSanPham = await _context.LoaiSanPham.ToListAsync();
                if (objLoaiSanPham.Count() > 0)
                {
                    var obj = objLoaiSanPham.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenLoaiSanPham).ToLower().Contains(ClassCommon.RemoveUnicode(_tenLoaiSanPham))).FirstOrDefault();
                    if (obj != null)
                    {
                        _loaiSanPhamID = obj.LoaiSanPhamID;
                    }
                  
                }
            }
          
            return _loaiSanPhamID;
        }

        public async Task<int> GetDonViTinh(string TenDonViTinh)
        {
            int _DVTID = 0;
            if (!string.IsNullOrEmpty(TenDonViTinh))
            {
                string _tenDVT = TenDonViTinh.ToLower();
                var objDVT = await _context.DonViTinh.ToListAsync();
                if (objDVT.Count() > 0)
                {
                    var obj = objDVT.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenDVT).ToLower().Contains(ClassCommon.RemoveUnicode(_tenDVT))).FirstOrDefault();
                    if (obj != null)
                    {
                        _DVTID = obj.DVTID;
                    }
                }
            }

            return _DVTID;
        }


        public bool CheckDecimal(string _decimal)
        {        
            try
            {
               decimal Check = decimal.Parse(_decimal);
                if (Check >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }              
            }
            catch
            {
                return false;
            }            
        }

        public bool CheckDateTime(string _datetime)
        {
            try
            {
                DateTime Check = DateTime.ParseExact(_datetime, Common.ClassParameter.formatDate, CultureInfo.InvariantCulture); 
                if (Check.Year > 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public SanPham GetLikeTenSanPham(string tenSanPham)
        {
            if (!string.IsNullOrEmpty(tenSanPham))
            {
                // Perform the fuzzy search

                var objSanPham = _context.SanPham.Include(x => x.ThongTu).Include(x => x.MonHoc).Include(x => x.KhoiLop).Include(x => x.LoaiSanPham).Include(x => x.DonViTinh).ToList();
                var obj = objSanPham.AsQueryable().Where(ExpressionSanPham(tenSanPham)).Include(x => x.ThongTu).Include(x => x.MonHoc).Include(x => x.KhoiLop).Include(x=>x.LoaiSanPham).Include(x=>x.DonViTinh).Take(1).FirstOrDefault();
                //var obj = await _context.SanPham.Where(x => (_context.FuzzySearch(x.TenSanPham) == _context.FuzzySearch(tenSanPham))).Include(x=>x.ThongTu).Include(x=>x.MonHoc).Include(x=>x.KhoiLop).Take(1).FirstOrDefaultAsync();
                return obj;
            }
            else
            {
                return null;
            }        
        }
        public Expression<Func<SanPham, bool>> ExpressionSanPham(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenSanPham).ToLower().Contains(_key);
        }

        public async Task<bool> CheckMST_NhaThau(string _MST)
        {
            bool result = false;
            var objNhaThau = await _context.NhaThau.Where(x => x.MaSoThue == _MST).ToListAsync();
            if (objNhaThau.Count == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public async Task<bool> CheckMST_NhaCungCap(string _MST)
        {
            bool result = false;
            var objNhaCungCap = await _context.NhaCungCap.Where(x => x.MaSoThue == _MST).ToListAsync();
            if (objNhaCungCap.Count == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public async Task<bool> CheckMST_DaiLy(string _MST)
        {
            bool result = false;
            var objDaiLy = await _context.DaiLy.Where(x => x.MaSoThue == _MST).ToListAsync();
            if (objDaiLy.Count == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        
        public async Task<bool> CheckMST_CoQuan(string _MST)
        {
            bool result = false;
            var objCoQuan = await _context.NSCoQuan.Where(x => x.MaSoThue == _MST).ToListAsync();
            if (objCoQuan.Count == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public async Task InsertSanPham_Monhoc(string pMonHoc, int sanphamID)
        {
            var LstMonHoc = await _context.MonHoc.ToListAsync();
            string[] MonHocs = pMonHoc.Split(", ");
            foreach(var it in MonHocs)
            {
                var objMonHoc =  LstMonHoc.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenMonHoc).ToLower().Contains(ClassCommon.RemoveUnicode(it.ToLower()))).FirstOrDefault();
                if (objMonHoc != null)
                {
                    SanPhamMonHoc spmh = new SanPhamMonHoc();
                    spmh.SanPhamID = sanphamID;
                    spmh.MonHocID = objMonHoc.MonHocID;
                    _context.SanPhamMonHoc.Add(spmh);
                    await _context.SaveChangesAsync();
                }
            }          
        }
        public async Task InsertSanPham_ThongTu(string pThongTu, int sanphamID)
        {
            var lstThongTu = await _context.ThongTu.ToListAsync();
            string[] ThongTus = pThongTu.Split(", ");
            foreach (var it in ThongTus)
            {
                var objThongTu = lstThongTu.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenThongTu).ToLower().Contains(ClassCommon.RemoveUnicode(it.ToLower()))).FirstOrDefault();
                if (objThongTu != null)
                {
                    SanPhamThongTu sptt = new SanPhamThongTu();
                    sptt.SanPhamID = sanphamID;
                    sptt.ThongTuID = objThongTu.ThongTuID;
                    _context.SanPhamThongTu.Add(sptt);
                    await _context.SaveChangesAsync();
                }
            }
        }
        public async Task InsertSanPham_Lop(string pLop, int sanphamID)
        {
            var lstLop = await _context.KhoiLop.ToListAsync();
            string[] lops = pLop.Split(", ");
            foreach (var it in lops)
            {
                var objLop = lstLop.AsQueryable().Where(x => ClassCommon.RemoveUnicode(x.TenKhoiLop).ToLower().Contains(ClassCommon.RemoveUnicode(it.ToLower()))).FirstOrDefault();
                if (objLop != null)
                {
                    SanPhamKhoiLop spl = new SanPhamKhoiLop();
                    spl.SanPhamID = sanphamID;
                    spl.KhoiLopID = objLop.KhoiLopID;
                    _context.SanPhamKhoiLop.Add(spl);
                    await _context.SaveChangesAsync();
                }
            }
        }
    }
}

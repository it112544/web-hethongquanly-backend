﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NghiPhepRepository : GenericRepository<NghiPhep>, INghiPhepRepository
    {
        public NghiPhepRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task<ICollection<NghiPhepViewModel>> GetDanhSach(int nhanVienID)
        {
            var lstNhanVien = _context.NhanVien.ToList();
            List<NghiPhepViewModel> lstView = new List<NghiPhepViewModel>();       
                var lstNgay = await _context.NghiPhep.Where(x => (nhanVienID == 0 || nhanVienID == x.NguoiTaoID)).Include(x => x.NghiPhepLoai).Include(x => x.NghiPhep_LichSu).ToListAsync();
                foreach (var item in lstNgay)
                {
                var objNhanVien = _context.NhanVien.Where(x => x.NhanVienID == item.NguoiTaoID).FirstOrDefault();
                if (objNhanVien != null)
                {
                    float _phepNam = 0;
                    if (objNhanVien.NgayKyHopDong != null && objNhanVien.NgayKyHopDong.Value.Year != 1)
                    {
                        if (objNhanVien.NgayKyHopDong.Value.Day < 15)
                        {
                            _phepNam = 13 - objNhanVien.NgayKyHopDong.Value.Month;
                        }
                        else
                        {
                            _phepNam = 12 - objNhanVien.NgayKyHopDong.Value.Month;
                        }
                    }
                    // Phép đã nghỉ trong năm
                    float _phepDaSuDung = 0;
                    var objPhep = _context.NghiPhep.Where(x => x.NguoiTaoID == nhanVienID && x.LoaiID == 1 && x.DenNgay.Year == DateTime.Now.Year).ToList();
                    foreach (var it in objPhep)
                    {                 
                        var objLS = _context.NghiPhep_LichSu.Where(x => x.NghiPhepID == it.NghiPhepID).OrderByDescending(x => x.ThoiGan).Take(1).FirstOrDefault();
                        // Phép đã đc duyệt
                        if (objLS != null && objLS.TrangThaiID == 4)
                        {
                            _phepDaSuDung += it.NgayNghi;
                        }
                    }
                    NghiPhepViewModel objView = new NghiPhepViewModel();
                    objView = _mapper.Map<NghiPhepViewModel>(item);
                    var obj = await _context.NghiPhep_LichSu.Where(x => x.NghiPhepID == item.NghiPhepID).Include(x => x.NghiPhep_TrangThai).Include(x => x.NhanVien).OrderByDescending(x => x.ThoiGan).ToListAsync();
                    if (obj.Count() > 0)
                    {
                        objView.NghiPhep_LichSu = _mapper.Map<List<NghiPhep_LichSuView>>(obj);
                    }
                    // Tính ngày nghỉ
                    objView.PhepNam = _phepNam;
                    objView.PhepDaSuDung = _phepDaSuDung;
                    objView.PhepDuocSuDung = (_phepNam + DateTime.Now.Month) - 12;
                    objView.PhepCoTheSuDung = objView.PhepDuocSuDung - _phepDaSuDung;
                    lstView.Add(objView);
                }
            }                                    
            return lstView;
        }
        public async Task<NghiPhepViewModel> GetDetailsByID(int nghiPhepID)
        {
            var lstNgay = await _context.NghiPhep.Where(x => x.NghiPhepID == nghiPhepID).Include(x=>x.NghiPhepLoai).Include(x => x.NghiPhep_LichSu).FirstOrDefaultAsync();
            NghiPhepViewModel objView = new NghiPhepViewModel();          
            if (lstNgay != null)
            {
                var objNhanVien = _context.NhanVien.Where(x => x.NhanVienID == lstNgay.NguoiTaoID).FirstOrDefault();
                if (objNhanVien !=null)
                {
                    float _phepNam = 0;
                    if (objNhanVien.NgayKyHopDong != null && objNhanVien.NgayKyHopDong.Value.Year != 1)
                    {
                        if (objNhanVien.NgayKyHopDong.Value.Day < 15)
                        {
                            _phepNam = 13 - objNhanVien.NgayKyHopDong.Value.Month;
                        }
                        else
                        {
                            _phepNam = 12 - objNhanVien.NgayKyHopDong.Value.Month;
                        }
                    }
                    // Phép đã nghỉ trong năm
                    float _phepDaSuDung = 0;
                    var objPhep = _context.NghiPhep.Where(x => x.NguoiTaoID == lstNgay.NguoiTaoID && x.LoaiID == 1 && x.DenNgay.Year == DateTime.Now.Year).ToList();
                    foreach (var it in objPhep)
                    {
                        var objLS = _context.NghiPhep_LichSu.Where(x => x.NghiPhepID == it.NghiPhepID).OrderByDescending(x => x.ThoiGan).Take(1).FirstOrDefault();
                        // Phép đã đc duyệt
                        if (objLS != null && objLS.TrangThaiID == 4)
                        {
                            _phepDaSuDung += it.NgayNghi;
                        }
                    }
                    objView = _mapper.Map<NghiPhepViewModel>(lstNgay);
                    var obj = await _context.NghiPhep_LichSu.Where(x => x.NghiPhepID == lstNgay.NghiPhepID).Include(x => x.NghiPhep_TrangThai).Include(x => x.NhanVien).OrderByDescending(x => x.ThoiGan).ToListAsync();
                    if (obj.Count() > 0)
                    {
                        objView.NghiPhep_LichSu = _mapper.Map<List<NghiPhep_LichSuView>>(obj);
                    }
                    // Tính ngày nghỉ
                    objView.PhepNam = _phepNam;
                    objView.PhepDaSuDung = _phepDaSuDung;
                    objView.PhepDuocSuDung = (_phepNam + DateTime.Now.Month) - 12;
                    objView.PhepCoTheSuDung = objView.PhepDuocSuDung - _phepDaSuDung;
                }            
            }
            return objView;
        }
        public async Task<List<NghiPhep_TrangThai>> GetTrangThai()
        {
            var obj = await _context.NghiPhep_TrangThai.ToListAsync();
            return obj;
        }
        public async Task<List<NghiPhepLoai>> GetLoaiNghiPhep()
        {
            var obj = await _context.NghiPhepLoai.ToListAsync();
            return obj;
        }

        public async Task<NghiPhepNhanVienView> GetByNhanVien(int nhanvienID)
        {
            NghiPhepNhanVienView nghiPhepNhanVien = new NghiPhepNhanVienView();
            var objNhanVien = _context.NhanVien.Where(x => x.NhanVienID == nhanvienID).FirstOrDefault();
            if (objNhanVien != null)
            {
                float _phepNam = 0;
                if (objNhanVien.NgayKyHopDong != null && objNhanVien.NgayKyHopDong.Value.Year != 1)
                {
                    if (objNhanVien.NgayKyHopDong.Value.Year < DateTime.Now.Year)
                    {
                        _phepNam = 12;
                    }
                    else
                    {
                        if (objNhanVien.NgayKyHopDong.Value.Day <15)
                        {
                            _phepNam = 13 - objNhanVien.NgayKyHopDong.Value.Month;
                        }
                        else
                        {
                            _phepNam = 12 - objNhanVien.NgayKyHopDong.Value.Month;
                        }                    
                    }
                }
                // Phép đã nghỉ trong năm
                float _phepDaSuDung = 0;
                var objPhep = await _context.NghiPhep.Where(x => x.NguoiTaoID == nhanvienID && x.LoaiID == 1 && x.DenNgay.Year == DateTime.Now.Year).ToListAsync();
                foreach (var it in objPhep)
                {
                    var objLS = _context.NghiPhep_LichSu.Where(x => x.NghiPhepID == it.NghiPhepID).OrderByDescending(x => x.ThoiGan).Take(1).FirstOrDefault();
                    // Phép đã đc duyệt
                    if (objLS != null && objLS.TrangThaiID == 4)
                    {
                        _phepDaSuDung += it.NgayNghi;
                    }
                }               
                // Tính ngày nghỉ
                nghiPhepNhanVien.PhepNam = _phepNam;
                nghiPhepNhanVien.PhepDaSuDung = _phepDaSuDung;
                nghiPhepNhanVien.PhepDuocSuDung = (_phepNam + DateTime.Now.Month) - 12;
                nghiPhepNhanVien.PhepCoTheSuDung = nghiPhepNhanVien.PhepDuocSuDung - _phepDaSuDung;
            }
            return nghiPhepNhanVien;
        }
    }
}

﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class PhongBanRepository : GenericRepository<PhongBan> ,IPhongBanRepository
    {

        public PhongBanRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper) 
        {        
        }       
        public async Task<IEnumerable<PhongBan>> GetPhongBanByTuKhoa(string Key, int _CongTyID)
        {
            var objPhongBan = await _context.PhongBan.Where(x => x.TenPhongBan.Contains(Key) && (_CongTyID == 0 || x.CongTyID == _CongTyID)).ToListAsync();
            return objPhongBan;
        }
        public bool DoiTrangThai(int ID)
        {
            bool result = false;
            var objPhongBan = _context.PhongBan.Find(ID);
            if (objPhongBan != null)
            {
                if (objPhongBan.Active == true)
                {
                    objPhongBan.Active = false;
                }
                else
                {
                    objPhongBan.Active = true;
                }
                _context.Entry(objPhongBan).State = EntityState.Modified;
                _context.SaveChanges();
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}

﻿using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.AspNetCore.Identity;
using System;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class DbInitializer : IDbInitializer
    {
        private RoleManager<IdentityRole> _roleManager;
        private UserManager<IdentityUser> _userManager;
        public DbInitializer(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
        {

            _roleManager = roleManager;
            _userManager = userManager;
        }
        public async void Initialize()
        {

            if (!_roleManager.RoleExistsAsync("Admin").GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole("Admin")).GetAwaiter().GetResult();
            }

            if (!_roleManager.RoleExistsAsync("PTGD").GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole("PTGD")).GetAwaiter().GetResult();
            }

            if (!_roleManager.RoleExistsAsync("GD").GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole("GD")).GetAwaiter().GetResult();
            }

            if (!_roleManager.RoleExistsAsync("PGD").GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole("PGD")).GetAwaiter().GetResult();
            }

            if (!_roleManager.RoleExistsAsync("PhongBan").GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole("PhongBan")).GetAwaiter().GetResult();
            }

            if (!_roleManager.RoleExistsAsync("QuanLy").GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole("QuanLy")).GetAwaiter().GetResult();
            }

            if (!_roleManager.RoleExistsAsync("NhanVien").GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole("NhanVien")).GetAwaiter().GetResult();
            }

            var userManager = _userManager.FindByNameAsync("Host").GetAwaiter().GetResult();
            if (userManager == null)
            {
                var user = new IdentityUser { UserName = "Host", Email = "nhkhanhkc@gmail.com" };
                _userManager.CreateAsync(user, "admin!@#2024").GetAwaiter().GetResult();
                _userManager.AddToRoleAsync(user, "Admin").GetAwaiter().GetResult();

            }
        }
    }
}

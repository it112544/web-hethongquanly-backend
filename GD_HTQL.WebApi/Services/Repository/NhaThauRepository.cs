﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NhaThauModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NhaThauRepository :GenericRepository<NhaThau> , INhaThauRepository
    {    
        public NhaThauRepository(ApplicationDbContext context, IMapper mapper) :base(context, mapper) 
        {         
        }   
        public async Task<IEnumerable<NhaThau>> GetNhaThauByTuKhoa(string? Key, int tinhID)
        {
            IEnumerable<NhaThau> objNhaThau = await _context.NhaThau.Where(x=> (tinhID ==0 || x.TinhID ==tinhID) && x.Active == true).OrderByDescending(x => x.NhaThauID).ToListAsync();
            if (!string.IsNullOrEmpty(Key))
            {
                objNhaThau = objNhaThau.AsQueryable().Where(ExpressionNhaThauTheoTuKhoa(Key)).OrderByDescending(x => x.NhaThauID).ToList();
            }           
            return objNhaThau;
        }             
        public Expression<Func<NhaThau, bool>> ExpressionNhaThauTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenCongTy).ToLower().Contains(_key);
        }
        public async Task<IEnumerable<NhaThau>> GetbyNhanVien(int nhaVienID)
        {
            List<NhaThau> objNhaThau = new List<NhaThau>();
            var objTinh_NhanVien = _context.TinhNhanVien.Where(x => x.NhanVienID == nhaVienID).Select(x => x.TinhID).ToList();
            if (objTinh_NhanVien.Count > 0)
            {
                objNhaThau = await _context.NhaThau.Where(x => objTinh_NhanVien.Contains((int)x.TinhID)).ToListAsync();
            }
            return objNhaThau;
        }
        public async Task<IEnumerable<NhaThauViewModel>> GetDanhSach(int nhaVienID)
        {
            List<NhaThauViewModel> objNhaThauView = new List<NhaThauViewModel>();
            List<NhaThau> objNhaThau = new List<NhaThau>();
            if (nhaVienID == 0)
            {
                objNhaThau = await _context.NhaThau.Include(x=>x.NhanVien).Include(x => x.Tinh).Include(x=>x.LoaiNhaThau).Include(x => x.NT_LoaiHopTac).Include(x => x.NT_LoaiHopTac).Include(x => x.NT_DiaBanHoatDong).Include(x=>x.NT_DuToan).Include(x => x.NT_GiaTriCoHoi).ToListAsync();
            }
            else
            {
                var objTinh_NhanVien = _context.TinhNhanVien.Where(x => x.NhanVienID == nhaVienID).Select(x => x.TinhID).ToList();
                if (objTinh_NhanVien.Count > 0)
                {
                    objNhaThau = await _context.NhaThau.Where(x => objTinh_NhanVien.Contains((int)x.TinhID)).Include(x => x.NhanVien).Include(x => x.Tinh).Include(x => x.LoaiNhaThau).Include(x => x.NT_LoaiHopTac).Include(x => x.NT_LoaiHopTac).Include(x => x.NT_DiaBanHoatDong).Include(x=>x.NT_DuToan).Include(x=>x.NT_GiaTriCoHoi).ToListAsync();
                }             
            }
            if (objNhaThau.Count() > 0)
            {               
                foreach (var item in objNhaThau)
                {
                    if (item.NT_DuToan != null && item.NT_DuToan.Count() > 0)
                    {
                        item.NT_DuToan = item.NT_DuToan.OrderByDescending(x => x.DuToanID).Take(1).ToList();
                    }
                    if(item.NT_GiaTriCoHoi !=null && item.NT_GiaTriCoHoi.Count() > 0)
                    {
                        var objCH = item.NT_GiaTriCoHoi.OrderByDescending(x => x.GTCHID).Select(x=>x.GTCHID).Take(1).FirstOrDefault();
                        var obj =await _context.NT_GiaTriCoHoi.Where(x =>   x.GTCHID == objCH).Include(x => x.LoaiSanPham).Include(x => x.NhaThau).Include(x => x.NhanVien).Include(x => x.NguonVon).Include(x => x.NT_The).ToListAsync();
                        item.NT_GiaTriCoHoi = obj;
                    }
                }
                objNhaThauView = _mapper.Map<List<NhaThauViewModel>>(objNhaThau);
            }        
            return objNhaThauView;
        }
        public async Task<IEnumerable<NT_The>> GetDsThe()
        {
            var objThe = await _context.NT_The.ToListAsync();

            return objThe;
        }
        public async Task<IEnumerable<NguonVon>> GetDsNguonVon()
        {
            var objNguonVon = await _context.NguonVon.ToListAsync();

            return objNguonVon;
        }
        public async Task<IEnumerable<NT_LoaiHopTac>> GetDsLoaiHopTac()
        {
            var objLoaiHopTac = await _context.NT_LoaiHopTac.ToListAsync();

            return objLoaiHopTac;
        }
        public async Task<IEnumerable<NT_DiaBanHoatDong>> GetDsDiaBanHoatDong()
        {
            var objDiaBan = await _context.NT_DiaBanHoatDong.ToListAsync();

            return objDiaBan;
        }
        public bool CheckMaSoThue(string? _maSoThue, int _nhaThauID)
        {
            bool result = false;
            var objNhaThau = _context.NhaThau.Where(x => x.MaSoThue == _maSoThue && (_nhaThauID == 0 || x.NhaThauID != _nhaThauID)).ToList();
            if (objNhaThau.Count() == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}

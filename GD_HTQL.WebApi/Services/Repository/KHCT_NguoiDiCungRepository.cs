﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.CongTacModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class KHCT_NguoiDiCungRepository : GenericRepository<KHCT_NguoiDiCung>, IKHCT_NguoiDiCungRepository
    {
        public KHCT_NguoiDiCungRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

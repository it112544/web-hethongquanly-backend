﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.CustomModels;
using GD_HTQL.WebApi.Data.Models.EF.NhanVienModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class QuanLyNhanVienRepository : GenericRepository<QuanLyNhanVien>, IQuanLyNhanVienRepository
    {
        public QuanLyNhanVienRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task<IEnumerable<QLNhanVienViewModel>> GetQLNhanVien(List<int> nhanVienID)
        {
            var obj = await _context.NhanVien.Where(x=> nhanVienID.Contains(x.NhanVienID) ).Select(x => new QLNhanVienViewModel
            {
               NhanVienID = x.NhanVienID,
               TenNhanVien = x.TenNhanVien,
               SoDienThoai = x.SoDienThoai,
               Email =x.Email
            }).ToListAsync();
            return obj;
        }
        public void AddMulti(QuanLyNhanVienCustom quanLyNhanVien)
        {
            // Xóa Quản lý nhân viên Củ 
            var objQL = _context.QuanLyNhanVien.Where(x => x.QuanLyID == quanLyNhanVien.QuanLyID).ToList();
            _context.QuanLyNhanVien.RemoveRange(objQL);
            _context.SaveChanges();

           List<QuanLyNhanVien> lstQLNV = new List<QuanLyNhanVien>();
            if(quanLyNhanVien.lstNhanVienID.Count() > 0)
            {
                foreach(var it in quanLyNhanVien.lstNhanVienID)
                {
                    QuanLyNhanVien obj = new QuanLyNhanVien()
                    {
                        ID = 0,
                        NhanVienID = it,
                        QuanLyID = quanLyNhanVien.QuanLyID
                    };
                    lstQLNV.Add(obj);
                } 
                _context.QuanLyNhanVien.AddRange(lstQLNV);
                _context.SaveChanges();
            }                     
        }
    }
}

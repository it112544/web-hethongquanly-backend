﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.NghiPhepModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class NghiPhep_LichSuRepository : GenericRepository<NghiPhep_LichSu> , INghiPhep_LichSuRepository
    {
        public NghiPhep_LichSuRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}

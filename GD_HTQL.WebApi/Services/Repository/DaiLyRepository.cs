﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.DaiLyModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class DaiLyRepository : GenericRepository<DaiLy>, IDaiLyRepository
    {
        public DaiLyRepository(ApplicationDbContext context, IMapper mapper) : base(context,mapper)
        {
        }
        public async Task<IEnumerable<DaiLy>> GetDaiLyByTuKhoa(string? Key, int tinhID)
        {
            IEnumerable<DaiLy> objDaiLy = await _context.DaiLy.Where(x => (tinhID == 0 || x.TinhID == tinhID) && x.Active == true).OrderByDescending(x => x.DaiLyID).ToListAsync();
            if (!string.IsNullOrEmpty(Key))
            {
                objDaiLy = objDaiLy.AsQueryable().Where(ExpressionDaiLyTheoTuKhoa(Key)).OrderByDescending(x => x.DaiLyID).ToList();
            }
            return objDaiLy;
        }    
        public Expression<Func<DaiLy, bool>> ExpressionDaiLyTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.TenDaiLy).ToLower().Contains(_key);
        }
        public async Task<IEnumerable<DaiLy>> GetbyNhanVien(int nhaVienID)
        {
            List<DaiLy> objDaiLy = new List<DaiLy>();
            var objTinh_NhanVien = _context.TinhNhanVien.Where(x => x.NhanVienID == nhaVienID).Select(x => x.TinhID).ToList();
            if (objTinh_NhanVien.Count > 0)
            {
                objDaiLy = await _context.DaiLy.Where(x => objTinh_NhanVien.Contains((int)x.TinhID)).ToListAsync();
            }
            return objDaiLy;
        }
        public bool CheckMaSoThue(string? _maSoThue, int _daiLyID)
        {
            bool result = false;
            var objNhaThau = _context.DaiLy.Where(x => x.MaSoThue == _maSoThue && (_daiLyID == 0 || x.DaiLyID != _daiLyID)).ToList();
            if (objNhaThau.Count() == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}

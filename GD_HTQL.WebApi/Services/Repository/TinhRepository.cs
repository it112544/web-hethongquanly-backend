﻿using AutoMapper;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.EF.DanhMucModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
namespace GD_HTQL.WebApi.Services.Repository
{
    public class TinhRepository : GenericRepository<DMTinh> ,ITinhRepository
    {   
        public TinhRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {                    
        }      
    }
}

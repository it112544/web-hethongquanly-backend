﻿using AutoMapper;
using GD_HTQL.WebApi.Common;
using GD_HTQL.WebApi.Data.Models;
using GD_HTQL.WebApi.Data.Models.Dtos.SanPham;
using GD_HTQL.WebApi.Data.Models.EF.SanPhamModels;
using GD_HTQL.WebApi.Data.Models.ViewModels;
using GD_HTQL.WebApi.Infrastructure;
using GD_HTQL.WebApi.Services.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GD_HTQL.WebApi.Services.Repository
{
    public class SanPhamRepository : GenericRepository<SanPham>,ISanPhamRepository
    {  
        public SanPhamRepository(ApplicationDbContext context , IMapper mapper) :base(context,mapper) 
        {          
        }     
        public async Task<IEnumerable<SanPhamViewModel>> GetByTuKhoa(string? Key)
        {
            List<SanPhamViewModel> sanPhamViews = new List<SanPhamViewModel>();
            IEnumerable<SanPham> objSanPham = await _context.SanPham.Include(x => x.MonHoc).Include(x => x.ThongTu).Include(x => x.KhoiLop).Include(x => x.DonViTinh).Include(x => x.LoaiSanPham).ToListAsync();
            if (!string.IsNullOrEmpty(Key))
            {
                objSanPham = objSanPham.AsQueryable().Where(ExpressionTheoTuKhoa(Key)).Include(x => x.MonHoc).Include(x => x.ThongTu).Include(x => x.KhoiLop).Include(x => x.DonViTinh).Include(x => x.LoaiSanPham).ToList();               
            }
            if (objSanPham.Count() > 0)
            {
                sanPhamViews = objSanPham.AsQueryable().Include(x => x.MonHoc).Include(x => x.ThongTu).Include(x => x.KhoiLop).Include(x => x.DonViTinh).Include(x => x.LoaiSanPham).Select(x => new SanPhamViewModel
                {
                    SanPhamID = x.SanPhamID,
                    MaSanPham = x.MaSanPham,
                    TenSanPham = x.TenSanPham,
                    TieuChuanKyThuat = x.TieuChuanKyThuat,
                    HangSanXuat = x.HangSanXuat,
                    XuatXu = x.XuatXu,
                    BaoHanh = x.BaoHanh,
                    NamSanXuat = x.NamSanXuat,
                    NhaXuatBan = x.NhaXuatBan,
                    DonViTinh = x.DonViTinh == null ? null : _mapper.Map<DonViTinhDto>(x.DonViTinh),
                    GiaVon = x.GiaVon,
                    GiaDaiLy = x.GiaDaiLy,
                    GiaTTR = x.GiaTTR,
                    GiaTH_TT = x.GiaTH_TT,
                    GiaCDT = x.GiaCDT,
                    Thue = x.Thue,
                    SoLuong = x.SoLuong,
                    DoiTuongSuDung = x.DoiTuongSuDung,
                    LoaiSanPham = x.LoaiSanPham == null ? null : _mapper.Map<LoaiSanPhamDto>(x.LoaiSanPham),
                    ThuongHieu = x.ThuongHieu,
                    HinhAnh = x.HinhAnh,
                    Model = x.Model,
                    MonHoc = x.MonHoc == null ? null : _mapper.Map<List<MonHocDto>>(x.MonHoc),
                    KhoiLop = x.KhoiLop == null ? null : _mapper.Map<List<KhoiLopDto>>(x.KhoiLop),
                    ThongTu = x.ThongTu == null ? null : _mapper.Map<List<ThongTuDto>>(x.ThongTu),
                }).ToList();
            }
            return sanPhamViews;
        }

        public async Task<IEnumerable<SanPhamViewModel>> GetDanhSach(string tukhoa, int loaiID, int monhocID, int khoilopID, int pageCurrent , int pageSize)
        {
            List <SanPhamViewModel> sanPhamViewModels = new List<SanPhamViewModel>();
            IEnumerable<SanPham> objSanPham = await _context.SanPham.Include(x=>x.LoaiSanPham).Include(x => x.MonHoc).Include(x => x.KhoiLop).Include(x=>x.ThongTu).Include(x=>x.DonViTinh).OrderByDescending(x => x.SanPhamID).ToListAsync();
            
                objSanPham = objSanPham.AsQueryable().Where(ExpressionDanhSach(tukhoa, loaiID ,monhocID, khoilopID)).OrderByDescending(x => x.SanPhamID).ToList();
                if (objSanPham.Count() > 0)
                {
                    
                    if (objSanPham.Count() >pageSize * pageCurrent)
                    {
                        objSanPham = objSanPham.Skip(pageSize * pageCurrent).Take(pageSize).ToList();
                    }
                    else
                    {
                        objSanPham = objSanPham.Take(pageSize).ToList();
                    }
                    sanPhamViewModels = objSanPham.Select(x => new SanPhamViewModel {
                        SanPhamID = x.SanPhamID,
                        MaSanPham = x.MaSanPham,
                        TenSanPham = x.TenSanPham,
                        TieuChuanKyThuat = x.TieuChuanKyThuat,
                        HangSanXuat = x.HangSanXuat,
                        XuatXu = x.XuatXu,
                        BaoHanh = x.BaoHanh,
                        NamSanXuat = x.NamSanXuat,
                        NhaXuatBan = x.NhaXuatBan,
                        DonViTinh = x.DonViTinh == null ? null : _mapper.Map<DonViTinhDto>(x.DonViTinh),
                        GiaVon = x.GiaVon,
                        GiaDaiLy = x.GiaDaiLy,
                        GiaTTR = x.GiaTTR,
                        GiaTH_TT = x.GiaTH_TT,
                        GiaCDT = x.GiaCDT,
                        Thue = x.Thue,
                        SoLuong = x.SoLuong,
                        DoiTuongSuDung = x.DoiTuongSuDung,
                        LoaiSanPham = x.LoaiSanPham == null ? null : _mapper.Map<LoaiSanPhamDto>(x.LoaiSanPham),
                        ThuongHieu = x.ThuongHieu,
                        HinhAnh = x.HinhAnh,
                        Model = x.Model,
                        MonHoc = x.MonHoc == null ? null : _mapper.Map<List<MonHocDto>>(x.MonHoc),
                        KhoiLop = x.KhoiLop == null ? null : _mapper.Map<List<KhoiLopDto>>(x.KhoiLop),
                        ThongTu = x.ThongTu == null ? null : _mapper.Map<List<ThongTuDto>>(x.ThongTu),
                    }).ToList();
                }
            
            return sanPhamViewModels;
        }
        public bool CheckMaSanPham(string _MaSanPham , int sanphamID)
        {
            bool result = false;
            var objSanPham =  _context.SanPham.Where(x=> x.MaSanPham == _MaSanPham && (sanphamID == 0 || x.SanPhamID != sanphamID)).ToList();
            if (objSanPham.Count() == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public async Task ThemThongTu(List<int> sanPhamThongTu, int sanphamID, string loai)
        {
            List<SanPhamThongTu> sanPhamThongTus = new List<SanPhamThongTu>();
            foreach (var it in sanPhamThongTu)
            {
                SanPhamThongTu thongtu = new SanPhamThongTu();
                thongtu.ThongTuID = it;
                thongtu.SanPhamID = sanphamID;
                sanPhamThongTus.Add(thongtu);
            }
            if (loai =="add")
            {
               
                _context.SanPhamThongTu.AddRange(sanPhamThongTus);
                await _context.SaveChangesAsync();
            }
            else
            {
                // Kiểm tra trong csdl
                var _dataSPTT = _context.SanPhamThongTu.Where(x => x.SanPhamID == sanphamID).ToList();
                if(_dataSPTT.Count > 0)
                {
                    foreach(var item in _dataSPTT)
                    {
                        var objSPTT = sanPhamThongTus.Where(x => x.ThongTuID == item.ThongTuID).FirstOrDefault();
                        if (objSPTT == null)
                        {
                            _context.SanPhamThongTu.Remove(item);
                            _context.SaveChanges();
                        }
                        else
                        {
                            sanPhamThongTus.Remove(objSPTT);
                        }
                    }
                }
                if (sanPhamThongTus.Count > 0)
                {
                    _context.SanPhamThongTu.AddRange(sanPhamThongTus);
                    await _context.SaveChangesAsync();
                }
            }                  
        }
        public async Task ThemMonHoc(List<int> sanPhamMonHoc, int sanphamID , string loai)
        {
            List<SanPhamMonHoc> sanPhamMonHocs = new List<SanPhamMonHoc>();
            foreach (var it in sanPhamMonHoc)
            {
                SanPhamMonHoc mon = new SanPhamMonHoc();
                mon.MonHocID = it;
                mon.SanPhamID = sanphamID;
                sanPhamMonHocs.Add(mon);
            }
            if (loai =="add")
            {            
                _context.SanPhamMonHoc.AddRange(sanPhamMonHocs);
                await _context.SaveChangesAsync();
            }
            else
            {
                // Kiểm tra trong csdl
                var _dataSPMH = _context.SanPhamMonHoc.Where(x => x.SanPhamID == sanphamID).ToList();
                if (_dataSPMH.Count > 0)
                {
                    foreach (var item in _dataSPMH)
                    {
                        var objSPTT = sanPhamMonHocs.Where(x => x.MonHocID == item.MonHocID).FirstOrDefault();
                        if (objSPTT == null)
                        {
                            _context.SanPhamMonHoc.Remove(item);
                            _context.SaveChanges();
                        }
                        else
                        {
                            sanPhamMonHocs.Remove(objSPTT);
                        }
                    }
                }
                if (sanPhamMonHocs.Count > 0)
                {
                    _context.SanPhamMonHoc.AddRange(sanPhamMonHocs);
                    await _context.SaveChangesAsync();
                }
            }          
        }

        public async Task ThemKhoiLop(List<int> sanPhamKhoiLop, int sanphamID, string loai)
        {
            List<SanPhamKhoiLop> sanPhamKhoiLops = new List<SanPhamKhoiLop>();
            foreach (var it in sanPhamKhoiLop)
            {
                SanPhamKhoiLop khoi = new SanPhamKhoiLop();
                khoi.KhoiLopID = it;
                khoi.SanPhamID = sanphamID;
                sanPhamKhoiLops.Add(khoi);
            }
            if (loai == "add")
            {             
                _context.SanPhamKhoiLop.AddRange(sanPhamKhoiLops);
                await _context.SaveChangesAsync();
            }
            else
            {
                // Kiểm tra trong csdl
                var _dataSPKL = _context.SanPhamKhoiLop.Where(x => x.SanPhamID == sanphamID).ToList();
                if (_dataSPKL.Count > 0)
                {
                    foreach (var item in _dataSPKL)
                    {
                        var objSPTT = sanPhamKhoiLops.Where(x => x.KhoiLopID == item.KhoiLopID).FirstOrDefault();
                        if (objSPTT == null)
                        {
                            _context.SanPhamKhoiLop.Remove(item);
                            _context.SaveChanges();
                        }
                        else
                        {
                            sanPhamKhoiLops.Remove(objSPTT);
                        }
                    }
                }
                if (sanPhamKhoiLops.Count > 0)
                {
                    _context.SanPhamKhoiLop.AddRange(sanPhamKhoiLops);
                    await _context.SaveChangesAsync();
                }
            }
        }
        public void XoaSanPhamThongTu(int sanphamID)
        {
            var objSanPhamThongTu = _context.SanPhamThongTu.Where(x => x.SanPhamID == sanphamID).ToList();
            if (objSanPhamThongTu != null)
            {
                _context.SanPhamThongTu.RemoveRange(objSanPhamThongTu);
                _context.SaveChanges();
            }
        }
        public void XoaSanPhamMonHoc(int sanphamID)
        {
            var objSanPhamMonHoc = _context.SanPhamMonHoc.Where(x => x.SanPhamID == sanphamID).ToList();
            if (objSanPhamMonHoc != null)
            {
                _context.SanPhamMonHoc.RemoveRange(objSanPhamMonHoc);
                _context.SaveChanges();
            }
        }
        public void XoaSanPhamKhoiLop(int sanphamID)
        {
            var objSanPhamKhoiLop = _context.SanPhamKhoiLop.Where(x => x.SanPhamID == sanphamID).ToList();
            if (objSanPhamKhoiLop != null)
            {
                _context.SanPhamKhoiLop.RemoveRange(objSanPhamKhoiLop);
                _context.SaveChanges();
            }
        }

        public Expression<Func<SanPham, bool>> ExpressionTheoTuKhoa(string _key)
        {
            _key = ClassCommon.RemoveUnicode(_key).ToLower();
            return x => ClassCommon.RemoveUnicode(x.MaSanPham).ToLower().Contains(_key) || ClassCommon.RemoveUnicode(x.TenSanPham).ToLower().Contains(_key);
        }

        public Expression<Func<SanPham, bool>> ExpressionDanhSach(string? tukhoa, int loaiID, int monhocID, int khoilopID)
        {
            string _tukhoa = "";
            if ( !string.IsNullOrEmpty(tukhoa))
            {
                _tukhoa = ClassCommon.RemoveUnicode(tukhoa).ToLower();
            }        
            return x => ((ClassCommon.RemoveUnicode(x.MaSanPham).ToLower().Contains(_tukhoa) || ClassCommon.RemoveUnicode(x.TenSanPham).ToLower().Contains(_tukhoa))
                        //&&( loaiID ==0 || x.LoaiSanPhamID == loaiID)
                        //&& (monhocID ==0 || x.MonHocID == monhocID)
                        //&& (khoilopID ==0 || x.KhoiLopID == khoilopID)
                        );
            
        }

        public IEnumerable<SanPhamViewModel> GetInclude()
        {            
           var objSanPham =  _context.SanPham.Include(x=>x.MonHoc).Include(x=>x.ThongTu).Include(x=>x.KhoiLop).Include(x => x.DonViTinh).Include(x=>x.LoaiSanPham).Select(x => new SanPhamViewModel
            {

               SanPhamID = x.SanPhamID,
               MaSanPham = x.MaSanPham,
               TenSanPham = x.TenSanPham,
               TieuChuanKyThuat = x.TieuChuanKyThuat,
               HangSanXuat = x.HangSanXuat,
               XuatXu = x.XuatXu,
               BaoHanh = x.BaoHanh,
               NamSanXuat = x.NamSanXuat,
               NhaXuatBan = x.NhaXuatBan,
               DonViTinh = x.DonViTinh == null ? null : _mapper.Map<DonViTinhDto>(x.DonViTinh),
               GiaVon = x.GiaVon,
               GiaDaiLy = x.GiaDaiLy,
               GiaTTR = x.GiaTTR,
               GiaTH_TT = x.GiaTH_TT,
               GiaCDT = x.GiaCDT,
               Thue = x.Thue,
               SoLuong = x.SoLuong,
               DoiTuongSuDung = x.DoiTuongSuDung,
               LoaiSanPham = x.LoaiSanPham == null ? null : _mapper.Map<LoaiSanPhamDto>(x.LoaiSanPham),
               ThuongHieu = x.ThuongHieu,
               HinhAnh = x.HinhAnh,
               Model = x.Model,
               MonHoc  = x.MonHoc ==null ? null : _mapper.Map<List<MonHocDto>>(x.MonHoc),
               KhoiLop = x.KhoiLop == null ? null : _mapper.Map<List<KhoiLopDto>>(x.KhoiLop),
               ThongTu = x.ThongTu == null ? null : _mapper.Map<List<ThongTuDto>>(x.ThongTu),
           }).ToList();
            return objSanPham;
        }
        public IEnumerable<SanPhamViewModel> GetByThongTu(int thongTuID)
        {
            var lstSanPhamID = _context.SanPhamThongTu.Where(x => x.ThongTuID == thongTuID).Select(x => x.SanPhamID).ToList();
            var objSanPham = _context.SanPham.Where(x=> lstSanPhamID.Contains(x.SanPhamID)).Include(x => x.MonHoc).Include(x => x.ThongTu).Include(x => x.KhoiLop).Include(x=>x.DonViTinh).Include(x => x.LoaiSanPham).Select(x => new SanPhamViewModel
            {
                SanPhamID = x.SanPhamID,
                MaSanPham = x.MaSanPham,
                TenSanPham = x.TenSanPham,
                TieuChuanKyThuat = x.TieuChuanKyThuat,
                HangSanXuat = x.HangSanXuat,
                XuatXu = x.XuatXu,
                BaoHanh = x.BaoHanh,
                NamSanXuat = x.NamSanXuat,
                NhaXuatBan = x.NhaXuatBan,               
                DonViTinh = x.DonViTinh == null ? null : _mapper.Map<DonViTinhDto>(x.DonViTinh),
                GiaVon = x.GiaVon,
                GiaDaiLy = x.GiaDaiLy,
                GiaTTR = x.GiaTTR,
                GiaTH_TT = x.GiaTH_TT,
                GiaCDT = x.GiaCDT,
                Thue = x.Thue,
                SoLuong = x.SoLuong,
                DoiTuongSuDung = x.DoiTuongSuDung,
                LoaiSanPham = x.LoaiSanPham == null ? null : _mapper.Map<LoaiSanPhamDto>(x.LoaiSanPham),
                ThuongHieu = x.ThuongHieu,
                HinhAnh = x.HinhAnh,
                Model = x.Model,
                MonHoc = x.MonHoc == null ? null : _mapper.Map<List<MonHocDto>>(x.MonHoc),
                KhoiLop = x.KhoiLop == null ? null : _mapper.Map<List<KhoiLopDto>>(x.KhoiLop),
                ThongTu = x.ThongTu == null ? null : _mapper.Map<List<ThongTuDto>>(x.ThongTu),
            }).ToList();
            return objSanPham;
        }

        public IEnumerable<SanPham> GetLikeTenSanPham(string tenSanPham)
        {
            string[] searchTerms = tenSanPham.ToLowerInvariant().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            // Perform the fuzzy search          
            var obj = _context.SanPham.Where(x =>( _context.FuzzySearch(x.TenSanPham) == _context.FuzzySearch(tenSanPham))).ToList();
            return obj;
        }
    }
}

